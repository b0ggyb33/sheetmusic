
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Lady Marmalade"
  opus = "16 October 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
#(set-global-staff-size 21)

Intro = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
          \set Score.skipBars = ##t   {R1*4}  \bar "||"
          \relative c^"Hey sister ..."
          \set Score.skipBars = ##t   {R1*4}    
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'' {}
    }}
    {\chords 
     { a2:m d a2:m d a2:m d a2:m d 
       a2:m d a2:m d a2:m d a2:m d } 
    }
  >>
}

Instrumental = 
{<<
  \relative c''
  {
    a4.^"INSTRUMENTAL" b8 c4. d8 e1 e8 d c d~ d2~ d2. c8 d
    e8 d c e~e2~e4 c a2  e'8 d c d~d2~d4 c a2
    e'4. gis8~gis2 b8 b r b r4 e8 r
  }
  \chords {a1*2:m f a:m f e}
 >>
}




VerseChorus = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c' 
      {
        r1^"VERSE & CHORUS 1&2" r r r d'2 f g b
        r1 r4 r r8 g' a g a8 r r4 r r   r r r8 g a g a8 r r4 r r   r r r8 g a g
        r4^"Creole..." r r r a1~a r r r r8 a r a~a2
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'
      {
        r1 r r r d2 f g b
        r1 r4 r r8 g' a g a8 r r4 r r   r r r8 g a g a8 r r4 r r   r r r8 g a g
        r4 r r r8 a~a4. g8 e4. d8 c4. a8 c4 a r1 r r r8 a' r a~a2
      }
    }}
    { 
      \chords 
      {
        a1:m d a:m d d:m e:m
        a:m d a:m d a:m d
        d:m r r a:m d a:m d
      }    
    }
  >>
}

VerseThree = 
{<< 
    
     {
      \new Voice = "first"
      {   
        \voiceOne 
        \relative c'''' 
        {
        a8^"(  ) VERSE 3" r r4 r r r1 r r d,,2 f g b
        e,4. gis8~gis2  b8 b b b b e fis gis 
        }    
      }
    }
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'''
      {
        a8r r4 r r r1 r r d,,2 f g b
        e,4. gis8~gis2  b8 b b b b e fis gis 
      }
    }}
     
    { 
      \chords 
      {
        a1:m d a:m d d:m e:m
        e1*2
      }    
    }
  >>
}


ChorusThree = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''''
      {
        \repeat volta3
        { a8^"CHORUS 3"  r r4 r r r4 r r8 g a g^"x3" }
        a8^"Creole ..." r r4 r r   
        a1~a 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'''
      {
        a8 r r4 r r r4 r r8 g a g 
        a8 r r4 r r8 a~a4. g8 e4. d8 c4. a8 c4 a^"x2"
      }
    }}
    { 
      \chords 
      {
        a1:m d  
        
      }    
    }
  >>
}

Outro = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'''' 
      {
        \repeat volta3
        { a8^"OUTRO"  r r4 r r r4 r r8 g a g^"xLots" }
        a8^"Creole ..." r r4 r r   
        a1~a 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        a'8 r r4 r r  r4 r r8 g a g 
        a8 r r4 r r8 a~a4. g8 e4. d8 c4. a8 c4 a
      }
    }}
    { 
      \chords 
      {
        a1:m d  
        
      }    
    }
  >>
}






Bbsaxpart =
{
  \tempo 4 = 114
  \key a \minor


  \Intro  \break
  \repeat volta2{\VerseChorus} \break
  \Instrumental \break
  \repeat volta2{\VerseThree} \break
  \repeat volt2 {\ChorusThree} \break
  \Outro
  
}


\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"} 
    \transpose c g, \Bbsaxpart   
}

\pageBreak
\markup {"Lady Marmalade"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

