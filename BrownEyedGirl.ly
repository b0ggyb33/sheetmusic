
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Brown Eyed Girl"
  subtitle = "V1"
}
#(set-global-staff-size 25)

Intro =
{
  \relative c^"Intro"
  \repeat volta 2 {\chords {a1 d a e  } }
}

Verses = 
{
  \repeat volta 2
  {
    \relative c^"Verse: play V3" \relative c^\segno
    <<
      {\chords
        {
          a1 d a e
          \bar "||"a d a e
          \bar "||"a d a e
          \bar "||"a d a
        }
      }
      {
        \relative c''
        {
          a8 r r a a4 a8 r a1 
          a8 r r a a4 a8 r gis1 
          a8 r r a a4 a8 r a1 
          a8 r r a a4 a8 r b1
          a8 r r a a4 a8 r a1 
          a8 r r a a4 a8 r gis1 
          a8 r r a a4 a8 r a1 
          a8 r r a a4 a8 r           
        }
      }
    >>
    
    {<<
      \chords
      {
        e1 
        \bar "||"d e a fis:m
        \bar "||"d e a 
      }
      \relative c'
      {
        e4^"Play always" fis8 gis4 b8 cis4
        d1 e a2. gis4 fis2. cis4 
        d1 e a4 r4 r2^\coda  
      }
    >>}
  }
  \alternative
  {
    { \chords { e1 } }
    { 
      {<< \chords { e e e } \relative c' { r1 r1 r1 } >>}    
    }
  }
  
  {<<
      \chords { a d a e    a d a e }
      \relative c''
      {
        r4 e8 e4 e fis8~fis8 fis4 fis fis e8~e8 e4 e8 e4 cis8 b~b4 r4 r2
        r4 e8 e4 e fis8~fis fis4 fis fis e8~e8 e4 e8 e4 cis8 b~b4 r cis8 b4 a8~a4
        r4 r2 r1 r1^"bass" r1 r1 r1 r1^"D.S. al Coda" r1 
      }
   >>}  
}

Coda =
{
  {\bar "||"<< \chords { e e e } \relative c' { r1^\coda ^"Coda" r1 r1 } >>} 
  \repeat volta 2
  { <<\chords { a d a }
    \relative c'' {r4 e8 e4 e fis8~fis fis4 fis fis e8~e8 e4 e8 e4 cis8 b~ }>>
  }
  \alternative
  {
    {<<\chords { e }\relative c'' {b4 r4^"rpt xn" r2} >>}
    {\relative c'' {b4 r cis8 b4 a8~a4 r4 r2} }
  }
}

Bbsaxpart =
{
  \tempo 4 = 160
  \key a \major
  \time 4/4
  
  \Intro 
  \Verses
  \Coda
}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Brown Eyed Girl   "} 

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 


}

