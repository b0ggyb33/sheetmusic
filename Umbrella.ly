
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Umbrella"
  subtitle = "V1"
}

Intro = 
{<< 
    %\new Voice = "first"
    { \voiceOne r1^"Intro" r r r \bar "||"}
    %\new Voice= "second"
    %{ \voiceTwo c, ees f8 g4. c1 c8 bes4. g4 f ees c r r%}
    {\chords {aes1 ees g c:m} }
  >>
}

Verse =
{<<
  {c'''^"Verse - play V2" r1 r1 r1^"x2" \bar "||" }
  {c''}
>>}
  

Chorus = 
{<< 
  \relative c'''
  \new Voice
  { 
    \voiceOne c2.^"Chorus"~c8 aes  bes1  b2.~b8 g c1 \bar "||"
    c2.~c8 aes  bes2 \tuplet 3/2 {ees8 d c} bes8 g b2 \tuplet 3/2 {ees8 d c} b8 g c2~c8 bes aes g \bar "||"
    c4 c c c8 aes  bes4 bes bes bes8 g   b4 b \tuplet 3/2 {ees8 d c} b8 g c4 c g'8 f es d \bar "||"
  }
  \relative c''
  \new Voice
  { 
    \voiceTwo c2.~c8 aes  bes1  b2.~b8 g c1
    c2.~c8 aes  bes2 \tuplet 3/2 {ees8 d c} bes8 g b2 \tuplet 3/2 {ees8 d c} b8 g c2~c8 bes aes g
    c4 c c c8 aes  bes4 bes bes bes8 g   b4 b \tuplet 3/2 {ees8 d c} b8 g c4 c g'8 f es d   
  }
 >>
}

Interlude =
{<<
  {\chords {des1 aes ees1*2  des1 aes g2 b2 d2 g2}}
  {
    \relative c'''
    \new Voice
    {\voiceOne r1^"Interlude" r r r r r g2 b2 d2 g8 f ees d \bar "||" c1^"Chorus etc - play long behind gtr solo" bes b c \bar "||"}
  }
  {
    \relative c''
    \new Voice   
    {\voiceTwo r1 r r r r r g2 b2 d2 g8 f ees d c1 bes b c}
  }
  
>>}


Bbsaxpart =
{
  \tempo 4 = 90
  \key c \minor

  \relative c''
  \Intro
  \Verse
  \Chorus
  \Interlude

}

\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"}  
    \transpose d a, \Bbsaxpart 
    
}

\markup {"   "}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

