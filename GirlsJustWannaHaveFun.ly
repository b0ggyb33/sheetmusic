
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Girls Just Wanna Have Fun"
  subtitle = "V1"
}
#(set-global-staff-size 26)



IntroA = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c''' 
      {
        r2 g r2 g4 fis r2 e r2 e4 fis
      } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r2 g r2 g4 fis r2 e r2 e4 fis
      }}
    }
    {\chords {g1*2 e:m } 
    }
  >>
}

Chorus = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c''' 
      {
        r4 g2.~g1 r4 e2.~e1
        d1 g2 b 
        r4 b r a r g2. r4 b r a
      } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c'''
      {
        r4 g2.~g1 r4 e2.~e1
        d1 g2 b 
        r4 b r a r g2. r4 b r a
      }}
    }
    {\chords { } 
    }
  >>
}



GirlsTheyWanna = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c''' 
      {
        g4^"Girls they wanna" r8 g g g r4 r2 g8 g fis4
        e4 r e r r2 e8 e fis4
      }}    
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        g4 r8 g g g r4 r2 g8 g fis4
        e4 r e r r2 e8 e fis4                
      }}
    }
    {\chords {} 
    }
  >>
}

Outro = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
        r4 d' g,8 g r4 d' d8 g,4 a8 b4 
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
      }  }  
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
        r4 d' g,8 g r4 d' d8 g,4 a8 b4 
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1^" etc .."
                
      }}
    }
    {\chords {} 
    }
  >>
}

Verse =
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*2}
  << 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r4 b r a r g2. r4 b r a 
      } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r4 b r a r g2. r4 b r a          
      }}
    }
    {\chords {d1:m g} 
    }
  >>
}

SynthSolo =
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}


Bbsaxpart =
{
  \tempo 4 = 122
  \key g \major


  \relative c^"Intro"  \IntroA \IntroA
  
 \bar "||" \relative c^"Verse 1"  \Verse \bar "||"
 \bar "||" \relative c^"Interlude"  \IntroA 
 \repeat volta2
 {
   \relative c^"Verse 2 & 3"  \Verse 
   \bar "||" \relative c^"Chorus"  \Chorus 
   \bar "||" \GirlsTheyWanna 
   \bar "||" \relative c^"1st Time: Synth Solo"  \SynthSolo
 }
 \break \relative c^"Outro"  \Outro

}

\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"}  
    \transpose d a, \Bbsaxpart 
    
}

\pageBreak
\markup {"Girls Just Wanna Have Fun"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

