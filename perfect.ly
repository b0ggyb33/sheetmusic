
\version "2.18.2"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Perfect"
  subtitle = "V0.1"
}
#(set-global-staff-size 21 )

%18 bars per block??

chorus = {{<<
    \chords { a1 b1 e2. a4 e1
              a1 b1 e2. a4 e2 e:7
              a1 gis
              cis2 b2 a1
              a1 b1 a e }
    \relative c''
    {
      gis1^\markup{CHORUS} fis fis2 gis r1
      gis1 fis fis2 e r1
      a1 gis cis2 b2
      a2 r2 r1 r a1 b4 r r2
    }
 >>}}

verse = {{<<
\chords{ a1 a e e
         a a e e
         a a cis cis
         a b e2 a4 b e1}
\relative c''
{
  r^\markup{VERSE}
}
  >>}}

verseTwo = {{<<
\chords{ a1 a e e
         a a e e
         a a cis cis
         a b e2 a4 b e1}
\relative c''
{
  r4^\markup{VERSE} a a b a r r2
  r4 gis gis a gis r r2
  r4 fis fis gis fis r r2
  r4 e e fis e r r2
  r4 a a b a r r2
  r4 gis gis fis e r r2
  r4 e e fis fis r r2
  r4 e e fis e r r2
}
  >>}}
  

  
soloverse = {{<<
  \relative c ^\markup{SOLO VERSE} \chords{
      a1 a e e
      a a e e
      a a cis cis
      a b e2 a4 b e1
  }
  >>}}

  outro = {{<<
    \chords{
     a1 b1 a e
     a1 b1 a e
    }
    \relative c ''
    {
      r1^\markup{OUTRO} r a1 b4 r r2      
      r1 r a1 gis
    }
    >>}}

Bbsaxpart =
{
  \tempo 4 = 138
  \key a \major

  \verse
  \bar "||"
  \chorus
  \bar "||"
  \verseTwo
  \bar "||"
  \chorus
  \bar "||"
  \soloverse
  \bar "||"
  \verse
  \bar "||"
  \chorus
  \bar "||"
  \outro
}

\score
{
  \new Staff \with { instrumentName = \markup \bold {"ALTO"}}
  \transpose d a, \Bbsaxpart
  %\midi{}
}

\pageBreak
\markup { "Perfect"  }
\score
{

  \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
  \Bbsaxpart

}
