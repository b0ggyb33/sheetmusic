
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "9 to 5"
  opus = "6 April 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
\layout{ragged-last =##t}

%#(set-global-staff-size 25)

Intro =
{ 
  \relative c^"Intro"
  { \compressFullBarRests
    R1*4
    aes''4 r r2 
    R1*3
  }
}




Verse = 
{
  <<
    \relative c''^"Verse"
    { \compressFullBarRests
      R1*2 R1*2 R1*2 R1*2   R1*2 R1*2 R1*1 R1*1 R1*2 
    }
    \chords 
    {
      aes1*2 des \bar "||" aes ees  \bar "||" aes1*2 des \bar "||" aes1 es1 aes1*2
    }
  >>
}


Chorus = 
{
  <<
    \relative c'''
    { 
       r1^"Chorus" r4 aes r2 r1 r4 bes,8^"?"( des~des4 bes-.)
       r1 r4 aes r2 r1 f'4 (ees8) r c (bes aes4)
       r1 r4 aes' r2 r1 r4 des,-- c-- b--  
       \bar "||"bes1~bes ees ~ees2 r4 d--
       (des4-.) r r2 r4 aes' r2 r1 r4 bes,8 (des~des4 bes-.)
       r1 r4 aes' r2 r1 f4 (ees8 )r c (bes aes4)
       r1 r4 aes' r2 r1 r4 des,-- c-- b--  
       bes2 c des d ees f fis^"No rpt for C1" r4 g-- (aes-.) r r2 r1 r^"Link to V2" r
    }
    \chords 
    { 
      \bar "[|:" des1*4 \bar "||"aes   \break des bes1*2 ees
      \break des1*4 \bar "||"aes    \break des  \bar "||"bes1*2 ees   \bar ":|]"
      \break aes1*4
    }
  >>
}



Bbsaxpart =
{
  \tempo 4 = 210
  \key as \major
  \time 4/4
  
  \Intro \bar "."
  
  \Verse \break
    
  \Chorus \bar "."


}


\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
%\pageBreak
%\markup {"9 to 5"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

