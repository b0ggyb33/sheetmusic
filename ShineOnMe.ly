
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Shine On Me"
  subtitle = "V1"
}
#(set-global-staff-size 23)

FourPlusFOur =
{
  %\bar "||"
  \relative c^"Guitars"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}

Verse =
{
  \repeat volta2
  {
    <<{\chords {e2 b a1 a2 b e1}}
      \new Voice = "first"
      {\voiceOne \relative c''' {
         
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {

      }}
    >>
  }
}

PreChorus =
{
    <<{\chords {a1*2 b a gis}}
      \new Voice = "first"
      {\voiceOne \relative c'' {
         cis1^"PreChorus"~cis dis~dis e~e fis~fis2 gis8 fis e dis
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
         a1~\ppp\<a b~b cis~cis dis~dis2 gis8 fis e dis\!\f
      }}
    >>
}

Chorus =
{
  \repeat volta2
  {
    <<{\chords {e2 b a1 a2 b e}}
      \new Voice = "first"
      {\voiceOne \relative c' {
        e2 b'8 b~b4 a2 cis8^"outro only" b a gis  a2 b8 b~b4 e,2 gis'8^"1st only & outro" fis e dis 
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        
      }}
    >>
  }
}


Bbsaxpart =
{
  \tempo 4 = 156
  \key e \major
  
  \FourPlusFOur
  \repeat volta3
  {
    \FourPlusFOur \break
    \relative c^"Verse - skip 3rd time"\Verse \break
    \PreChorus \break
    \Chorus
  }
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Shine On Me"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

