
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Rolling in the Deep"
  subtitle = "V1"
}
#(set-global-staff-size 27)

verse =
{<<
  {r1^"Verse1:x4  V2:x2" r1 r1 r1 }
  {
    \chords {cis1:m gis:m b gis2 b}
  }
>>}
  
prechorus =
{<< \chords 
     {
       a1  b gis:m a a  b gis:m gis:7
     }
     \relative c''' 
     \new Voice= "1"
     {
       \voiceOne {a1->^"Prechorus" b-> gis-> a-> 
                  a->  b-> gis-> fis2-> e4-> dis->} 
     }
     \relative c'' 
     \new Voice= "2"
     {
       \voiceTwo {a1 b gis a a  b gis fis2 e4 dis} 
     }
  >>   \bar "||"
}

chorus = 
{<< \chords 
  {
    cis1:m b a~a2 b cis1:m b a~a2 b
  }
  \relative c'''
  \new Voice = "first"
  {\voiceOne gis1^"others x2,"^"1st Chorus x1" fis e1~e2 fis gis1 fis e e2 fis }
  \relative c''
  \new Voice= "second"
  { \voiceTwo e1 dis cis~cis2 dis2 e1 dis cis cis2 dis}
 >>
}

bridge = 
{<<
  \relative c''
  \new Voice = "first"
  { \voiceOne r1^"Bridge" r2 r8 cis e( fis) gis2 r r2 r8  gis fis(( e)) 
               cis2 r2 r2 r8 cis e( fis) gis2 r gis fis}
  \relative c'
  \new Voice= "second"
  { \voiceTwo r1 r2 r8 cis e( fis) gis2 r r2 r8  gis fis( e) 
               cis2 r2 r2 r8 cis e( fis) gis2 r gis fis}  
 >> 
  \repeat volta 2
  {<<
    \relative c''
    { \voiceOne e8 fis gis fis e2 dis8 e fis e dis2  
                 cis8 dis e dis cis2 cis8 dis e4 dis8 e fis4}
    \relative c''
    \new Voice= "second"
    { \voiceTwo cis8 dis e dis cis2 b8 cis dis cis b2  
                 a8 b cis b a2 a8 b cis4  b8 cis dis4}
  >>}
  
}

Bbsaxpart =
{
  \tempo 4 = 110
  \key cis \minor  

  \relative c''
  \set Score.skipBars = ##t
  {R1*4}
 
  \repeat volta2 
  {
    \verse
    \bar "||"
    \prechorus
    << {\chorus} >>
  }
  \bridge
  \repeat volta2 
  {
    << {\chorus} >>
  }
  {
    <<gis''1 e''>>
  }

}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Rolling in the Deep   "} 

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

