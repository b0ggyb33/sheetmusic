\version "2.18.2"

\header {
  title = "She‘s Electric"
  instrument = "Alto Sax"
}

global = {
  \key fis \major
  \time 4/4
}

intro = { r1^"twiddles" r r r r^"intro" r r r r r r r
          r^"verse 1" r r r r r r r
          r^"verse 2" r r r r r r r }

chorus =
{
  r1^"chorus" r r e4( cis e cis~) cis2 r2 r1
  e2. dis8 cis b2 a8 gis fis4 fis2 r2 r1 r1 r2
  e'8. cis16( e8. cis16~) || c2 r1
  e1~ e
  e~ e
  fis fis
  b~ b
}

solo = {r1 r r r r r r r}

verseThree = {r1^"verse 3" cis4 e8 fis e2 || r1 r2 ees8. ees16 ees8. d16 cis1~ cis1
              cis2.(b8. ais16 cis8. b16 gis16. cis2) r4
              cis16 b8. a16 \tuplet 3/2{cis4 dis e} fis16 e8 e2 r2
              ees8. ees16 ees8. d16 cis1 r1
              cis2. (b8. ais16 cis8. b16 gis16. cis2) r4

}

ending = { c4( b8. a16 c8. b16 g8. c16~ c1)}

altoSax = {\relative c'' {
  \global
  \transposition es
  % Music follows here.
  \intro
  \chorus
  \solo}
  \relative c''{\verseThree
  \chorus
  \ending
}}




\score {
  \new Staff \with {
    instrumentName = "Alto Sax"
  } \altoSax
}
