
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "I Love Rock and Roll"
  opus = "22 April 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
\layout{ragged-last =##t}

#(set-global-staff-size 21)

Riff =
{ \relative c'
  {\teeny \tuplet 3/2 {fis'8 (e cis)} b8 a \normalsize}
}


Intro =
{ 
  <<\relative c''^"Intro/Chorus"
  { 
    \relative c'
    {     
      fis8 fis8 r4 fis8 fis8 r8 ais8 b2 cis8 cis8 r8 ais8
      fis8 fis8 r4 fis8 fis8 r8 ais8 b2 cis8 fis,8 fis4
      \time 3/4 r4 r r \time 4/4 fis8 fis8 r4  \Riff %r2
    } 
  }
  { \chords { fis1 b2 cis4. fis8 fis1 b2 cis8 fis fis4 r2. fis1  }
  }
  >>
}




Verse = 
{
  <<
    \relative c''
    { 
        \relative c'
        {
          fis8^"Verse: V3 = gtr solo" fis8 r4 r4 r4   fis8 fis8 r4 \Riff
          fis8 fis8 r4 r4 r4   cis'8 cis8 r4 \Riff
          b4^"Vocal back after solo" r4 cis8 cis8 r4 \time 2/4 fis,4 r  \time 4/4 b1^"  ... to acapella section"
          %\compressFullBarRests R1*3  
          r1 r r
          cis8 cis cis cis cis cis cis cis 
        } 
      
    }
    \chords 
    {
      fis1*3 cis1 b2 cis2 fis2 b1 R1*3 cis1
    }
  >>
}


Chorus = 
{
  <<
    \relative c'''
    { 
       r1^"Chorus" r1 r1\pp e~\coda\<
       e~ e~ e~ e
       e,1\ff\! dis cis b
       a4-. r2. r1 
       e'1 cis cis2. cis4 b2. cis4 \bar "||"e1^"back to gtr riff & V2 until Coda"~\>e1\ppp\! r1 r1 \bar ":|]"
    }
    \chords 
    { 
     
    }
  >>
}

Coda =
{
  <<
    \relative c'^"Acapella"
    {  
       \compressFullBarRests R1*4
     %  r1 r r r
       \bar "||"
       \compressFullBarRests R1*4
      % r1 r r r
       \repeat volta2
       { \relative c'
         {fis8^"Outro: x4" fis8 r4 fis8 fis8 r8 ais8 b2 cis8 cis8 r8 ais8
         fis8 fis8 r4 fis8 fis8 r8 ais8 \time 3/4 b2 cis8 fis,8} 
       }
       
    }
    \chords 
    { 
      R1*8
      fis1 b2 cis4. fis8 fis1 b2 cis8 fis 
    }
  >>
}



Bbsaxpart =
{
  \tempo 4 = 94
  \key fis \major
  \time 4/4
  
  \Intro \break
  
  \bar "[|:"\Verse \break
    
  \Intro \bar ":|]" \break
  \Coda 



}


\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"I Love Rock and Roll"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

