
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Think"
  opus = "15 November 2019"
}
\paper {top-margin = 3 bottom-margin = 5 left-margin = 5 right-margin = 5}
#(set-global-staff-size 22)

Intro = 
{<< 
    {
      \relative c'' 
      {
          \set Score.skipBars = ##t   {R1*4}  \bar "||"
          \relative c^"Think - think ..."
          \set Score.skipBars = ##t   {R1*3} 
          \relative c''
          {
             r4 r c8 bes g f16 ees 
          }
      }    
    }

    {\chords {  }  }
  >>
}

Verse = 
{<<
  \relative c'
  {
    \repeat volta4 {c4^"Verse" r r r  r4 r r b c4 r r r  r^"V1&2: x4"_"V3: x2" r r r}
  }
  \chords {c1 f2 c c1 f2 c }
 >>
}

Chorus = 
{<<
  \relative c'
  {
    \repeat volta4 {c1^"Chorus (Freedom...)"  ees f g2 c8 bes g f16 ees}
  }
  \chords {c1 ees f c }
 >>
}

KeyChange =
{<<
  \relative c'
  {
    r4^"Key change ...   Alto adlib blues scale?..." cis~cis2 r1
  }
  \chords {cis1*2}
 >>
}

PreChorus =
{<<
  \relative c'
  {
    fis1~^"Pre-chorus (You need...)" fis  cis'1~cis2 
    cis8 b gis fis16 e
    cis4 cis'2.~cis1 r1 r1
  }
  \chords {fis1*4 cis1*4}
 >>
   
}

Outro =
{<<
  \relative c^"Pre-chorus (You need...)"
  \relative c''
  {
    \repeat volta4 {d1~d  d,~d^"x4"} 
    d4 e fis g a b c8 b c cis d8 r d4
  }
  \chords {}
 >>
   
}

Bbsaxpart =
{
  \tempo 4 = 114
  \key c \major


  \Intro  \break
  \Verse \break
  \Chorus\break
  \KeyChange \break 
  
  %\key cis \major
  \transpose c cis {\Verse \break}
  \PreChorus \break
  \transpose c cis {\Chorus \break}
  %\transpose c des {\KeyChange \break}
  
  %\key d \major
  \transpose c cis {\Verse \break}
  %\transpose c d {\Chorus \break}
  \transpose cis cis {\PreChorus \break}
  
  \transpose d cis {\Outro}

}


\score 
  { 
    \new Staff \with {instrumentName = #"Alto sax"} 
    \transpose c g \Bbsaxpart   
}

\pageBreak
\markup {"Think"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

