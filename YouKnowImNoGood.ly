
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "You Know I'm No Good"
  subtitle = "V1"
}

fanfare = 
{<< 
    \new Voice = "first"
    { \voiceOne c ees f8 g4. c1 c8 bes4. g4 f ees c r r}
    \new Voice= "second"
    { \voiceTwo c, ees f8 g4. c1 c8 bes4. g4 f ees c r r}
  >>
}

Verse =
{<< \chords 
     {
       e1:m a:m b e:m   e:m a:m b e:m
     }
     \relative c'' 
     \new Voice= "1"
     {
       \voiceOne {} 
     }
     \relative c' 
     \new Voice= "2"
     {
       \voiceTwo {e2.^"Verse: tacit V1 & V3" g4 a1 b,2. dis4 e1  
                  e2.^"play V3" g4 a1 b,2. dis4 e1 \bar "||"} 
     }
  >>
}

  
Prechorus =
{<< \chords 
     {
       a1*2:m fis1*2:7 g1*2 fis1:7 b1
     }
     \relative c''' 
     \new Voice= "1"
     {
       \voiceOne {r4 c e2  r4 c e2 r4 cis fis2  r4 cis fis2 r1 r1 r1 b,4 b b r \bar "||" } 
     }
     \relative c'' 
     \new Voice= "2"
     {
       \voiceTwo {a1^"Prechorus" a fis fis 
                  g4. fis8 g4. fis8  g4. fis8 g4. e8  fis4 fis fis fis b b b r8 dis,} 
     }
  >>
}

ChorusSevenbars = 
{<< \chords 
  {
    e1:m b:m fis:7 b:m   e1:m b:m fis:7   
  }
  \relative c''
  \new Voice = "first"
  { \voiceOne  r4 e r2   r4 \tuplet 3/2 {fis8 f fis } r2 r4 fis r2   r4 \tuplet 3/2 {fis8 f fis } r2 
     r4 e r2   r4 \tuplet 3/2 {fis8 f fis } r2 r4 fis r2    }
  \relative c'
  \new Voice= "second"
  { \voiceTwo e4^"Chorus" r r r8 bes b4 r r r8 f' fis4 r r r8 bes, b4 r r r8 dis
    e4 r r r8 bes b4 r r r8 f' fis4 r r r8 bes, }
  
>>}


Bbsaxpart =
{
  \tempo 4 = 105
  \key e \minor  

  \relative c''
  \Verse
  \Prechorus
  
   \repeat volta2 {\ChorusSevenbars} 
   \alternative 
   {
     {<< \chords 
       {
         b:m  
       }
       \relative c''
       \new Voice = "first"
       { \voiceOne   r4 \tuplet 3/2 {fis8 f fis } r2 r1 r1}
       \relative c'
       \new Voice= "second"
       { \voiceTwo b4^"chorus 1 & 3 ending"-"after C3 play C4" r r r \bar "||" r1^"drums"^"Interlude after Chorus 1
"  r4 e8 fis^"to Verse 2" g fis e dis \bar "||" }  
      >>}
     {<< 
        \relative c'''
        \new Voice = "first"
        { \voiceOne r4^"chorus 2 & 4 ending & outro" e8 fis g fis e dis 
          e2~e8 f e d  b2~b8 b d e fis2 ~fis8 f e d b2~b8 b cis d 
          e2~e8 f e d  b2~b8 b d e fis2 ~fis8 f e d b2~b8 b cis d e1
        }
        \relative c''
        \new Voice= "second"
        { \voiceTwo r4 e8 fis g fis e dis 
          e2~e8 f e d  b2~b8 b d e fis2 ~fis8 f e d b2~b8 b cis d 
          e2~e8 f e d  b2~b8 b d e fis2 ~fis8 f e d b2~b8 b cis d e1^"V3"
         \bar "||" 
        }  
      >>}
   }
}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart 
    %\midi{}
}

\pageBreak
\markup {"You Know I'm No Good"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
}

