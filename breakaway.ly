\version "2.18.2"
\header{
	title = "Breakaway"
}


%Eb concert

intro = {<<
	\chords{
		c1 a:m
	}
	\relative c ''
  {
		r 1
		r 1
  }
>>}

chorus = {<<
  \chords {
    c1 c a:m a:m f g c a:m c a:m
  }
  \relative c ''
  {
  c8-.^"chorus" c4. c8-. c4. c8-. c4. c8-. c4.
	a8-. a4. a8-. a4. a8-. a4. a8-. a4.
  <a f'>1 <b g'>1
  c4-. c4-. c4-. r4
	c4-. c4-. c4-. r4
	c4-. c4-. c4-. r4
  c4-. c4-. c4-. r4
  }

>>}

verse = {<<
  \chords{c1 a:m c a:m f f}
  \relative c ''
  {
	c4-.^"V1 tacit, play V2" c8-. c16 d e d c8-. r4
	a4-. a8-. a16 b c b a8-. r4
	c4-. c8-. c16 d e d c8-. r4
	a4-. a8-. a16 b c b a8-. r4
  f'1~ f1
  }
>>}

verseThree = {<<
  \chords{c1 a:m c a:m f f}
  \relative c ''
  {
	r1^"V3"
	r1
	r1
	r1
  f'1~ f1
  }
>>}

bridge = {<<
  \chords{a1 d:m f g a d:m f g}
  \relative c ''
  {
	r1^"bridge" r1 r1 r1
	b4 d4-. \tuplet 3/2 {cis4 d e} %a
	r4 d4 f8 d c a %dm
	r8 <c e>4 <d f>8 <c e>4 <d f>8 <e gis>8  %f
	<g b>1 %g
  }
>>}

instrumental = {<<
  \chords{c1 a:m c a:m}
  \relative c ''
  {
	c8-.^"instrumental" c8-. c8-. c8-. c8-. c16 d e d c8-. a2 g2
	c8-. c8-. c8-. c8-. c8-. c16 d e d c8-. f2 g2
  }
>>}

drumFill = {<<
	\relative c ''{r1^"Drum" r1}
>>}

guitarFill = {<<
	\relative c''{r1^"Guitars?" r1}
>>}

outro = {<<
\chords{c1 a:m c a:m}
\relative c ''
{
c8-.^"outro" c4. c8-. c4.
a8-. a4. a8-. a4.
c8-. c4. c8-. c4.
a8-. a4. a8-. a4.
}
>>}

altopart = {
    \tempo 4 = 120
    \time 4/4
    \clef treble
    \key c \major
    \intro
		\repeat volta 2
		{
		\verse
		\bar "||"
    \chorus
		}
    \bridge
		\bar "||"
		\drumFill
		\instrumental
		\instrumental
		\bar "||"
    \verseThree
		\bar "||"
		\chorus
		\bar "||"
		\guitarFill
  	\bar "||"
		\chorus
		\repeat volta 2
		{
		\outro
		}
}
\score
{
    \new Staff \with { instrumentName = #"Alto Sax"}
    \relative c'' {
        \altopart
			}
}
\pageBreak
\score
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \transpose c f, \altopart

}
