
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Poker Face"
  subtitle = "V1"
}
#(set-global-staff-size 17)

LeadIn = 
{
    <<\new Voice = "first"
    {\voiceOne \relative c''' {
       bes8 bes aes
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
       bes8 bes aes
    }}
    >>
}

DooDoos =
{ 
  \repeat volta3
  {<<
    \new Voice = "first"
    {\voiceOne \relative c''' {
       bes8 r r4 r2  
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
       bes8 r r4 r2  
    }}
  >>}
  
    \alternative
    {
      {<<    
        \new Voice = "first"
        {\voiceOne \relative c''' {
         r2 r8 bes bes aes  
         }}   
        \new Voice = "second"
        {\voiceTwo \relative c'' {
         r2 r8 bes bes aes 
        }} 
      >>}
      
      {<<    
        \new Voice = "first"
        {\voiceOne \relative c''' {
         r1  
         }}   
        \new Voice = "second"
        {\voiceTwo \relative c'' {
         r1  
        }} 
      >>} 
   }
}


WigOne =
{   
  
    <<
      \new Voice = "first"
      {\voiceOne \relative c''' {
        bes4. des8 ees4 des4
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        bes4. des8 ees4 des4
      }}
    >>
}

WigTwo =
{    
    <<
      \new Voice = "first"
      {\voiceOne \relative c''' {
        bes4. des8 ees8 f ees des
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        bes4. des8 ees8 f ees des
      }}
    >>
}

WigThree =
{   
    <<
      \new Voice = "first"
      {\voiceOne \relative c''' {
        bes4 r4 r2
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        bes4 r4 r2
      }}
    >>
}

Chorus =
{
 
  \repeat volta2
  {<<
    \new Voice = "first"
    {\voiceOne \relative c'' {
       f2. f4 ges1 f2. f4   
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
      des2. des4 des1 des2. des4   
    }}
  >>}
  
    \alternative
    {
      {<<    
        \new Voice = "first"
        {\voiceOne \relative c'' {
         ees1 
         }}   
        \new Voice = "second"
        {\voiceTwo \relative c'' {
         c1 
        }} 
      >>}
      
      {<<    
        \new Voice = "first"
        {\voiceOne \relative c'' {
          ees2~ees8 
         }}   
        \new Voice = "second"
        {\voiceTwo \relative c'' {
          c2~c8 
        }} 
       >>
      \LeadIn
      } 
   }
}



Bbsaxpart =
{
  \tempo 4 = 136
  \key bes \minor
 
  {r2 r8}
  \LeadIn \DooDoos
  \set Score.skipBars = ##t   {R1*2} 
  \relative c^"Verse" 
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4} 

  \repeat volta2 {\WigOne \WigTwo \WigThree r1} \break
  \Chorus 
  \DooDoos
  \set Score.skipBars = ##t   {R1*4}  
  \relative c^"Verse" 
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4} \bar "||"
  
  \WigOne \WigTwo \WigOne \WigThree \break
  \Chorus 
 
  
  <<\new Voice = "first"
    {\voiceOne \relative c''' {
       bes8 r r4 r2
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
       bes8 r r4 r2
    }}
  >>

  \set Score.skipBars = ##t   {R1*3} 
  
  <<\new Voice = "first"
    {\voiceOne \relative c''' {
       bes4 \fermata r r2
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
       bes4 r r2
    }}
  >>
  \set Score.skipBars = ##t   {R1*3} 
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}  \bar "||"
  
  {<<
    \new Voice = "first"
    {\voiceOne \relative c''' {
       r1 r f ees   
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
      r1 r f ees  
    }}
  >>}  \break
  \Chorus \DooDoos
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Poker Face"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

