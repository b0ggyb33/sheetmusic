
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Groove is in the Heart"
  subtitle = "V1"
}
#(set-global-staff-size 23)

Chorus =
{ \repeat volta 2
  {\relative c''
    {
      bes8^"Groove is ...  Chorus" r r bes r2  r8 ees ees8 (des) bes4 aes8 bes
      r1  r8 ees ees (f) ees4 des 8 bes^"x2"
    }
  
  } 
}

SetUpOne =
{ r1 r^"4 bar blocks" r r^"last kazoo" }

SetUpTwo =
{ r r r r^"1-2-3-blrr"  }

Saxbreak =
{ \repeat volta 4
  {\relative c''
    {
      bes8 des^"Sax break   x4"  ees r16 f16 (ees8) des bes4   bes8 des ees r r2^"back to chorus"
    }
  }
}


Bbsaxpart =
{
  \tempo 4 = 122
  \key bes \minor
  
  \SetUpOne
  \Chorus
  \break
  \SetUpTwo
  \Saxbreak
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Groove is in the Heart"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

