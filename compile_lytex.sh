#!/bin/sh
lilypond-book --output=build/ --pdf $1
cd build/
pdflatex ${1%.*}.tex
cd ..
cp build/${1%.*}.pdf .
rm -r build
