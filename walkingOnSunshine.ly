 \version "2.18.2"
\header{
	title = "Walking on Sunshine"
}

#(set-global-staff-size 25)


intro = {
  \relative c ''
  {
    \set Score.skipBars = ##t   {R1*4}
    \set Score.skipBars = ##t   {R1*4}
  }
}
chorus = 
{
\relative c ''
  {
    r4 r g' r
    r c,8 c c cis r d~
    d d~ d d d4 cis
    c r r^"x2 xN at end" r
  }
}

verseOne = 
{  
  r1^"Verse 1" r r r
  <<
    \chords{g1 c d c}
    \relative c'' {d1 e fis e^"x4"}
    \relative c' {d1 e fis e}    
>>}

verseTwo = 
{  
  r1^"Verse 2" r r r
  <<
    \chords{g1 c d c}
    \relative c''
    { b2 (d) c (e) d (fis) e (c^"x4") }
    \relative c'
    { b2 (d) c (e) d (fis) e (c) }
  >>
}


verseOut = 
{  
  <<
    \chords{g1 c d c g1 c d c}
    \relative c'' 
    {
      d2^"Outro Verse: build slowly ppp to fff" d e e fis fis e e \bar "||"
      d2 d e e fis fis g1^"x3"
    }
    \relative c'  
    {
      d2 d e e fis fis e e
      d2 d e e fis fis g1
    }    

>>}


bridge = 
{<<
  \relative c ''' {b1~b a~a^"x3" }
  \relative c '' {b1~b a~a }
>>}


altopart = {
    \tempo 4 = 210
    \time 4/4
    \clef treble
    \key g \major
    \intro
    \repeat volta 2 {\chorus} \break
    \repeat volta 2 {\verseOne}  \break
    \repeat volta 3 {\bridge}
    \repeat volta 2 {\chorus} \break
    
    \repeat volta 2 {\verseTwo}  \break
    \repeat volta 3 {\bridge}
    \repeat volta 2 {\chorus} \break
    
    {
      \set Score.skipBars = ##t   {R1*4}
      \relative c^"Gtr" \set Score.skipBars = ##t   {R1*4}
    }


    \repeat volta 2 {\verseOut}  \break
    \repeat volta 3 {\bridge}
    \repeat volta 2 {\chorus} \break

}
\score
{
    \new Staff \with { instrumentName = #"Alto Sax"}
    \relative c'' { \altopart }
    %\midi{}
}

\pageBreak
\markup {"Walking on Sunshine"}

\score
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \transpose c f \altopart

}
