
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Take Your Mama Out"
  subtitle = "V1"
}
#(set-global-staff-size 25)

Intro = 
{ \repeat volta2
  {<< 
     \new Voice = "first"
     {\voiceOne \relative c''{ e1^\markup { \with-color #red "Intro play 2nd" }~e d~d d~d c~c}}
     \new Voice = "second"
     {\voiceTwo \relative c''{ c1~c2. g4-> bes1~bes a~a2. f4-> g1~g  }} 
  >>}  
}

PreChorus = 
{ \repeat volta2
  {<< 
     \new Voice = "first"
     {\voiceOne \relative c''{ e1^\markup { \with-color #red"PreCh Play 2nd"}~e d~d d~d c~c}}
     \new Voice = "second"
     {\voiceTwo \relative c''{ c1~c2. g4-> bes1~bes a~a2. f4-> g1~g  }} 
  >>}  
}




Verse =
{
  \relative c^\markup { \with-color #red "Verse"}
  \set Score.skipBars = ##t   {R1*16}
  %\set Score.skipBars = ##t   {R1*8}
}

BridgeA =
{<<
    \new Voice = "first"
    {
      \voiceOne \relative c''' 
      \repeat volta2 {a2^\markup { \with-color #red"Bridge 1"} a f f }
      \alternative{{g1~g} {e1~e} }
    }
    \new Voice = "second"
    {
      \voiceTwo \relative c'' 
      \repeat volta2 {f2 f d d }
      \alternative { {c1~c} {c1~c} }
    }
>>} 

BridgeB =
{<<
    \new Voice = "first"
    {
      \voiceOne \relative c''' 
      \repeat volta2 {a1^\markup { \with-color #red"Bridge 2"} a f f }
      \alternative{{g1~g} {e1~e} }
    }
    \new Voice = "second"
    {
      \voiceTwo \relative c'' 
      \repeat volta2 {f1 f d d }
      \alternative { {c1~c} {c1~c} }
    }
>>}  


Chorus =
{<<
    \new Voice = "first"
    {
      \voiceOne \relative c'''' 
      \repeat volta 2 {g1^\markup { \with-color #red"Chorus"}~g f~f f~f }
      \alternative { {e~e} {g~g} }
    }   
    \new Voice = "second"
    {
      \voiceTwo \relative c''' 
      \repeat volta2 {c1~c bes~bes a~a }
      \alternative { {g~g} {c~c} }
    }
>>}

OutroChorus =
{<<
    \new Voice = "first"
    {
      \voiceOne \relative c'''' 
      {g1^\markup { \with-color #red"Outro Chorus"}~g f~f f~f e~e \bar "||"g1~g f~f f~f g~g \bar "||"e,\fermata }
    }   
    \new Voice = "second"
    {
      \voiceTwo \relative c''' 
      {c1~c bes~bes a~a g~g e'1~e bes~bes a~a c~c c, }
    }
>>}


SaxSolo =
{
  \relative c'''
  <<
    \chords {c1*2 bes d:m c}
    {a8^\markup { \with-color #red"Sax solo Ad Lib"} g r4 r r  r g g g  f4. d8 bes 2 r2 g8 a bes c 
     f4. d8 a2~a f'4. e8 c2 r4 g8a bes c-. r c4. r4}
  >>
  
}
GtrSolo =
{
  \relative c^\markup { \with-color #red"Gtr solo"}
  \set Score.skipBars = ##t   {R1*16}
  %\set Score.skipBars = ##t   {R1*8}
}

Bbsaxpart =
{
  \tempo 4 = 155
  \key c \major
 
  \Intro
  \Verse
  \BridgeA
  \PreChorus
  \Chorus
  \bar "||"\SaxSolo
  \bar "||"\Verse
  \BridgeB
  \PreChorus
  \Chorus
  \bar "||"\GtrSolo
  \bar "||"\OutroChorus
  
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Take Your Mama Out"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

