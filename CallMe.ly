
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Call Me"
  subtitle = "V1"
}

Intro =
{
    \relative c'
    {b4.^"intro" b8 b4. b8 b8 b8 e4 d4 b4}

}

IntroAlternate =
{
    \relative c'
    {cis1~ cis4 fis4 e4 cis4}

}


Verse =
{
  \relative c ''
  {
    \set Score.skipBars = ##t   {R1*8^"Verse"}
    e, fis
    e fis
  }
}

Chorus =
{
  \relative c ''
  {
    r4^"Chorus" b4 b4 r4 d1
    e g
  }
}

Bridge = {<<
  \chords{cis:m cis:m gis:m gis:m cis:m cis:m gis:m gis:m d d a a
  b:m b2:m g2 g1 e:m e2:m fis2 fis 1}
  \relative c''
  {
    r1^"Bridge" r1 r r
    r r r r
    r r r r
    b,2 d fis g~ g1
    e2 g b ais2~ ais1
  }
>>}

Instrumental = {
  \relative c ''
  {
    \set Score.skipBars = ##t   {R1*20^"Instrumental"}
  }
}

Ebsaxpart =
{
  \key b \minor
  \repeat volta 2
  {
  \Intro
  \Intro
  \bar "||"
  \Verse
  \bar "||"
  \Chorus
  \Chorus
  }
  \Intro
  \IntroAlternate
  \bar "||"
  \Bridge
  \bar "||"
  \Instrumental
  \bar "||"
  \Chorus
  \Chorus
  \Chorus
  \chords
  {
    e1:m e:m g
  }
  \Chorus


}

\score
{
    \new Staff \with { instrumentName = #"Alto Sax"}
    \Ebsaxpart
}

\score
{

    \new Staff \with { instrumentName = #"Tenor Sax"}
    \transpose c f \Ebsaxpart

}
