
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Crazy Little Thing called Love"
  subtitle = "V1"
}
#(set-global-staff-size 23)

VerseTwo =
{<< 
     \new Voice = "first"
     { \voiceOne \relative c''
       { \repeat volta2
         {
           r4^"play Verse 2" b8 b4. r4
           r4 b8 b4. r4
           r4 cis8 cis4. r4
           d4. d8 cis4 r
         }
         r1 r1 e4 fis g gis e r r2
       }
     }
     \new Voice = "second"
     {\voiceTwo \relative c'
       { \repeat volta2
         {
           r4 e8 e4. r4
           r4 e8 e4. r4
           r4 e8 e4. r4
           fis4. fis8 e4 r
         } 
         r1 r1 e4 fis g gis e r r2
       }
     } 
>>}

VerseFinalOut =
{<< 
     \new Voice = "first"
     { \voiceOne \relative c''
       { \repeat volta2
         {
           r4^"play Final Verse" b8 b4. r4
           r4 b8 b4. r4
           r4 cis8 cis4. r4
           d4. d8 cis4 r
         }
         r1 r1 e4 fis g gis e r r2
       }
     }
     \new Voice = "second"
     {\voiceTwo \relative c'
       { \repeat volta2
         {
           r4 e8 e4. cis4
           r4 e8 e4. cis4
           r4 e8 e4. fis4
           fis4. fis8 e4 cis
         } 
         r1 r1 e4 fis g gis e r r2
       }
     } 
>>}

OutA = 
{<< 
     \new Voice = "first"
     { \voiceOne \relative c'' { e4^"repeat until ..." fis g gis e r r2 } }
     \new Voice = "second"
     {\voiceTwo \relative c'   { e4 fis g gis e r r2 } } 
>>}

OutB = 
{<< 
     \new Voice = "first"
     { \voiceOne \relative c'' { \tuplet 3/2 {e4 fis g} \tuplet 3/2{gis g fis} e r r2 } }
     \new Voice = "second"
     {\voiceTwo \relative c'   { \tuplet 3/2 {e4 fis g} \tuplet 3/2{gis g fis} e r r2 } } 
>>}

Text =
{
  r1^"... chorus - verse (tacit) - gtr solo - off-beat hand clap verse ..."
}

Bbsaxpart =
{
  \tempo 4 = 156
  \key e \major
 
  \VerseTwo \break
  \Text \break
  \VerseFinalOut
  \bar "||"\OutA \bar "||"\OutB
  \repeat volta2 {\OutA}

  %\Chorus \break
  %\PostChorusOne \break
  %\PostChorusTwo \break
  
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Crazy Little Thing Called Love"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

