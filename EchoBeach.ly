
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Echo Beach"
  %subtitle = "V1"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

#(set-global-staff-size 26)

IntroOne =
{
  \relative c''
  { 
    \repeat volta4 {r1^"2x4 bars: Gtr; 2x4 bars with Dms+Bass" r r r}  
  }
}
IntroTwo =
{
  <<
    \relative c''
    { 
      \repeat volta4 {b2.\segno^"Play x 2" b4  a2. a4 fis2. e8 fis   g2 a}  
    }
    \chords { b1:m a fis:m g2 a}
  >>
}


Verse = 
{
  <<
    \relative c''
    { 
      \repeat volta2 {r1^"Verse" r1  r8 b d b d b~b4  e2 fis2}
    }
    \chords {b1:m e2 d b1:m e2 fis:m  }
  >>
}


Chorus = 
{
  <<
    \relative c''
    { 
      \repeat volta2 {a2^"Chorus" r gis2 e4 gis4}      
      \repeat volta2 {b2 r a2 fis4 a4}
    }
    \chords { a1 e b:m fis:m}
  >>
}



ShortBreak =
{  
  <<
    \relative c''  {g1 a^"D.S." }
    \chords { g1 a}
  >>
}

LongBreak =
{
  <<
    \relative c'' { g1 a c d \bar "||" g a c d \break }
    \chords { g1 a c d  g a c d}
  >>
}




SaxSolo = 
{
  <<
    \relative c'''
    { 
      fis4^"Sax Solo" d e b d a b d8 e fis4 e d b d2. cis4 \break
      b1~b2 a4 b  b a fis fis fis fis fis d \break b2 r
    }
    \chords {b1:m a fis:m g2 a b1:m a fis:m g2 a   b1:m }
  >>
}


OutroStart =
{ 
  <<
    \relative c'' { r1 r1 r1 }
    \chords {a1 fis:m g1 a}
  >>
}


OutroOut =
{
  \repeat volta2 
  {<<
    \relative c'' { r1 ^"after about 4 'Echo Beaches' Sax ad lib"  r r r}
    \chords {b1:m a1 fis:m g1 a}
  >>}
}

Bbsaxpart =
{
  \tempo 4 = 128
  \key b \minor
  \time 4/4
  
  \IntroOne \break
  
 
  \repeat volta2
  {
    \IntroTwo \break
    \relative c'' {b1^"Skip straight to Verse for V1"  r r r} \break
    \Verse \break
    \Chorus \break
  }
  \alternative
  {
    {\ShortBreak}
    {\LongBreak}
  }
  \SaxSolo

  \OutroStart
  \OutroOut

}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Echo Beach"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

