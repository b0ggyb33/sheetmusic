
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Waterloo"
  %subtitle = "V1"
  opus = "12 Aug 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

%#(set-global-staff-size 26)



Intro =
{
  r1^"Intro" r1 r1 r1
}


Verse = 
{
  <<
    \relative c''
    { 
      r1^"Verse" r r r r
      r r r r
      r r r b4. a8~a2 gis4. fis8~fis2
    }
    \chords 
    {
      e1 fis1   b2 a2  b1  \bar "||"
      e1  fis1   b2 a2  b1  \bar "||"
      cis1*3 fis1:7 \bar "||"
      b1 b1:7 \bar "||"
    }
  >>
}


Chorus =
{
  <<
    \relative c''
    { 
      r1^"Chorus" r1 
      r4 cis8 e~e4 cis8 b  \tuplet 3/2 {gis8^"??????" (fis e)} fis8 cis e4 fis4
      r4 r r r r1
      r1 r8 b,  b b  b b  b b
      r1 r1 r4 cis'8 e~e2~e2. cis4 
      b4 r4 r4 r4 r1 cis8 b b gis b4 cis r1 
      r1 r1 cis8 b b gis b4 cis
      b8 gis e cis e4 r8 cis
    }
    \chords 
    { 
      e1*2 a1*2 \bar "||"
      b1*2 e1 b1 \bar "||"
      e1*2 a1*2 \bar "||"
      b1*2 e1*2 \bar"||"
      b1*2 e1*2 
    }
  >>
}

Fill = 
{
  <<
    \relative c'
    { 
      e1^"Fill" r1 
    }
    \chords 
    {
      
    }
  >>
}

Bridge =
{
  <<
    \relative c'
    { 
      e1^"Bridge"  r1 r1 
      b'8 b  a4 gis fis
    }
    \chords 
    { 
      cis1*2 fis1 b2 b2:7 \bar "||"
    }
  >>
}

Outro =
{
  <<
    \relative c''
    { 
      r1^"Outro" r1 cis8 b b gis b4 cis
      b8 gis e cis e4 r8 cis
    }
    \chords 
    { 
      b1*2 e1*2 
    }
  >>
}

Bbsaxpart =
{
  \tempo 4 = 148
  \key e \major
  \time 4/4
  
  \Intro \break
  \repeat volta2
  {
    \Verse \break
    \Chorus \break
  }
  \alternative
  {
    {\Fill}
    {\Bridge}
  }
  \Chorus
  \repeat volta2{\Outro}

}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}

    \transpose d a \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Waterloo"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart
    %\midi{}
}

