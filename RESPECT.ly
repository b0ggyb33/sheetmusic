
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "RESPECT"
  opus = "7 October 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
#(set-global-staff-size 21)

Intro = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
        a2.^"Intro" r8 a8 g2. r4 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        
      }
    }}
    {
      \chords { d1:7  g1:7 } 
    }
  >>
}

Verse = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''' 
      {
        \repeat volta3 { r1^"Verse" r1^"x3"} 
        \repeat volta2 {r1 r1^"x2"} 
 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        
      }
    }}
    {
      \chords {a1:7 g:7 d1:7 g:7}
    }
  >>
}

SaxSolo = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''' 
      {
        \repeat volta3 { } 

 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        gis1^"Alto? Sax Solo - chord pentatonics maybe" cis gis a
      }
    }}
    {
      \chords {gis1:m7 cis:7 gis1:m7 a:7}
    }
  >>
}

Outro = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''' 
      {
        \repeat volta3 { r1^"R-E-S-P-E-C-T  etc" r1^"x2"} 
        \repeat volta2 
        {
          r4^"xn" a16 (aes a aes a8) r  r fis (g) fis g r r d_"...last note go up 8ve" r4
        } 
 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        
      }
    }}
    {
      \chords {a1:7 g:7 d1:7 g:7}
    }
  >>
}


Bbsaxpart =
{
  \tempo 4 = 114
  \key c \major


  \repeat volta2 {\Intro}
  \relative c^"3 Verses - last one ends 'justa- justa- ... just a little bit'" {\Verse }\break
  \SaxSolo \break
  \Verse \break
 
  \Outro
}

\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"}  
    \transpose d a, \Bbsaxpart 
    
}

%\pageBreak
%\markup {"The Power of Love"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

