
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Thorn In My Side"
  %subtitle = "V1"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

%#(set-global-staff-size 26)

SaxA =
{
  \relative c'''
  { 
    r4 gis gis8 fis4 gis8~gis4 r8  e fis gis r gis~gis (a4) a (e) e8~e dis4 cis dis4.   
  }
}

SaxB =
{
    \relative c'''
    { 
        r4 r8 gis8 gis8 fis8 gis8 gis8~gis4 r8 e fis gis r gis8~gis4 (a4) a8 (gis4) b8~b1^"To verse" 
    }
}

Intro =
{
  r1 r1 r1 r4 e''\fermata r r \bar "||"
  r1^"Spoken bit" r1 r1 r1\bar "||" 
  r1^"Gtr" r1 r1 r1
}

RunRunRun =
{
  \relative c'' {a8 a4 a a r8}
}

Verse = 
{
  <<
    \relative c''
    { 
      r1^"VERSE" r r r
      r r r r
      e d cis b
      r r r r
      r1 d4 cis b a  b4 a g e r1 
    }
    \chords 
    {
      e1*2 a1 b \bar "||"       
      e1*2 a1 b \bar "||"
      e1 d a b \bar "||"
      c1 g c a \bar "||"
      a1*3 d1  \break  
    }
  >>
}


ChorusOne = 
{
  <<
    \relative c''
    { 
      \repeat volta3 {r1^"CHORUS 1  Repeat x3" \RunRunRun r \RunRunRun} 
      r1 \RunRunRun r1
    }
    \chords 
    { 
      g1 d a d
      g1 d b
    }
  >>
}

ChorusTwo = 
{
  <<
    \relative c''
    { 
      r1^"CHORUS 2" \RunRunRun r1 \RunRunRun r1 \RunRunRun r1      
    }
    \chords 
    { 
      g1 d a d
      g1 d b
    }
  >>
}



ShortBreak =
{  
  
    {\compressFullBarRests
      R1*4^"Gtr"
    \SaxB}
}

LongBreak =
{
  <<
    \relative c'' { g1 a c d \bar "||" g a c d \break }
    \chords { g1 a c d  g a c d}
  >>
}




SaxSolo = 
{
  <<
    \relative c'
    { 
      r8^"Sax solo" c16 c e8 g a g r4
      r8 g b a b a g a
      r8 c,16 c e8 g a g r8 g
      b8 d e g~g2
      r8 c,,16 c e8 g a g r8 g
      r8 g b a b a g a
      r8 c,16 c e8 g a g r8 g
      a8 cis e a~a2~a1
      
    }
    \chords { c1 g c g \bar "||"c g c a a }
  >>
}


OutroStart =
{ 
  <<
    \relative c'' \repeat volta2
    { 
      \RunRunRun r1 \RunRunRun e8^"ad lib" f r c b a r4^"x2"
    }
    \chords {d1 g d a}
  >>
}


OutroOut =
{
  \repeat volta2 
  {<<
    \relative c' 
    {
      r8^"Repeat & fade" d16 d fis8 a b a r4 r1
      r8 d,16 d fis8 a b a r4 r1
    }
    \chords {d1*4 }
  >>}
}

Bbsaxpart =
{
  \tempo 4 = 124
  \key e \major
  \time 4/4

  %\SaxA \break
  %\SaxB \break
  
  \Intro \break
  \SaxA \break
  \SaxB \break

  \Verse \break
  \ChorusOne \break
  \ShortBreak \break
  \ChorusTwo \break

  \SaxSolo \break
  
  \OutroStart
  \OutroOut

}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}

    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Thorn In My Side"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart
    %\midi{}
}

