
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Can't Stop The Feeling"
  subtitle = "V1"
}
#(set-global-staff-size 23)

Intro =
{<< 
     \new Voice = "first"
     {\voiceOne \relative c''{ d1^"Intro" b g a }}
     \new Voice = "second"
     {\voiceTwo \relative c'{  d1 d d d  }} 
>>}

Chorus = 
{<< 
     \new Voice = "first"
     {\voiceOne \relative c''{ 
       r1 b1^"Chorus" a d cis^"a or g#?" f e f~f r2 r4 b,8 b a fis~fis2 b8 b a b~b2 
       b8 b a fis~fis2 b8 b a d~d2 b8 b a fis~fis2 b8b a d~d2 b8 b a fis~fis2 r4
     }}
     \new Voice = "second"
     {\voiceTwo \relative c''{ 
       r1_"under the"\bar "||"g1_"LIGHTS" fis b a d c bes~bes r2 r4 b8 b a fis~fis2 b8 b a b~b2 
       b8 b a fis~fis2 b8 b a d~d2 b8 b a fis~fis2 b8b a d~d2 b8 b a fis~fis2 r4
     }} 
>>}

PostChorusOne = 
{<< 
     \new Voice = "first"
     {\voiceOne \relative c'''{r4^"PostChorus 1"_"FEELING" d2. b2. r4 r8 d, d4 b8 r r4 r8 a \tuplet 3/2 {b8 cis d} e8 d d4
     }}
     \new Voice = "second"
     {\voiceTwo \relative c'''{r4 d2. b2. r4 r8 d, d4 b8 r r4 r8 a \tuplet 3/2 {b8 cis d} e8 d d4 
       
     }} 
>>}

PostChorusTwo = 
{<< 
     \new Voice = "first"
     {\voiceOne \relative c'''{
       r4^"PostChorus 2"_"FEELING" d2. b2. r4 r8 d, d4 b8 r r4 \tuplet 3/2 {b8 cis d} d8 b d4 e
       r4 d'2. b2. r4 r8 d, d4 b8 r r4 r8 a \tuplet 3/2 {b8 cis d} e8 d d4
     }}
     \new Voice = "second"
     {\voiceTwo \relative c'''{ 
       r4 d2. b2. r4 r8 d, d4 b8 r r4 \tuplet 3/2 {b8 cis d} d8 b d4 e
       r4 d'2. b2. r4 r8 d, d4 b8 r r4 r8 a \tuplet 3/2 {b8 cis d} e8 d d4       
     }} 
>>}



Bbsaxpart =
{
  \tempo 4 = 115
  \key b \minor
 
  \Intro \break
  \Chorus \break
  \PostChorusOne \break
  \PostChorusTwo \break
  
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Can't Stop The Feeling"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

