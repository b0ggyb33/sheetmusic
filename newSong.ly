\version "2.18.2"
\header{
	title = "New Song"
}

bridge ={ a8^"sax fill thing" a4. c8 c4. a4-. a4 c4-. c4 d8
          d,4-. d4 e8 e8 g4-. g4 a4-. a4 c8}

altopart = {
    %\tempo 4 = 80
    \time 4/4
    \clef treble
    \key f \major

    \bridge
}

\score
{
    \new Staff \with { instrumentName = #"Alto Sax"}
    \relative c'' {
        \altopart
	}
}

\score
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \transpose c f \relative c ''{\altopart}

}

\score
{
  \new Staff \with { instrumentName = #"Keyboard"}
  \transpose f aes \relative c ''{\altopart}

}
