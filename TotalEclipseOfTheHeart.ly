
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Total Eclipse of the Heart"
  subtitle = "V1"
}
#(set-global-staff-size 25)



Intro = 
{<< 
    \set Score.skipBars = ##t   {R2*4}
  >>}
  
UnisonSection = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r2\mf bes g a \bar "||"bes g g a \bar "||" bes g g a
      }}    
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r2 bes g a bes g g a bes g g a
      }}
    }
  >>
}  

HarmonySection = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        \bar "||" bes2\f\< c d e c'8. c16 e8 d16 c16 \!
        \bar "||" d,2 c bes c 
        c16 \< d e f g a b c  \bar "." c,^"Chorus 2 only" d e f g a b c\ff \!  
        \bar "." d2 \> c,4  d ees2 c\mf  
        \bar "||" d2 \! \>  \relative c\!
      }}    
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        g2 a bes c~c bes a g g 
        c16 d e f g a b c c,, d e f g a b c
        bes2 a4 bes4 bes2 a bes2
      }}
    }
  >>
}  
Chorus = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c''' 
      {
        \UnisonSection
        \HarmonySection
        r2  r2 r2
        r2 \bar "||"r2 r2 \bar "||" r2  r2 r2 r2\bar "||"r2
        r2 r2 r2 \bar "||"r2  r2 \bar "||" r2 r2 r2
        r2 r2 r2  

      } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c'''
      {

      }}
    }
    {
      \chords 
      { 
         bes2 g:m ees f 
         bes g:m ees f 
         bes g:m ees f
         ees2 f g:m c c 
         bes2 f g:m c c c
         bes2 f4:/a bes es2
         f2 
         bes2 g:m d:m ees es:/f
         c2:m f bes g:m es f 
         bes2 g:m d:m ees4 es:/f
         c2:m f bes g:m es f bes2*2
      } 
    }
  >>
}



Inst = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        c8 d ees4~ees2 f4 ees d c
        c8 d ees4~ees2 f4 ees d c
        ees8 f g4~g2 aes4 g f ees
        ees8 f g4~g2 aes4 g f ees
      }}    
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c'
      {
        ees8 f g4~g2 aes4 g f ees
        ees8 f g4~g2 aes4 g f ees
        g8 aes bes4~bes2 c4 bes aes g
        g8 aes bes4~bes2 c4 bes aes g
      }}
    }
    {\chords 
     { 
       c2*2:m bes c2*2:m bes
       es2*2 des es des
     } 
    }
  >>
}

Outro = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
        r4 d' g,8 g r4 d' d8 g,4 a8 b4 
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
      }  }  
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
        r4 d' g,8 g r4 d' d8 g,4 a8 b4 
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1^" etc .."
                
      }}
    }
    {\chords {} 
    }
  >>
}

Verse =
{
  << 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r2 r2 r2 r2  r2 r2 r2 r2
        r2 r2 r2 r2  r2 r2 r2 r2
        \bar "||" r2^"Verse 2 start here" r2 r2 r2  r2 r2 
      } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
                
      }}
    }
    {\chords 
     {
       c2*2:m bes
       c2*2:m bes
       ees2*2 des
       ees2*2 des
       ges2 b2*2
       ges2 b2*2
     } 
    }
  >>
}

SynthSolo =
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}


Bbsaxpart =
{
  \time 2/4
  \tempo 4 = 68
  \key c \minor


  \relative c^"Intro"  \Intro
  
  \bar "||" \relative c^"Verse 1" \Verse
  \bar "||" \relative c^"Chorus" \Chorus
  \bar "||" \relative c^"Inst" \Inst


}

\score 
  {
    \new Staff \with {instrumentName = #"ALTO"}  
    \transpose d a, \Bbsaxpart 
    
}

\pageBreak
\markup {"Total Eclipse of the Heart"}

\score 
  {
    \new Staff \with { instrumentName = #"TENOR"}
    \Bbsaxpart 
    %\midi {}

}

