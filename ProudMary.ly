
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Proud Mary"
  subtitle = "V1.1"
}
#(set-global-staff-size 26)

Intro =
{

  \bar ".|:"r1^"Intro: slow ad lib" r1 r1 r1
}

SlowVerseChorus =
{
  \bar ":|.|:"r1^"Slow Verse 1 & 2 & Chorus as below" r1 r1 r1
}

SlowEnding =
{
  \relative c' {\bar ":|.|:" fis2_"river" e4 r \bar":|." }
}

LinkToFast =
{
  r1^"1-2-3-4"
  \relative c''
  {e1^"at speed"~e4 r8 cis~cis b gis (b) e,1~e4 r8 cis'8~cis b gis (b) e,4 r8 cis'8~cis b gis (b) e,4 r cis'8 b r4 
  }
}

BigWheel = 
{
  << 
    \new Voice = "first"
    {\voiceOne \relative c''{
      fis1~fis2. g4  gis1 e1
    }}
    \new Voice= "second" 
    {\voiceTwo \relative c''{
      b1~b2. c4 cis1 a1
    }}
  >>
 
}

Verse = 
{
  \bar".." \mark #1
  \relative c'' 
  {e,4^"Verse 3 & 4" r r2 r1 r1 r1 \bar "||"
   r1 r1 r1 r8 gis 4 a bes r8 \bar "||"
   \BigWheel \bar ".|:"
   r2 gis'4 fis8 r r2 gis4 fis8 r
   r2 r4 r8 gis~gis gis fis e fis4 e8 r \bar":|."
  }
}

Bridge =
{ \relative c''
  { r1^"Bridge" r1 r d8 d r b~b2 d8 d r b~b2
    d8 d r4 b a  g g e16 g8. a4 e1
    r4 r8 fis gis b cis e~ e1~ e2^"back to A V4 "_"& then to V5" cis8 b r4 \bar".."
    
  }
}



VerseThree = 
{
  r1^"Verse 5" r1 r1 
  <<
    \new Voice = "first"
    {\voiceOne \relative c''{
       r2 r4 b8 cis \bar "||" e4 r r b8 cis e4 r r b8 cis e4 r e  r e e e dis
    }}
    \new Voice= "second" 
    {\voiceTwo \relative c'{
      r2 r4 fis8 gis b4 r r fis8 gis b4 r r fis8 gis b4 r b r b b b bes  
    }}
  >>
}





Bbsaxpart =
{
  \tempo 4 = 170
  \key e \major
  
  \Intro
  \SlowVerseChorus
  \SlowEnding
  \LinkToFast
  \bar ".|."
  \Verse \Bridge
  \VerseThree
  \BigWheel
 
  \repeat volta 4
  {\relative c'' {r2 gis'4 fis8 r r2 gis4 fis8 r r2 r4 r8 gis~}}
  \alternative
  {
    {\relative c''' {gis gis fis e fis4 e8 r}}
    {\relative c'' {r4 gis a ais}}
  }
  \relative c'' {b gis a ais b gis a ais b cis d dis e1\fermata}
  
}

\score 
{
    \new Staff \with { instrumentName = #"ALTO"}  
    \transpose d a \Bbsaxpart  
}
\pageBreak
\markup {"Proud Mary"}


\score 
{
    \new Staff \with { instrumentName = #"TENOR"}
    \Bbsaxpart
    %\midi{}
}

