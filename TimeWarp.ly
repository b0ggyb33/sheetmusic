
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Time Warp"
  subtitle = "V1"
}

Intro =
{ r1^"Intro: gtr & drum taps" r1 r1 r1}

Verse = 
{\repeat volta 4
  {<<
    {\chords {b1*2 cis1*2 } }
    {\relative c' { fis1^"V2 play pp ad lib"^"Verse: V1play 3rd/4th rpt"~fis gis~
                  gis  }}
  >>}
  \alternative
  {
    {<<
     {\chords {a1*2 b1*2} }
     {\relative c'' {a1~a  b1~b4 b^"Play rpt 2" a gis_"x4" }}
    >>}
    {}
  }
}

LetsDoTOCoda =
{ 
  \repeat volta 2
  {<<
    {\chords {g1 d1 } }
    {\relative c'' { b2^"Lets do ..." a cis b}}
  >>}
  \alternative
  {
    {\relative c'' { b1~ b} }
    {\relative c'' { r4 b gis8 fis e (fis) gis b~b2 r4^\coda} }
  }
}

LetsDoDS =
{ 
  \repeat volta 2
  {<<
    {\chords {g1 d1 } }
    {\relative c'' { b2^"Lets do ..." a cis b}}
  >>}
  \alternative
  {
    {\relative c'' { b1~ b} }
    {\relative c'' { r4 b gis8 fis e (fis) gis^"D.S. al Coda" b~b2 r4} }
  }
}

LetsDo =
{ 
  \repeat volta 2
  {<<
    {\chords {g1 d1 } }
    {\relative c'' { b2^"Lets do ..." a cis b}}
  >>}
  \alternative
  {
    {\relative c'' { b1~ b} }
    {\relative c'' { r4 b gis8 fis e (fis) gis b~b2 r4} }
  }
}

LetsDoEnd =
{ 
  \repeat volta 2
  {<<
    {\chords {g1 d1 } }
    {\relative c'' { b2^"Lets do ..." a cis b}}
  >>}
  \alternative
  {
    {\relative c'' { b1~ b} }
    {\relative c'' { r4 b gis8 fis e ees d b^"End in heap!"~b2 r4} }
  }
}



ItsJust = 
{
  \repeat volta 2
  {<< 
    {\chords {r1 fis1*2 b1} }
    {\relative c'{r4^"^jump to the .." r_"^with your" r r r4^"^gtr" fis4 gis8 b dis fis~fis1 r4 fis fis8 dis cis b} }
  >>}
  << 
    {\chords {b1 e1*2 b1*2} }
    {\relative c''{r1 b4 gis8 r b4 gis8 r b4 cis8 r r2 b4 gis8 r b4 gis8 r fis1 } }
  >>
}


Bity =
{ \relative c'
  { fis4 gis8 r fis4 gis8 r fis4 gis8 fis~ fis4 r4  }
}

CodaStart = 
{
  \relative c''^\coda \relative c''^"12 Bar Verse"
  { \repeat volta 2 {\Bity} \relative c'' {b4 cis8 r b4 cis8 r b4 cis8 b8~ b4 r4} \Bity
    \relative c'' {cis4 dis8 r cis4 dis8 r b4 cis8 b~ b4  r} \Bity}
  
}


Bbsaxpart =
{
  \tempo 4 = 170
  \key b \major
  
  \Intro
  \relative c^\segno 
  \Verse
  \LetsDoTOCoda 
  \ItsJust
  \LetsDoDS
  \CodaStart
  \LetsDoDS
  \ItsJust
  \LetsDoEnd
}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a \Bbsaxpart  
}
\pageBreak
\markup {"Time Warp"}


\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart
    %\midi{}
}

