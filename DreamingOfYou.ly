
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Dreaming of You"
  subtitle = "V2.1"
}
#(set-global-staff-size 24)

Intro = 
{ r1^"Intro: 4 bars bass" r r r
}

Bridge = 
{<< 
    \relative c'''
    \new Voice = "first"
    { 
      \voiceOne  b1\segno^\markup { \bold \with-color #red"Bridge 1"}~b2. a4 g1~g2. e'4 fis1~fis2. e4 d1~d2. r4
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo
      b4. b8 fis4 r b4. b8 fis4 r g4. g8 d4 r g4. g8 d4 r  
      b'4. b8 fis4 r b4. b8 fis4 r g4. g8 d4 r g4. g8 d4 a'
    }
 >>
}


Outro = 
{
   << 
    \relative c''
    \new Voice = "first"
    { \voiceOne \bar "||" d1^"Outro Ooohs Verse"  ~d2. cis4 b1 b2 cis \bar "||"
    d1~d2. cis4 b1 b2 cis \bar "||"}
    \relative c''
    \new Voice= "second"
    { \voiceTwo 
      b1 ~b2. a4 g1 g2 a b1 ~b2. a4 g1 g2 a}
    >> 
    
    {<< 
      \relative c''
      \new Voice = "first"
      { \voiceOne \bar "||"
        d1^"Outro Chorus" r2 r4 d4 (cis1) r2 r4 cis (b1) r2 r4 cis
      }
      \relative c''
      \new Voice= "second"
      { \voiceTwo 
          d1 r2 r4 d4 (cis1) r2 r4 cis (b1) r2 r4 cis
      }
    >>}
    
    {<< 
      \break
      \relative c'''
      \new Voice = "first"
      { \voiceOne  b1~b
        g~g b   g4._"   STOP!" fis8~fis2 }
      \relative c''
      \new Voice= "second"
      { \voiceTwo b4. b8 fis4 r b4. b8 fis4 r g4. g8 d4 r g4. g8 d4 r b'4. b8 fis4 r g4. fis8~fis2}
    >>}
}

Verse-Chorus =
{
     << 
    \relative c''
    \new Voice = "first"
    { \voiceOne \bar "||" r1  r r b2 cis \bar ":|."
      
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo 
      b4^"Verse" r r2 r1 r g2 a
      
    }
    >>  

    << 
    \relative c''
    \new Voice = "first"
    { \voiceOne \bar ".|:" d1^"V2 rpt x3 ..." ~d2. r4 r1 b2^"... rpt x3 V2" cis \bar ":|."
      d1^"Chorus" r2 r4 d4 (cis1) r2 r4 cis (b1) r2 r4 cis
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo 
      b1 ~b2. r4 r1 g2 a
      d1 r2 r4 d4 (cis1) r2 r4 cis (b1) r2 r4 cis
    }>>
}

GtrSolo = 
{
  \relative c^"Gtr solo"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
  \relative c^"cont over chorus"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
  {<< 
    \relative c'''
    \new Voice = "first"
    { \voiceOne  g4. fis8~fis2 
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo g4. fis8~fis2
    }
  >>} 

  
}



VerseChorus = 
{ \repeat volta 2 
  { 
    << 
    \relative c''
    \new Voice = "first"
    { \voiceOne \bar ".|:" d1 ~d2. cis4_"<-Play Ohs" b1_" ->" b2^"Rpt x3 V2" cis \bar ":|."
      d1^"Chorus"_"Play except Gtr solo" r2 r4 d4 (cis1) r2 r4 cis (b1) r2 r4 cis
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo 
      b1 ~b2. a4 g1 g2_"Play except"_" Gtr solo" a
      d1 r2 r4 d4 (cis1) r2 r4 cis (b1) r2 r4 cis
    }
    >> |  
  }
  \alternative 
  {
    {<< 
      \relative c'''
      \new Voice = "first"
      { \voiceOne  b1^"Ending for C1 & C3"~b2. a4
        g1~g2. a4 b1   g4.^"C3 back to V4"^"C1 continue to bridge"_"play always" fis8~fis2 \bar "||"
      }
      \relative c''
      \new Voice= "second"
      { \voiceTwo fis1~fis2. e4
        d1~d2. e4  fis1 g4. fis8~fis2
      }
      >> |  
      \Bridge
   }
    
   {<< 
     \break
     \relative c'''
     \new Voice = "first"
     { \voiceOne  b1^"Ending for C2 & C4"~b
        g~g b   g4._"   STOP! C4" fis8~fis2 
        g4. fis8~fis2 g'4. fis8~fis2 g4.^"to V3+C3 = gtr solo" fis8~fis2 \bar "||"}
     \relative c''
     \new Voice= "second"
     { \voiceTwo b4. b8 fis4 r b4. b8 fis4 r g4. g8 d4 r g4. g8 d4 r b'4. b8 fis4 r g4. fis8~fis2
     g4. fis8~fis2 g'4. fis8~fis2 g4. fis8~fis2 
     }
    >> |  
   }
 }
}



Bbsaxpart =
{
  \tempo 4 = 200
  \key b \minor  

  \Intro 
  \repeat volta 2
  {
    \Bridge
    \Verse-Chorus
  }
  \alternative
  {
    {<< 
      \relative c'''
      \new Voice = "first"
      { \voiceOne  b1~b2. a4
        g1~g2. a4 b1   g4. fis8~fis2 
      }
      \relative c''
      \new Voice= "second"
      { \voiceTwo fis1~fis2. e4
        d1~d2. e4  fis1 g4. fis8^\markup { \bold \with-color #red"DS"}~fis2
      }
    >>} 
   
 
     {
      {<< 
        \break
        \relative c'''
        \new Voice = "first"
        { \voiceOne  b1~b
           g~g b   g4. fis8~fis2 
           g4. fis8~fis2 g'4. fis8~fis2 g4. fis8~fis2 \bar "||"}
        \relative c''
        \new Voice= "second"
       { \voiceTwo b4. b8 fis4 r b4. b8 fis4 r g4. g8 d4 r g4. g8 d4 r b'4. b8 fis4 r g4. fis8~fis2
       g4. fis8~fis2 g'4. fis8~fis2 g4. fis8~fis2 
       }
     >>}     
    }
  }
  \GtrSolo
  \Outro
}

\score 
{
  \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
  \transpose d a, \Bbsaxpart 
  %\midi{}
}

\pageBreak
\markup { "Dreaming of You"  }
\score 
{
    
  \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
  \Bbsaxpart   

}

