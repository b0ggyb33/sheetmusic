
\version "2.18.2"
% automatically converted by musicxml2ly from D:\Users\Richard\Documents\Band Lemon Dogs\Tabs etc\AAA MuseScore Files\Heatwave\Heatwave.mxl

\header {
  encodingsoftware = "MuseScore 3.6.2"
  encodingdate = "2023-04-20"
  title = Heatwave
}

#(set-global-staff-size 21.0550393701)
\paper {
  paper-width = 21.0\cm
  paper-height = 29.7\cm
  top-margin = 1.5\cm
  bottom-margin = 1.5\cm
  left-margin = 1.5\cm
  right-margin = 1.5\cm
}
\layout {
  \context {
    \Score
    skipBars = ##t
    autoBeaming = ##f
  }
}
PartPOneVoiceOne =  {
  \repeat volta 2 {
    \repeat volta 2 {
      \transposition bes, \clef "treble" \key g \major
      \numericTimeSignature\time 4/4 | % 1
      \tempo 4=160 R1 | % 2
      R1*2 | % 4
      r4 _"Gtr etc" -"ALTO" g''8 [ a''8 ] r8 g''8 [ e''8 ] r8 \bar
      "||"
      d''8 _"Instrumental intro" r8 r8 d''8 ~ d''4 r4 | % 6
      e''8 r8 r8 e''8 ~ e''4 d''4 | % 7
      e''16 [ d''8. ] r8 r8 d''4 c''4 | % 8
      a'2 r4 r4 \bar "||"
      \break | % 9
      d''8 r8 r8 d''8 ~ d''4 c''4 | \barNumberCheck #10
      e''8 r8 r8 e''8 ~ e''4 d''4 | % 11
      a'2 r4 r4 | % 12
      \times 2/3  {
        e''4 g''4 a''4
      }
      \times 2/3  {
        a''4 g''4 e''4
      }
      \bar "||"
      d''8 r8 r8 d''8 ~ d''4 r4 | % 14
      \times 2/3  {
        e''4 g''4 g''4
      }
      \times 2/3  {
        g''4 e''4 d''4
      }
      \break | % 15
      c''8 r8 r8 c''8 ~ c''4 r4 | % 16
      \times 2/3  {
        fis''4 g''4 g''4
      }
      g''4. r8 \bar "||"
      c''8 r8 r8 c''8 ~ c''4 r8 c''8 | % 18
      c''8 r8 r8 c''8 ~ c''4 c''4 | % 19
      c''8 r8 r8 c''8 ~ c''4 r8 c''8 \break | \barNumberCheck #20
      c''8 r8 g''8 [ a''8 ] r8 g''8 [ e''8 ] r8 \bar "||"
      \break \repeat volta 2 {
        | % 21
        \mark \markup { \musicglyph #"scripts.segno" } | % 21
        d''8 _"1st - V1 tacit 2nd -V2 play 3rd - Sax Solo 4th -
                V3 play 5th - outro" r8 r8 d''8 ~ d''4 r4 | % 22
        e''8 r8 r8 e''8 ~ e''4 r4 | % 23
        a'8 r8 r8 a'8 ~ a'4 r4 | % 24
        a'8 r8 r8 a'8 ~ a'4 r4 \break | % 25
        d''8 r8 r8 d''8 ~ d''4 r4 | % 26
        e''8 r8 r8 e''8 ~ e''4 r4 | % 27
        a'8 r8 r8 a'8 ~ a'4 r4 | % 28
        a'8 r8 r8 a'8 ~ a'4 r4 \break | % 29
        d''8 r8 r8 d''8 ~ d''4 r4 | \barNumberCheck #30
        e''8 r8 r8 e''8 ~ e''4 r4 | % 31
        f''8 r8 r8 f''8 ~ f''4 r4 | % 32
        g''8 r8 r8 g''8 ~ g''4 r4 \break | % 33
        c'''4 ^"D.S." _"Play" c'''4 c'''4 a''4 | % 34
        c'''4. a''8 c'''4 r4 | % 35
        c'''4 c'''4 c'''4 a''4 | % 36
        c'''4. _"On Solo D. S. here if LR version" d'''8 c'''4 r4
        \break | % 37
        c'''4 c'''4 c'''4 a''4 | % 38
        c'''4. a''8 c'''4 r4 | % 39
        c'''4 c'''4 c'''4 d'''4
      }
      \alternative {
        {
          | \barNumberCheck #40
          c'''8 r8 g''8 [ a''8 ] r8 g''8 [ e''8 ] r8
        }
      } \break
    }
    \alternative {
      {
        | % 41
        d''8 r8 r8 d''8 ~ d''4 r4
      }
    } | % 42
    e''8 r8 r8 e''8 ~ e''4 r4 | % 43
    a'8 r8 r8 a'8 ~ a'4 ^\fermata r4 \bar "|."
    \pageBreak | % 44
    \key f \major | % 44
    R1*3 | % 47
    r4 _"Gtr etc" -"TENOR" -"Heatwave" c''8 [ d''8 ] r8 c''8 [ a'8 ]
    r8 \bar "||"
    g'8 _"Instrumental intro" r8 r8 g'8 ~ g'4 r4 | % 49
    a'8 r8 r8 a'8 ~ a'4 g'4 | \barNumberCheck #50
    a'16 [ g'8. ] r8 r8 g'4 f'4 \break | % 51
    d'2 r4 r4 \bar "||"
    g'8 r8 r8 g'8 ~ g'4 f'4 | % 53
    a'8 r8 r8 a'8 ~ a'4 g'4 | % 54
    d'2 r4 r4 | % 55
    \times 2/3  {
      a'4 c''4 d''4
    }
    \times 2/3  {
      d''4 c''4 a'4
    }
    \bar "||"
    g'8 r8 r8 g'8 ~ g'4 r4 \break | % 57
    \times 2/3  {
      a'4 c''4 c''4
    }
    \times 2/3  {
      c''4 a'4 g'4
    }
    | % 58
    f'8 r8 r8 f'8 ~ f'4 r4 | % 59
    \times 2/3  {
      bes'4 c''4 c''4
    }
    c''4. r8 \bar "||"
    f'8 r8 r8 f'8 ~ f'4 r8 r8 | % 61
    f'8 r8 r8 f'8 ~ f'4 f'4 \break | % 62
    f'8 r8 r8 f'8 ~ f'4 f'4 | % 63
    f'8 r8 c''8 [ d''8 ] r8 c''8 [ a'8 ] r8 \bar "||"
    \break \repeat volta 2 {
      | % 64
      \mark \markup { \musicglyph #"scripts.segno" } | % 64
      g'8 _"1st - V1 tacit 2nd -V2 play 3rd - Sax Solo 4th - V3
            play 5th - outro" r8 r8 g'8 ~ g'4 r4 | % 65
      a'8 r8 r8 a'8 ~ a'4 r4 | % 66
      d'8 r8 r8 d'8 ~ d'4 r4 | % 67
      d'8 r8 r8 d'8 ~ d'4 r4 \break | % 68
      g'8 r8 r8 g'8 ~ g'4 r4 | % 69
      a'8 r8 r8 a'8 ~ a'4 r4 | \barNumberCheck #70
      d'8 r8 r8 d'8 ~ d'4 r4 | % 71
      d'8 r8 r8 d'8 ~ d'4 r4 \break | % 72
      g'8 r8 r8 g'8 ~ g'4 r4 | % 73
      a'8 r8 r8 a'8 ~ a'4 r4 | % 74
      bes'8 r8 r8 bes'8 ~ bes'4 r4 | % 75
      c''8 r8 r8 c''8 ~ c''4 r4 \break | % 76
      f''4 ^"D.S." _"Play" f''4 f''4 d''4 | % 77
      f''4. d''8 f''4 r4 | % 78
      f''4 f''4 f''4 d''4 | % 79
      f''4. _"On Solo D. S. here if LR version" g''8 f''4 r4
      \break | \barNumberCheck #80
      f''4 f''4 f''4 d''4 | % 81
      f''4. d''8 f''4 r4 | % 82
      f''4 f''4 f''4 g''4
    }
    \alternative {
      {
        | % 83
        f''8 r8 c''8 [ d''8 ] r8 c''8 [ a'8 ] r8
      }
    } \break
  }
  \alternative {
    {
      | % 84
      g'8 r8 r8 g'8 ~ g'4 r4
    }
  } | % 85
  a'8 r8 r8 a'8 ~ a'4 r4 | % 86
  d'8 r8 r8 d'8 ~ d'4 ^\fermata r4 \bar "|."
}

PartPOneVoiceOneChords =  \chordmode {
  \repeat volta 2 {
    \repeat volta 2 {
      | % 1
      s1 | % 2
      s1 s1 | % 4
      s4 s8 s8 s8 s8 s8 s8 \bar "||"
      d8:m7 s8 s8 s8 s4 s4 | % 6
      e8:m7 s8 s8 s8 s4 s4 | % 7
      a16:m7 s8. s8 s8 s4 s4 | % 8
      s2 s4 s4 \bar "||"
      d8:m7 s8 s8 s8 s4 s4 | \barNumberCheck #10
      e8:m7 s8 s8 s8 s4 s4 | % 11
      a2:m7 s4 s4 | % 12
      s1*1/6 s1*1/6 s1*1/6 s1*1/6 s1*1/6 s1*1/6 \bar "||"
      d8:m7 s8 s8 s8 s4 s4 | % 14
      e4*2/3:m7 s1*1/6 s1*1/6 s1*1/6 s1*1/6 s1*1/6 | % 15
      f8:5 s8 s8 s8 s4 s4 | % 16
      g4*2/3:7 s1*1/6 s1*1/6 s4. s8 \bar "||"
      s8 s8 s8 s8 s4 s8 s8 | % 18
      s8 s8 s8 s8 s4 s4 | % 19
      s8 s8 s8 s8 s4 s8 s8 | \barNumberCheck #20
      s8 s8 s8 s8 s8 s8 s8 s8 \bar "||"
      d8:m7 s8 s8 s8 s4 s4 | % 22
      e8:m7 s8 s8 s8 s4 s4 | % 23
      a8:m7 s8 s8 s8 s4 s4 | % 24
      s8 s8 s8 s8 s4 s4 | % 25
      d8:m7 s8 s8 s8 s4 s4 | % 26
      e8:m7 s8 s8 s8 s4 s4 | % 27
      a8:m7 s8 s8 s8 s4 s4 | % 28
      s8 s8 s8 s8 s4 s4 | % 29
      d8:m7 s8 s8 s8 s4 s4 | \barNumberCheck #30
      e8:m7 s8 s8 s8 s4 s4 | % 31
      f8:5 s8 s8 s8 s4 s4 | % 32
      g8:7 s8 s8 s8 s4 s4 | % 33
      c4:5 s4 s4 s4 | % 34
      s4. s8 s4 s4 | % 35
      s4 s4 s4 s4 | % 36
      s4. s8 s4 s4 | % 37
      c4:5 s4 s4 s4 | % 38
      s4. s8 s4 s4 | % 39
      s4 s4 s4 s4
    }
    \alternative {
      {
        | \barNumberCheck #40
        s8 s8 s8 s8 s8 s8 s8 s8
      }
      {
        | % 41
        s8 s8 s8 s8 s4 s4
      }
    } | % 42
    s8 s8 s8 s8 s4 s4 | % 43
    s8 s8 s8 s8 s4 s4 \bar "|."
    s1 s1 s1 | % 47
    s4 s8 s8 s8 s8 s8 s8 \bar "||"
    g8:m7 s8 s8 s8 s4 s4 | % 49
    a8:m7 s8 s8 s8 s4 s4 | \barNumberCheck #50
    d16:m7 s8. s8 s8 s4 s4 | % 51
    s2 s4 s4 \bar "||"
    g8:m7 s8 s8 s8 s4 s4 | % 53
    a8:m7 s8 s8 s8 s4 s4 | % 54
    d2:m7 s4 s4 | % 55
    s1*1/6 s1*1/6 s1*1/6 s1*1/6 s1*1/6 s1*1/6 \bar "||"
    g8:m7 s8 s8 s8 s4 s4 | % 57
    a4*2/3:m7 s1*1/6 s1*1/6 s1*1/6 s1*1/6 s1*1/6 | % 58
    bes8:5 s8 s8 s8 s4 s4 | % 59
    c4*2/3:7 s1*1/6 s1*1/6 s4. s8 \bar "||"
    f8:5 s8 s8 s8 s4 s8 s8 | % 61
    s8 s8 s8 s8 s4 s4 | % 62
    s8 s8 s8 s8 s4 s4 | % 63
    s8 s8 s8 s8 s8 s8 s8 s8 \bar "||"
    g8:m7 s8 s8 s8 s4 s4 | % 65
    a8:m7 s8 s8 s8 s4 s4 | % 66
    d8:m7 s8 s8 s8 s4 s4 | % 67
    s8 s8 s8 s8 s4 s4 | % 68
    g8:m7 s8 s8 s8 s4 s4 | % 69
    a8:m7 s8 s8 s8 s4 s4 | \barNumberCheck #70
    d8:m7 s8 s8 s8 s4 s4 | % 71
    s8 s8 s8 s8 s4 s4 | % 72
    g8:m7 s8 s8 s8 s4 s4 | % 73
    a8:m7 s8 s8 s8 s4 s4 | % 74
    bes8:5 s8 s8 s8 s4 s4 | % 75
    c8:7 s8 s8 s8 s4 s4 | % 76
    f4:5 s4 s4 s4 | % 77
    s4. s8 s4 s4 | % 78
    s4 s4 s4 s4 | % 79
    s4. s8 s4 s4 | \barNumberCheck #80
    f4:5 s4 s4 s4 | % 81
    s4. s8 s4 s4 | % 82
    s4 s4 s4 s4
  }
  \alternative {
    {
      | % 83
      s8 s8 s8 s8 s8 s8 s8 s8
    }
    {
      | % 84
      s8 s8 s8 s8 s4 s4
    }
  } | % 85
  s8 s8 s8 s8 s4 s4 | % 86
  s8 s8 s8 s8 s4 s4 \bar "|."
}


% The score definition
\score {
  <<
    \context ChordNames = "PartPOneVoiceOneChords" \PartPOneVoiceOneChords
    \new Staff <<
      \set Staff.instrumentName = "Tenor Saxophone"
      \set Staff.shortInstrumentName = "T. Sax."
      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
      >>
    >>

  >>
  \layout {}
  % To create MIDI output, uncomment the following line:
  %  \midi {}
}

