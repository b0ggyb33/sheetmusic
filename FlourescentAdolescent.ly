
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Fluorescent Adolescent"
  subtitle = "V1"
}

IOne =
{ \bar "||"
  <<\relative c''
  {d8^"" d d r r2  b8 b b r r2  r8 e e r r4 r8 e  a,8 a a r r2}
  \chords {d1 b:m e:m a}>>
 \bar "||"
}
ITwo =
{ \bar "||"
  <<\relative c''
  {d8^"" d d r r2  b8 b b r r2  e8 e e r r4 r8 e  a,8 a a r r2}
  \chords {d1 b:m e:m a}>>
 \bar "||"
}
IThree =
{ \bar "||"
  <<\relative c''
  {d8^"" d d r r2  b8 b b r r2  e8 e e r r4 r8 e  a,8 a a a a a a a a8 a a a a a a a}
  \chords {d1 b:m e:m a}>>
 \bar "||"
}


VerseOne =
{<<
  \chords {d1 b:m e:m a d1 b:m e:m a e:m a e:m a e:m a e:m g }
  \relative c''
  {
    {r1^"Verse 1" r r r    d b r8 e e r r2 r8 a, a r r2
    e'1  a,1 e'1 a,1
    e'8 e e r r2 r8 a, a r r2 
    <<
      {e'2. r4 d2. r4 }
      {b2.  r4 b2. r4 }
    >>
    \bar "||" r1 \bar "||"}
  } \bar "||"
>>}

VerseTwo =
{
  \relative c''
  {
    r1^"Verse 2" r r r    d b e8 e e r r2 r1
    e1 a e a r8 e e4 r2  r8 a, a4 r2 
    <<
      {e'2. d4 d1}
      {b2. a4 b1}
    >>  
    %e1 a e4 r r2 d4 r r2
  } \bar "||"
  
}
BridgeOne =
{
  <<
    {\chords {fis1*2:m b1*2:m e1*2:m fis1:m e1:m  } }
    \relative c''
    {\bar "||" r1 r1 r1 r1 r1 r1 r1 r1 \bar "||" }
  >>
  \IOne
  \ITwo
  \IThree
}

Outro =
{<<
  {\chords {d1 b:m e:m a d b:m e:m a b:m} }
  \relative c''
  {\bar ".|:" 
   {<<
     \new Voice = "first"
     {\voiceOne d2. e4 fis1 e2. d4 cis1  d2. e4 fis1 e2 fis4 g a2 e}
     \new Voice= "second"
     {\voiceTwo a,1 b1 b1 a1 a1 b1 b1 cis1}
   >>}
   \bar ":|." 
   {<< {fis}{b} >>}
  }
>>}
  

Chorus= 
{<< \chords 
  {
    d1 f g bes
  }
  \relative c''
  \new Voice = "first"
  { \voiceOne r4^"Chorus"  d8 c8 d2 r4 f8 e f2 r4 g8 f g2 r4 bes8 a bes4 bes 
    a1 r r r
    r4 r d,8 c r4   r r r a'   a r d,8 c r4 r r d d 
    r4 r d8 c r4   r r r a'   a r d,8 c r4 r r d\glissando d, \bar "||"
  }
  \relative c'
  \new Voice= "second"
  { \voiceTwo d1 f g bes2. bes4 
    a1 r r r
    r4 r d8 c r4   r r r a'   a r d,8 c r4 r r d d 
    r4 r d8 c r4   r r r a'   a r d,8 c r4 r r d\glissando d, 
  }
 >>
}




Bbsaxpart =
{
  \tempo 4 = 112
  \key d \major  

  \IOne
  \ITwo
  \VerseOne
  \VerseTwo
  \BridgeOne
  \Outro
}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart 

}
\pageBreak
\markup {"Fluorescent Adolescent   "}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
    %\midi{}
}

