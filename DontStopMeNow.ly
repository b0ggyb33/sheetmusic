
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Don't Stop Me Now"
  opus = "28 March 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

%#(set-global-staff-size 25)

Intro =
{
  \relative c''
  { 
    r1^"Keys/Vocal"_"---" r1_"having" r1_"having"
  }
}


GtrSoloLastVerse = 
{
  <<
    \relative c''
    { 
      g'1~^"Verse 3 (starting with gtr solo)" g 
      r r r r r r 
        
           
      \relative c'' {r1 r r1
      g'1_"burning"~g r r r r r a,4 g e8 e4.  } 
    }
    \chords 
    {
      g1 b:m e:m a:m d \bar "||"
      g1 b:m e:m \break
       a:m d g \bar "||"
      \bar "||"g1:7 c a:m7 e:7 a:m7 e:7 a:m  a4:m7 g c cis:dim 
    }
  >>
}

Verse = 
{
  <<
    \relative c''
    { 
      r1^"Verse 1 & 2"_"shooting star"_"rocket ship"
      r r r r r r r 
         
      \tuplet 3/4 { c4^"play V2 only" d c d e8 fis4. } g2 r     
      \relative c'' {  
       r1_"burning" r1 r r r_"heit" r r_"light" a4 g e8 e4. } 
    }
    \chords 
    {
      g1 b:m e:m a:m d \bar "||"
      g1 b:m e:m 
      a:m d g \bar "."
      \bar "||"g1:7 c a:m7 e:7 a:m7 e:7 a:m  a4:m7 g c cis:dim 
    }
  >>
}


Chorus = 
{
  <<
    \relative c'
    { 
      d2 d8 e f fis \bar "||"
      g1^"Chorus" r a b
      g1 r a b
      a4. g fis4
      e2 r  
      a4. g fis4
      e2 r2 r1
    }
    \chords 
    { 
      d
      \tuplet 3/4 {g4 a:m b:m} e1 a:m7 d \bar "||"
      \tuplet 3/4 {g4 a:m b:m} e1 a:m7 e:7 \break
      \tuplet 3/4 {a4:m g d}  a2:m a:m7 \tuplet 3/4 {a4:m g d}  a2:m a:m7
      a1:m 
    }
  >>
}

ChorusEndOne =
{
  <<
    \relative c'
    {  
      r1 r1
    }
    \chords 
    { 
      d1 bes:6
    }
  >>
}

ChorusEndTwo =
{
  <<
    \relative c'
    {  
     bes'2^"off to keys/vox"^"Slower!"  \tuplet 3/2 {b4 c cis} d1\fermata
    }
    \chords 
    { 
      d1 bes:6
    }
  >>
}




VocalBreak =
{  
  <<
    \relative c'
    {
      d2 r^"Break" 
      r1\bar "||"r1^"Don't stop me" r4 c' cis d
      r1  f4 e-. d-- r4
      r4 r r f8 d f4 d f d
      r1 g,8 a b c d dis e fis \bar "||"
    }
    \chords {d1  r1*9  }
  >>
}





Bbsaxpart =
{
  \tempo 4 = 128
  \key g \major
  \time 4/4
  
  \Intro 
  
 
  \repeat volta2
  {
    
    \Verse \break
    
  }
  \alternative
  {
    {\Chorus \ChorusEndOne \break}
    {\VocalBreak}
  }
  \GtrSoloLastVerse
  \Chorus \ChorusEndTwo

}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Don't Stop Me Now"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

