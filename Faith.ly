
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Faith"
  %subtitle = "V1"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 10 right-margin = 10}

#(set-global-staff-size 27)

XIntro =
{
  \relative c''
  { 
    r1^"Intro" r1 a8 b d4-. d-- b-. d-- e-. d-. r  
  }
}

Hook =
{
  \repeat volta2
  {
    <<
      \relative c''
      {
        cis4^"Hook - push notes" cis cis r cis cis cis cis, 
        cis'4 cis cis r cis cis cis r
      }
      \chords {}
    >>
  }
}

Verse = 
{
  <<
    \relative c'
    { 
      \repeat volta 2
      {
        r1^"Verse" r1 r1 r1   
      }
      fis1 gis ais gis  fis gis4 ais cis,2  
      dis1 \tuplet 3/2 {dis4 f fis} gis4~\>gis4\!
    }
    \chords 
    {
      cis1*2 fis1 cis1  \break
      fis cis fis cis \break fis cis4 gis4/b bes2:m  dis1:m gis
    }
  >>
}

Bbsaxpart =
{
  \tempo 4 = 96
  \key cis \major
  \time 4/4
  

  \repeat volta3
  {
    \Verse  \break
    \Hook \break
  }


}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a \Bbsaxpart 
    %\midi {}
}
%\pageBreak
\markup {"Faith"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

