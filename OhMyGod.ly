
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Oh My God"
  subtitle = "V1"
}

Intro = 
{<< 
    \relative c'''
    \new Voice = "first"
    { 
      \voiceOne r1^"Lots of dms & gtr bars" r r r2^"or more" r8 gis b cis
      \tuplet 3/2 {d cis b} gis4 r b~b4 r r2 r4 dis2.~dis2 r8 gis, b cis
      \tuplet 3/2 {d cis b} gis4 r gis~gis4 r r2 r4 cis2.~cis2 r2 \bar "||"
    }
    \relative c''
    \new Voice= "second"
    {
      \voiceTwo r1 r r r2 r8 gis b cis
      \tuplet 3/2 {d cis b} gis4 r b~b4 r r2 r4 dis2.~dis2 r8 gis, b cis
      \tuplet 3/2 {d cis b} gis4 r gis~gis4 r r2 r4 cis2.~cis2 r2 
    }
 >>
}

Chorus = 
{ \repeat volta 4 
  { 
    << 
    \relative c'''
    \new Voice = "first"
    { \voiceOne  r2^"Chorus: after V2 & V3" gis4 r  r2 gis4 r r1 
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo 
      gis4 r r fis (gis) r r2 r1      
    }
    >> |  
  }
  \alternative 
  {
    {<< 
      \relative c'''
      \new Voice = "first"
      { \voiceOne  gis2 ais
      }
      \relative c''
      \new Voice= "second"
      { \voiceTwo e2 fis
      }
      >> |  
   |}
    
   {<< 
     \relative c''
     \new Voice = "first"
     { \voiceOne  dis8 fis4 gis b cis8 \bar "||"}
     \relative c'
     \new Voice= "second"
     { \voiceTwo dis8 fis4 gis b_"^ end" cis8}
    >> |  
   }
 }
}

  
OldSoloOne =
{<< 
    \relative c'''
    \new Voice = "first"
    { 
      \voiceOne 
      \tuplet 3/2 {d8^"First break" cis b} r4 r b~b4 gis8 b cis4 dis~dis8 dis dis2.~dis2 r8 gis, b cis
      \tuplet 3/2 {d cis b} r4 r gis~gis4 dis8 fis gis4 b~b8 b cis2.~cis4 cis2 b8 gis^"to V3" \bar "."
    }
    \relative c''
    \new Voice= "second"
    {
      \voiceTwo 
      \tuplet 3/2 {d8 cis b} r4 r b~b4 gis8 b cis4 dis~dis8 dis dis2.~dis2 r8 gis, b cis
      \tuplet 3/2 {d cis b} r4 r gis~gis4 dis8 fis gis4 b~b8 b cis2.~cis4 cis2 b8 gis 
    }
 >>
}

SoloOne = 
{<< 
    \relative c'''
    \new Voice = "first"
    { 
      \voiceOne 
      \tuplet 3/2 {d8 cis b} gis4 r b~b4 r r2^"ad lib Alto in gap" r4 dis2.~dis2 r8 gis, b cis
      \tuplet 3/2 {d8 cis b} gis4 r gis~gis4 r r2^"ad lib Alto in gap" r4 cis2.~cis2 r2 \bar "||"
    }
    \relative c''
    \new Voice= "second"
    {
      \voiceTwo 
      \tuplet 3/2 {d8 cis b} gis4 r b~b4 r r2 r4 dis2.~dis2 r8 gis, b cis
      \tuplet 3/2 {d8 cis b} gis4 r gis~gis4 r r2 r4 cis2.~cis2 r2 
    }
 >>
}

SoloTwo =
{ \relative c''
  {r4^"Second break (Kaiser Chiefs mostly)" b fis gis~gis2. b4 fis gis2 ais4~ais b2 r4  \bar "||"
   r4 b fis gis~gis2. b4 fis gis2 ais4~ais b2 cis4~   \bar "||"
   cis4 b fis gis~gis2. b4 fis gis2 ais4~ais b2 r4  \bar "||"
   r4 b fis gis~gis2. b4 fis gis2 ais4~ais b2 b8 b   \bar "||"
      b1^"Lily ragged bts" cis d dis \bar "||"
       e f ^"to Chorus with much wailing & stop" fis g \bar "||"
  }     
}
  



Verse = 
{<< 
  \relative c'''
  \new Voice = "first"
  { \voiceOne 
    dis2^"Verse: play V2 only" b gis r eis' cis  gis r
    dis'2 b gis r eis' cis  gis r
    \bar "||"
    r1 r r r  \bar "||" r r r^"some" r^"miliar" r2 b~b1 %\bar "||"
  }
  \relative c''
  \new Voice= "second"
  { \voiceTwo 
    r1 gis2 b4 gis r1 cis,2 eis4 cis
    r1 gis'2 b4 gis r1 cis,2 eis4 cis
    r1 r r r  r r r r gis''~gis
  }
 >>
}


Bbsaxpart =
{
  \tempo 4 = 180
  \key fis \major  

  \Intro 
  \Verse
  \Chorus
  \SoloOne
  \SoloTwo
}

\score 
{
  \new Staff \with { instrumentName = #"Alto Sax"}  
  \transpose d a, \Bbsaxpart 
  %\midi{}
}

\pageBreak
\markup { "Oh My God"  }
\score 
{
    
  \new Staff \with { instrumentName = #"Tenor Sax"}
  \Bbsaxpart   

}

