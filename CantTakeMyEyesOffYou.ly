
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Can't Take My Eyes Off You"
  opus = "13 April 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
\layout{ragged-last =##t}

%#(set-global-staff-size 25)

SubRiff =
{\relative c''  {d4^"Riff"-- dis-. d4-- dis-. d4-- dis8-. fis~ fis8 f8 r dis^"x3" }}

Riff =
{
  \repeat volta3 {\SubRiff}
  \relative c''  {d4-- dis-. d4-- dis-. fis1~fis2 r}
  
}


Intro =
{ 
 << \relative c''
  {
    r1^"Intro"  r
    gis4fis f8 (fis8) gis (fis8  gis4) fis4 f8  (fis8) gis (fis8 gis1)
    r1 gis4 fis f (fis) gis4 (ais2.)
  }
  \chords {fis1*4}
 >>
}

VerseA = 
{
  <<
    \relative c''
    { 
      fis1^"VerseA"~fis f~f e~e ees~ees  
      d~d des~des dis d cis2 cis4 dis fis (ais2) r4  

    }
    \chords 
    {
      fis1*2 fis:maj7 fis:7 b 
      b:m fis b1 b:m fis1*2
    }
  >>
}

VerseB = 
{
  <<
    \relative c'''
    \new Voice = "first"
    { \voiceOne
      r2 cis8-. r ais-. gis (ais2)  r2 
      r2 r4 ais8 (gis ais2)  r2
      r2 r8 dis cis (b ais2) r2  r2 r8 cis (b ais gis2) r2  
      r2 r8 gis d' (cis b2) r2  r2 r8 fis cis' (b ais2) r2  
      ais1 a ais~ ais   
    }
    \relative c''
    \new Voice = "second"
    {\voiceTwo
      fis1^"VerseB"~fis f~f e~e ees~ees  
      d~d des~des dis d\< cis fis\! 
    }
    \chords 
    {
      fis1*2 fis:maj7 fis:7 b 
      b:m fis b1 b:m fis1*2
    }
  >>
}

ChorusStart = 
{
  <<
    \relative c'''
    { 
      aes2.^"Chorus"  ges4 f1 ges2. f4 ees1
      

    }
    \chords 
    {
      aes1:m aes2:m des  ges1 ges2 ees 
    }
  >>
}
ChorusEndOne = 
{
  <<
    \relative c'''
    { 
      aes2. ges4 f2 ges4 f 
      ees4 r8 ges  {bes8 aes ges} bes   r^"On outro repeat until ..." bes r4r2

    }
    \chords 
    {
      aes1:m des2 ges ees1*2
    }
  >>
}
ChorusEndTwo = 
{
  <<
    \relative c'''
    { 
      aes1 ~\>aes ~aes^"next Verse B"~aes\!

    }
    \chords 
    {
      aes1*2:m  des
    }
  >>
}

Chorus =
{
  \repeat volta2 {\ChorusStart}
  \alternative
  {
    \ChorusEndOne
    \ChorusEndTwo
  }
}





Bbsaxpart =
{
  \tempo 4 = 120
  \key fis \major
  \time 4/4

  
  \repeat volta2 {\Intro }\break
  \VerseA \break
  \bar "[|:"\VerseB \break
  \Riff \break
  
  %\key aes \minor
  \Chorus \bar ":|]"
}


\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %midi {}
}
\pageBreak
\markup {"Can't Take My Eyes Off You"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

