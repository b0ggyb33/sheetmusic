
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Feel It Still"
  
  opus = "18 July 2022"
}


ChordsOne =
{
  \chords {ees1*2:m ges aes ees:m}
}
ChordsTwo =
{
  \chords {b1 aes ees1*2:m }
}


ChorusOne =
{
  <<
    {\ChordsOne \ChordsOne}
    \relative c'
    {
      ees8 r2.. r1  r r
      aes8 r2.. r1  ees8 r r ees8 r2 ees8 r bes4 des8 ees4 des 8
      ees8 r2.. r1  ges8 r r ges r2  r1 aes8 r2..  r1 ees8 r2.. r1
    }
  >>
}

VerseTwo =
{
  <<
    { \relative c''' { ges1~ges  ges~ges aes~aes ges~ges}}
    { \relative c'' { ees1~ees  des~des ees~ees ees~ees}}
  >>
}

ChorusTwo =
{
  \relative c'' 
  {
    r1 r1 ges8 r r ges r2  r1 aes8 r2..  r1 ees8 r r ees8 r2 ees8 r bes4 des8 ees4 des8
    ees8 r2.. r1 ges8 r r ges r2  r1 aes8 r2..  r1 ees8 r r ees8 r2 r1
  }
}

VerseThree =
{
  <<
  {\ChordsTwo \ChordsTwo \ChordsTwo \ChordsTwo}
  {
    \relative c'{r1 r r r  r1 r r ees8 r bes4 des8 ees4 des 8
      ees8 r2..  r1 r r}
    \relative c''{<< {ges2 aes bes1 bes~bes} {ges2 aes des1 ees~ees}>>}
  }
  >>
}

Bridge =
{
  <<
    {\ChordsTwo \ChordsTwo }
    \relative c'
    {ees4 f ges8 f r4 ees4 f ges8 f r4 ees4 f ges8 f r4
     ees4 f ges8 f r4 ees4 f ges8 f r4  ees4 f ges8 ges ees4~ees1 r }
  >>
}

ChorusThree =
{
  \relative c' 
  {
    ees8 r2.. r1 r1 r1 r1 r1 ees8 r2.. r1
  }
}

Outro =
{ 
  \relative c'
  {
    \repeat volta2
    {
      ees8 r2.. r4 bes4 des8 ees4 des8 ees8  r r ees r2 
      r4 bes4 des8 ees4 des8    
      ges8 r r ges r2  ges8 r r ges r2 ees8 r r ees8 r2
      ees8 r bes4 des8 ees4^"x2" des8 
      
    }
    ees8 r <<{bes4} {bes'4}>> 
  }
}


Bbsaxpart =
{
  \tempo 4 = 185
  \key ees \minor
  
  \relative c^"Intro: bass + dms"
  { \ChordsOne} \bar "||"
  \relative c^"Verse 1: bass + dms"
  { \ChordsOne}\bar "||"
  \relative c^"Chorus 1: bass, dms, brass"
  { \ChorusOne}\bar "||"
  \relative c^"verse 2: Dms first, then band"
  { \VerseTwo}\bar "||"
  \relative c^"Chorus 2: Nothing, then band"
  { \ChorusTwo}\bar "||"
  \relative c^"Verse 3: band"
  { \VerseThree}\bar "||"
  \relative c^"Bridge: band"
  { \Bridge}\bar "||"
  \relative c^"Chorus 3: bass, bit of brass"
  { \ChorusThree}
  \relative c^"Outro: band"
  { \Outro}
  
  
}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a \Bbsaxpart  
}

\pageBreak
\markup {"Feel It Still"}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart
    %\midi{}
}

