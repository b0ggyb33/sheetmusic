
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Black Eyed Boy"
  subtitle = "V1"
}

fanfare = 
{<< 
    \new Voice = "first"
    { \voiceOne c ees f8 g4. c1 c8 bes4. g4 f ees c r r}
    \new Voice= "second"
    { \voiceTwo c, ees f8 g4. c1 c8 bes4. g4 f ees c r r}
  >>
}

verse =
{r1^"Verse" r1 r1 r1 r1 r1 r1 r1
}
  
prechorus =
{<< \chords 
     {
       bes aes bes aes
     }
     \relative c'' 
     \new Voice= "1"
     {
       \voiceOne {f4 r r f ees r r c8 ees f4 r \tuplet 3/2 {r f f} ees r r bes'8 aes} 
     }
     \relative c'' 
     \new Voice= "2"
     {
       \voiceTwo {bes4^"Prechorus" r r bes aes r r f8 aes bes4 r \tuplet 3/2 {r bes bes} aes r r g'8 f} 
     }
  >>
}

chorus = 
{<< \chords 
  {
    c1:m bes1 aes g c:m bes aes g
  }
  \relative c'''
  \new Voice = "first"
  { \voiceOne g1 f ees d ees f ees d}
  \relative c''
  \new Voice= "second"
  { \voiceTwo ees1 d c b c d c b}
 >>
}


Bbsaxpart =
{
  \tempo 4 = 160
  \key c \minor  

  \relative c''
  \fanfare  
  \repeat volta2 
  {
    \verse
    \bar "||"
    \prechorus
    << {\chorus} {r^"Chorus: repeat 2nd time & big ral. at end"}>>
  }
  {
    r^"bass" r
  }
  \repeat volta2 {\relative c'' \fanfare}
  \prechorus
  \repeat volta2 
  {
    << {\chorus} {r^"Chorus: repeat untill end"}>>
  }
  {
    c''1
  }

}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\markup {"   "}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
    %\midi {}
}

