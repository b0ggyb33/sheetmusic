
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Five Colours in her Hair"
  subtitle = "V1"
}

Intro = 
{<< 
    \relative c'''
    \new Voice = "first"
    { \voiceOne  r1^"intro" r r r r r e,2 gis b d}
    \relative c''
    \new Voice= "second"
    { \voiceTwo r1 r r r r r e2 e gis b \bar "||" }
  >>
}

Dodoos = 
{<< 
    \relative c''
    \new Voice = "first"
    { \voiceOne  e4^"do doos" e e8 e4. fis2 a4 r 
      e4 e e8 e4. fis2 a4 r 
      e4 e e8 e4. fis2 a4 r 
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo e4 e e8 e4. d2 d4 r
      e4 e e8 e4. d2 d4 r
      e4 e e8 e4. d2 d4 r 
    }
  >>
}

DodooRun = 
{<< 
    \relative c''
    \new Voice = "first"
    { \voiceOne  e2 gis b d  \bar "||" }
    \relative c''
    \new Voice= "second"
    { \voiceTwo e2 e gis b 
    }
  >>
}


DodooEnd =
{<< 
    \relative c''
    \new Voice = "first"
    { \voiceOne fis1\fermata  }
    \relative c''
    \new Voice= "second"
    { \voiceTwo cis1  }
  >>
}



Verse = 
{<< 
    \relative c''
    \new Voice = "first"
    { \voiceOne  r1^"Verse" r d4 d d d e1
      r r d4 d d d e1
      a2 g e cis d4 d d d e1
      \bar "||" d^"V1 only" e4 r4 r2 \bar "||" d1^"V2 only"~d e~e \bar "||"
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo r1 r g4 g g g a1
      r r g4 g g g gis1
      a'2 e cis a g4 g g g a1
      \bar "||" d, e4 r4 r2  d1~d e~e 
    }
  >>
}


  

Chorus = 
{<< 
  \relative c''
  \new Voice = "first"
  { \voiceOne \bar ".|:"r1^"Chorus" r e4. d8 e4. d8 e4. d8 e2\bar ":|."
    fis4 gis8 a~a2   gis4 a8 b8~b2  e,4 fis8 gis~gis2  a4 b8 cis~cis2
    r1 r r r \bar "||"
  }
  \relative c''
  \new Voice= "second"
  { \voiceTwo r1 r e4. d8 e4. d8 e4. d8 e2
    d4 e8 fis~fis2   e4 fis8 gis8~gis2  cis,4 d8 e~e2  fis4 gis8 a~a2
    r1 r r r
  }
 >>
}

Interlude =
{
  \relative c'
  {
    fis2^"Interlude very free" ais r1 b2 d r1 g,2 b r1 d4^"then she ..." e f g a r4 r2
    r1^"..."   a4^"off her hair" gis fis r
    r1^"..."  d4^"weirdo with no name" cis b a a r r2 \break
    
    
  } 
}

Bbsaxpart =
{
  \tempo 4 = 190
  \key a \major  

  \Intro
  \Dodoos 
  \DodooRun
  \bar "[|:"\Verse
  \Chorus\bar ":|]"
  \Interlude  
 
  \Intro
  \Chorus
  {r1^"1-2-3-4"}  
  \Dodoos \DodooEnd
}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart 
    %\midi{}
}

\pageBreak
\markup { "Five Colours in her Hair"  }

\score 
{
    
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   

}

