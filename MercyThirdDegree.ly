
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Mercy (Third Degree)"
  subtitle = "V1"
}

Intro = 
{<< 
    \relative c'''
    \new Voice = "first"
    { \voiceOne a8^"Intro" r4 a8 r4 r4 a8 r4 a8 r4 r4 a8 r4 a8 r4 r8 gis (a8) r4 a8 r4 r4}
    \relative c''
    \new Voice= "second"
    { \voiceTwo e8 r4 e8 r4 r4 e8 r4 e8 r4 r4 e8 r4 e8 r4 r8 gis, (a8) r4 e'8 r4 r4 }
    \chords {a1*4:m}
  >>
}

Verse =
{<< 
    \relative c'''
    \new Voice = "first"
    { \voiceOne \bar ".|:" a8^"Verse" r4 a8 r4 r4 a8 r4 a8 r4 r4 a8 r4 a8 r4 r8 gis (a8) r4 a8 r4 r4 \bar ":|."}
    \relative c''
    \new Voice= "second"
    { \voiceTwo e8 r4 e8 r4 r4 e8 r4 e8 r4 r4 e8 r4 e8 r4 r8 gis, (a8) r4 e'8 r4 r4 }
    \chords {a1*4:m}
  >>
  << 
    \relative c'''
    \new Voice = "first"
    { \voiceOne c1 b2 bes a8 r4 a8 r4 r8 gis ( a8) r4 a8 r4 r4}
    \relative c'''
    \new Voice= "second"
    { \voiceTwo g1 fis2 f e8 r4 e8 r4 r8 gis, a r4 e'8 r4 r4 \bar "||"}
    \chords {c1 b2 bes a1*2:m}
  >>
  << 
    \relative c'''
    \new Voice = "first"
    { \voiceOne c4 c c8 c4.  b4 b bes8 bes 4. a8 r4 a8 r4 r8 gis ( a8) r4 r8 r4 r4}
    \relative c'''
    \new Voice= "second"
    { \voiceTwo g4 g g8 g4. fis4 fis4 f8 f4. e8 r4 e8 r4 r8 gis, a r4 r8 r4 r4 }
    \chords {c1 b2 bes a1*2:m}
  >>
  
}

ChorusFrag =
{
  << 
    \relative c'''
    \new Voice = "first"
    { \voiceOne r4 a b8 b4. r4 c b8 b4. }
    \relative c''
    \new Voice= "second"
    { \voiceTwo r4 e fis8 fis4. r4 g fis8 fis4.}
  >>
}


Chorus =
{
  << 
    {\bar ".|:" \ChorusFrag  \bar ":|." }      
    \chords {a1*2:m }

  >>
  << 
    \ChorusFrag       
    \chords {d1*2}
  >>
  << 
    \ChorusFrag       
    \chords {a1*2:m}
  >>
  << 
    \relative c'''
    \new Voice = "first"
    { \voiceOne b1 a r4 a b8 b4. e4 r r2^"to V2" \bar "||"}
    \relative c'''
    \new Voice= "second"
    { \voiceTwo e,1 fis r4 e fis8 fis4. a4 r r2}
    \chords {e1:m d a1*2:m}
  >>
}

Bridge =
{
  \relative c'
  { r1^"Bridge ...  Drums" r r r \bar "||" r^"Drums + Vocal" r r r\bar "||" r^"Drums + Vocal" r r r}
   << 
    \relative c'''
    \new Voice = "first"
    { \voiceOne  \bar ".|:"c4. c8 r2^"   x4" \bar ":..:"e4. e8 r2^"   x4" \bar ":|."}
    \relative c''
    \new Voice= "second"
    { \voiceTwo a'4. a8 r2 a4. a8 r2 }
  >>
}

OutroA =
{
  << 
    \relative c'''
    \new Voice = "first"
    { \voiceOne a1 g2. e8 c d1 }
    \relative c'''
    \new Voice= "second"
    { \voiceTwo a1 g2. e8 c d1}
    \chords {a1:m c d }
  >>
}
OutroB =
{
  << 
    \relative c''
    \new Voice = "first"
    { \voiceOne f2. g8 gis }
    \relative c''
    \new Voice= "second"
    { \voiceTwo f2. g8 gis}
    \chords {f }
  >>
}

Outro =
{
  <<\repeat volta2{ \OutroA \OutroB }  >> 
  

  {{\relative c^"With vocal"}
     \repeat volta 4
    <<{ \OutroA | } 
    >>
    \alternative 
    {
      { \OutroB }
      
      {<< 
        \break
        \relative c'''
        \new Voice = "first"
        { \voiceOne b2.. b8 b2.. b8 e4 r r2 \bar "||" }
        \relative c''
        \new Voice= "second"
        { \voiceTwo e2.. e8 e2.. e8 a4 r r2 }
       >>   
      }
    }
  } 
}



Bbsaxpart =
{
  \tempo 4 = 130
  \key a \minor  

  \relative c''
  \Intro 
  \Verse
  \Chorus
  \Bridge
  \Outro
  

}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Mercy"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
    %\midi{}
}

