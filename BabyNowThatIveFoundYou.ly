
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Now That I've Found You"
  %subtitle = "V1"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

#(set-global-staff-size 27)

Intro =
{
  \relative c''
  { 
    r1^"Intro" r1 a8 b d4-. d-- b-. d-- e-. d-. r  
  }
}

Chorus =
{
  \repeat volta2
  {
    <<
      \relative c'
      {
        r2^"Chorus (play twice)" d8 e fis4 a1 r2 d,8 e fis4 a1 \break
        r2 d,8 e fis4 b1^"After 3rd chorus, repeat chorus again - then outro" r4 b'-. r2 r1
      }
      \chords {d1 c g:/b g:m  d:/a e:7/gis e:m a}
    >>
  }
}

Break =
{
  \relative c''
  { 
    r1^"2 Bar Break" r4 e-- d-. r  
  }
}



Verse = 
{
  <<
    \relative c'
    { 
      r1^"Verse 1 - no verse 2! Skip straight to bridge" r1 r1 r1 \break
      
      r1 r1 r1 fis2 fis4 a
    }
    \chords {d2 g d e:m d g d e:m d g d e:m d g a1}
  >>
}

Bridge = 
{
  <<
    \relative c''
    { 
      b2.^"Bridge" b4-. a4-. r r2  r4 b-. r a-. b4-. r r2 \break
      r4 a-. r a-. g4-- b-. g-- b-. a1 r2^"Back to chorus" a8 b d b
    }
    \chords {b1 fis:m b2 fis:m b1  a1 e:m7 a1*2 }
  >>
}

Outro =
{
  \relative c''
  { 
    a8^"Outro" b d4-. d-- b-.  d8-- cis d4-. e-- d-.    
    a8 b d4-. d-- b-.  d-- e-. d-. r 
  }

}



Bbsaxpart =
{
  \tempo 4 = 128
  \key d \major
  \time 4/4
  
  \Intro  \break
  \repeat volta3
  {
    \Chorus \break
    \Break  \break
    \Verse  \break
    \Bridge \break
  }
  \Outro

}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Baby, now that I've found you"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

