\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Signed Sealed Delivered"
  subtitle = "Alto Sax"
}

\transpose c g
{
\relative c''' {
  \time 4/4
  \clef treble
  \key c \major
  g4^\markup{intro} f e d c bes g r

  g'4 f e d c bes g2 g'2 r4 r8 fis,8~

  \mark \markup { \musicglyph #"scripts.segno" }
   
  \repeat volta 2 { g8^\markup{verse} r2 r4 r8 r2 r4 r8 fis g r2 r4 r8 }
	\alternative{
		    {r1 r1 r1 g1 g1 \grace fis8}
		    {g4 b d e g2 e d1 g2. g4 \tuplet 3/2 { r4 g g } g g }
		    }
  \repeat volta 4 { r8^\markup{chorus} d f4 d8 f4 r8 g1 }
  
  g4 f e d c bes g2 

  \mark \markup { \musicglyph #"scripts.coda" }
  \break

  \mark \markup { \musicglyph #"scripts.coda" }
  d8 f g b~ b4 d c8 e g bes~ bes4 g   e8 d bes g~ g4 f g~ g8 bes8 g4 g8 f8 
  g8 g b4 d8 d f4 c8 c e g~ g4 bes g8 e~ d bes~ bes4 g8 e g4~ g8 bes g2^\markup{repeat chorus}
}
}