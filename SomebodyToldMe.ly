
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Somebody Told Me"
  subtitle = "V1"
}
#(set-global-staff-size 24)

Intro = 
{ \relative c^"Intro"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4} 
}

Verse =
{<< 
     \new Voice = "first"
     { \voiceOne \relative c'''
       { 
         r1^"Verse"\segno r r r c dis2 d 
         \bar "||"r1^"Skip to B on V2"  r r r r r c dis2 d\bar "||"
         r1^"B" r r r
         r1 r r2 r4 c ees4. c8~c bes aes f
         ees1 aes2 bes bes1 bes4-. r r2
       }
     }
     \new Voice = "second"
     {\voiceTwo \relative c''
       { 
         r1 r r^"teen"^"seven"_"taking" r^"had"_"leaving" f gis2 g
         r1 r r r r_"any"_"thing" r_"don't" f gis2 g
         r1 r r r
         r1_"bring" r_"bring" r2_"night." r4 c, ees4. c8~c bes aes f
         ees1 aes2 bes bes1 bes4-. r r2
       }
     } 
>>}

ChorusA =
{<< 
     \new Voice = "first"
     { \voiceOne \relative c''
       { \repeat volta2
         {ees1 c}
         \alternative
         {{d1 b4 c d2}{d1 r1^"coda"^"DS al"}}
       }
     }
     \new Voice = "second"
     {\voiceTwo \relative c''
       { \repeat volta2
         {c1 aes} 
         \alternative
         {{bes1g4 aes bes b}{bes1 r}}
       }
     } 
>>}

ChorusB =
{<< 
     \new Voice = "first"
     { \voiceOne \relative c''
       { \repeat volta2
         {ees1^"6x" c}
         \alternative
         {{d1^"1 3 5" b4 c d2}{ d1^"2 4 6" d1}}
       }
     }
     \new Voice = "second"
     {\voiceTwo \relative c''
       { \repeat volta2
         {c1 aes} 
         \alternative
         {{bes1g4 aes bes b}{bes1 b}}
       }
     } 
>>}

End =
{<< 
     \new Voice = "first"
     { \voiceOne \relative c'' ees\fermata }
     \new Voice = "second"
     {\voiceTwo \relative c'' c    } 
>>}

Bridge =
{ \relative c^"Bridge"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*3}  
}

Bbsaxpart =
{
  \tempo 4 = 140
  \key c \minor
  
  \Intro \bar "||"
  %\repeat volta2 
  {\Verse \ChorusA}
  \bar "||"\Bridge
  
  \repeat volta 3 {\ChorusB}
  \End
  
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Somebody Told Me"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

