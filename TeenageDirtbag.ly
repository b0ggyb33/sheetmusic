
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Teenage Dirtbag - all a bit flakey"
  subtitle = "V1"
}
#(set-global-staff-size 23)


Intro = 
{
  {
    <<{\chords {fis2 cis fis b fis2 cis fis b}}
      {r1^"Intro/Link" r r r }
    >>
  }
}

Verse = 
{
  {
    <<{\chords 
       {
         fis2 cis fis b fis2 cis fis b
         fis2 cis fis b fis2 cis fis b
         fis2 cis fis b dis2 b cis1
         dis2 b cis1
       }
      }
      {
        r1^"Verse" r r r
        r r r r
        r r r r
        r r
      }
    >>
  }
}

Chorus =
{
  \repeat volta 2
  {
    <<{\chords 
       {
         fis2 cis fis dis4 ais4 fis2 cis fis dis4 ais4
         fis2 cis fis dis4 ais4 fis2 b2 b2  ees4 bes:m b des
       }
      }
      \new Voice = "first"
      {\voiceOne \relative c'' {
        fis4^"Chorus" ais8 ais gis4 fis ais f f fis   fis4 ais8 ais gis4 fis ais f f fis 
        fis4 ais8 ais gis4 fis ais f f fis  \time 2/4 fis2 
        \time 4/4fis8 gis ais4 gis8. fis16 f4 cis8^"????" r ais r gis r fis
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        
      }}
    >>
  }
}

Bridge =
{
  \repeat volta2
  {
    <<{\chords {fis2 b:sus2 fis2 b:sus2 fis2 b:sus2 ees4 bes:m b des}}
      \new Voice = "first"
      {\voiceOne \relative c'' {
         cis4^"Bridge after C2 & C3" dis8 fis~fis2  cis4 dis8 fis~fis4 dis8 cis 
         cis4 dis8 fis~fis2  cis8^"????" r ais r gis r fis
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        
      }}
    >>
  }
}


Outro =
{
    <<{\chords {}}
      \new Voice = "first"
      {\voiceOne \relative c''' {
         gis2 fis  cis4 b8 ais~ais4 gis8fis fis (ais) r4 gis8 gis r4 fis8 fis r4
         b4^"Big rall!" ais gis8 ais gis4 fis2
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        
      }}
    >>
}

Bbsaxpart =
{
  \tempo 4 = 96
  \key fis \major
 
  \Intro \bar "||" \break
  \Verse  \break
  \Chorus \break
  \Bridge
  \Outro
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
    %\midi{}
}

\pageBreak
\markup {"Teenage Dirtbag"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart

}

