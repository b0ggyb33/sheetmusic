
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Disco 2000"
  subtitle = "V1.1"
}
#(set-global-staff-size 26)

intro =
{
  <<
  \relative c { \repeat percent 4 {r1} \bar "||" \repeat percent 8 {r} }
  \chords {\repeat percent 4 {g:1} 
           \repeat percent 4 {g:1} 
           \repeat percent 4 {c:1} }
  >>
}

verse = 
{
  <<
  {r1^"Verse" r r r\bar "||" r r r r\bar "||" r r r r\bar "||" r r r r}
  \chords {\repeat percent 4 {g1} 
           \repeat percent 4 {c}
           \repeat percent 4 {g} 
           \repeat percent 4 {c} }
  >>
}

prechorus =
{
  \repeat volta 2
  {
    << 
    \relative c''
    \new Voice = "first"
    {\voiceOne f2^"PreChorus x2" e2 f4 e f e f2 e2 f4 e f e}
    \relative c''
    \new Voice= "second" 
    {\voiceTwo f2 e2 f4 e f e f2 e2 f4 e f e}
    \chords {\repeat percent 4 {d1:m} }
    >>
  }
  <<
    \relative c''
    \new Voice = "first"
    {\voiceOne d1~d}
    \relative c''
    \new Voice= "second" 
    {\voiceTwo d2. b8 a g2 g4 b}
    \chords {\repeat percent 2 {g1} }
  >>
}

chorus = 
{
  \repeat volta 2
  {
    << 
      \relative c''
      \new Voice = "first"
      {\voiceOne e1^"Chorus" f4 e d c e1 e4 d c b \bar "||"
                 e1 c4 b a g d'1 e2 d2}
      \relative c''
      \new Voice= "second" 
      {\voiceTwo c1 f4 e d c b1 e4 d c b 
                 a1 c4 b a g a1 b2 b2}
      \chords {c1 c e:m e:m a:m a:m d:m e2:m g2}
    >>
  }
}

bridge =
{
  {r1^"After 2nd Chorus ...  Bridge" r r r r1 r r r}
}

ending =
{
  << 
    \relative c'
    \new Voice = "first"
    {\voiceOne e'2^"x4 at end" d2 c1}
    \relative c''
    \new Voice = "second" 
    {\voiceTwo b2 b2 c1}
  >>

}

Bbsaxpart =
{
  \tempo 4 = 120
  \key c \major
  
  \intro

  \relative c''
  \repeat volta 2 
  {
    \verse
    \prechorus
    \chorus 
  }
  \break \bridge 
  \prechorus
  \repeat volta 4 {\chorus}
  \ending
   
}

\score 
{
    \new Staff \with { instrumentName = #"ALTO"}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Disco 2000"}

\score 
{
    \new Staff \with { instrumentName = #"TENOR"}
    \Bbsaxpart
    %\midi{}
}

