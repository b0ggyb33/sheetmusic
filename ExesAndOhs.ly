
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Exes and Ohs"
  subtitle = "V1"
}

Intro = 
{<< 
    \new Voice = "first"
    { \voiceOne r4 r r fis8 e  \bar "||"fis2 r r r4 fis8 e fis2 r r r \bar "||"}
    \new Voice= "second"
    { \voiceTwo }
  >>
}

Verse =
{
  \relative c''
  {r1^"Verse" r1 r1 r2 r4 fis
  gis1~gis a2. gis4 fis1 \bar "||"}
}
  
Prechorus =
{
  r1^"Prechorus" r r r r r r r \bar "||"
}

Chorus = 
{<< 
  \relative c''
  \new Voice = "first"
  { 
    \voiceOne a1^"Chorus: repeat except after V1" cis4 b a gis a1 cis4 b a gis
    a1 cis4 b a gis cis2 cis cis cis8 b a fis \bar "||"
  }
  \relative c''
  \new Voice= "second"
  { \voiceTwo }
 >>
}

Bridge =
{
  \relative c'
  {fis2^"Bridge/outro:2nd=gtr solo" cis'4 e fis2 cis4 e fis2 cis4 e fis2^"3rd Stop here!" cis4 e }
}


Bbsaxpart =
{
  \tempo 4 = 140
  \key fis \minor  

  \relative c''
  \Intro 
  \Verse
  \Prechorus
  \Chorus
  \Bridge
  

}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\markup {"   "}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
    %\midi{}
}

