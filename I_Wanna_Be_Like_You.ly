\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "I Wanna be Like You"
}
\score{
\transpose c f {
\key c \major
\relative c' {
  r2. e4 c' c b8 c b4 a a r e e e e e 
  gis2. gis4 gis4. gis8 gis4 gis  gis4. gis8 gis4 gis
  gis4 gis4 gis8 fis gis4 a2 r4 e c'8 c8 c8 c8 b2 a4 a r e 
  e e e e  gis2. gis4 gis4 gis4 gis4. gis8 gis4 gis4 gis4 gis4
  gis4 gis4 gis8 fis8 gis4 a r g2
  e'4^\markup{chorus} c c r c8 c8 c8 b4 c4 cis4 a a r2 r4
  e'8 e dis e4 a, a r 
  e' b b r a g a r2 e'4 e e e e c c r2 c4 b c cis a a r2 e'4 e a, a r e' b b8 e4. e4 c4 c4
}}}