
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "I Want You Back"
  subtitle = "V2"
}

IntroLine =
{
  \relative c''
  {
    \set Score.skipBars = ##t  {R1*4}
    \repeat volta 2 {fis16 r r dis fis fis r fis r fis r dis fis fis dis cis | }
    \alternative{ 
      {fis16 r r dis fis fis r fis r fis r dis fis fis dis cis | }
      {r1} 
      }
  }
}


VerseOne =
{<<
  \chords {fis1 b ees4:m bes:m b fis  aes:m cis:7 fis2}
  \relative c''
  {
    {fis8^"Verse 1" r r4 r2 fis8 r r4 r2 fis4 cis dis ais gis4 cis fis,4. dis'8}
  } 
>>}

StraightChorus =
{<<
  \chords {fis1 b ees4:m bes:m b fis  aes:m cis:7 fis2}
  \relative c''
  {
    {r1^"Chorus" | r2 ais'16. ais32 gis16 gis16 fis8 dis8 | ais'4 r ais r | ais4 r8 ais8~ ais16 fis16 gis8-. fis8-. r8}
  } 
>>}

BridgeOne =
{ 
    \relative c''
    { r2^"Bridge?" r16 fis fis fis fis8 r r2 r16 fis fis fis fis8 r  
      r2 r16 fis fis fis fis8 r  r1}
}

VerseTwo =
{<<
  \chords {fis1 b ees4:m bes:m b fis  aes:m cis:7 fis2}
  \relative c''
  {
    {fis4^"Verse two?? No need something better" f dis cis b4 ais gis cis fis4 cis dis ais gis4 cis fis,4 r}
  } 
>>}

ChorusTwo =
{<<
  \chords {fis1 b ees4:m bes:m b fis  aes:m cis:7 fis2}
  \relative c''
  {
    {
      fis4^"Chorus Two" f dis cis dis8 (fis fis4) dis8 (fis fis4) 
      fis4 cis dis8 (fis fis4) fis8 fis fis (dis) fis (gis~gis) fis
      fis4 f dis8 (fis fis4)  dis8 (fis fis4)  fis dis 
      fis4 cis dis8 (fis fis4)  r1
    }
  } 
>>}

BoomBooms =
{ 
    \relative c''
    { r1^"Boom Boom Bridge?" r r r r r r}
}


OutroChorus =
{<<
  {\chords {fis1 b ees4:m bes:m b fis  aes:m cis:7 fis2}}
  \relative c''
  \new Voice = "first"
  { \voiceOne fis4^"Outro Chorus repeat 4x" f dis cis
       b8 b ais ais gis16. gis32 gis16 gis cis8 cis 
       ais'4 f fis cis  b f' cis  }
  \relative c''
  \new Voice = "second"
  { \voiceTwo fis4                f dis cis 
       b8 b ais ais gis16. gis32 gis16 gis cis8 cis 
       fis4 cis dis ais gis4 cis fis,4 r   }

>>}

KTOutro=
{ 
    \relative c'
    { fis8^"KT Out" fis a fis~fis4 r cis'8 cis e cis~cis4 r 
      fis8 fis a fis~fis4 r r1 }
}


Bbsaxpart =
{
  \tempo 4 = 99
  \key fis \major  

  \IntroLine \break
  \repeat volta2 {\VerseOne} \break
  \repeat volta2 {\StraightChorus} \break
  \BridgeOne \break
  \repeat volta2 {\VerseTwo} \break
  \ChorusTwo \break
  \BoomBooms \break
  \repeat volta2 {\OutroChorus r1 \time 2/4 r2^"Jacksons"} \break
  \time 4/4 \KTOutro

}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart 

}
\pageBreak
\markup {"I Want You Back   "}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
    %\midi{}
}

