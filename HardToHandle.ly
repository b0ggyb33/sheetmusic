
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Hard to Handle"
  opus = "13 Novemeber 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
#(set-global-staff-size 23)

Intro = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
        r4^"Intro" r r8 c16^"piano" c e f8 g16 (g2) r8 f16 ees f es8  c16 (c2)
        r8 c16^"play" c e f8 g16 (g2) r8 f16 (ees) f (ees) r8       
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'' {}
    }}
    {\chords {  } }
  >>
}

ChorusStart = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      { g4 r8 g16 g b c8 d16 (d8) r   g,4 r8 g16 g b c8 d16 (d8) r 
        \set Score.skipBars = ##t   {R1*2}      
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'' {}
    }}
    {\chords {  } }
  >>
}

ChorusEnd = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      { r4 r8 c16 c e f8 g16~g4  
        r4 r8 f16 (ees f ees c bes c4)
        r4 r8 c16 c e f8 c'16~c4 
        bes8. f16 (f) c8. r2
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'' 
      {
        r4 r8 c16 c e f8 g16~g4  
        r4 r8 f16 (ees f ees c bes c4)
        r4 r8 c16 c e f8 c16~c4 
        bes8. f16 (f) c8. r2
      }
    }}
    {\chords {  } }
  >>
}


VerseChorusOne = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
        c1^"VERSE/CHORUS 1" (c1) \set Score.skipBars = ##t   {R1*6}
        \ChorusStart 
        r8 f16 f (f) f f8 r g16 g (g) g g8
        bes8 bes16 f (f) f c8 (c4) r
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        
      }
    }}
    { \chords {}    }
  >>
}


VerseChorusTwoThree = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
        r2^"VERSE CHORUS 2&3" r4  f16 f r16 ees  
        \repeat volta7 {  e2 r4 f16 f r16 ees^"x7" }
        e2 r2 
        \ChorusStart 
        \ChorusEnd
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        
      }
    }}
    { \chords {}    }
  >>
}


GuitarSoloandChorus = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'''^"GUITAR SOLO - OUTRO" 
      {
        
        \set Score.skipBars = ##t   {R1*8}
        \ChorusStart 
        \ChorusEnd
 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {      }
    }}
    {\chords {} }
  >>
}

Outro = 
{ \relative c^"OUTRO: adlib vocal & gtr"
  \set Score.skipBars = ##t   {R1*8}
  << 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'''
      {      
        bes8. f16~f c8. r2
        bes'8. f16~f c8. r2
        bes'8. f16~f c'8. r2
        bes8. f16~f c8. r2
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        bes8. f16~f c8. r2
        bes'8. f16~f c8. r2
        bes'8. f16~f c'8. r2
        bes8. f16~f c8. r2
      }
    }}
    {
      \chords {}
    }
  >>
}


Bbsaxpart =
{
  \tempo 4 = 114
  \key c \major


  \Intro \bar "||"\break
  \VerseChorusOne \break
  \repeat volta2 \VerseChorusTwoThree \break
  \repeat volta2 {\GuitarSoloandChorus} \break
  %\Outro
}

BbsaxpartTwo =
{ \transpose c cis \Bbsaxpart}

\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"} 
    \transpose cis aes, \BbsaxpartTwo   
}

\pageBreak
\markup {"Hard to Handle"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \BbsaxpartTwo 
    %\midi {}

}

