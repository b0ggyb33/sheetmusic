\version "2.18.2"
\header{
	title = Valerie
}

ChorusOne =
{<<
  \relative c'''
  {c,1^\markup{\small Chorus}~ c b~ b c~ c b~ b
	c~ c b~ b d~ d~ d~ d}
  {a~ a g~ g a~ a g~ g
	a~ a g~ g g,~ g~ g~ g}
  
>>}

ChorusTwo =
{<<
  \relative c''
  {c1^\markup{\small Chorus 2}~ c b~ b a~ a g~ g
	a~ a b~ b d~ d~ d~ d}
  {a~ a g~ g f~ f e~ e
	f~ f e~ e d~ d~ d~ d}
  
>>}

DoDoos =
{<<
  \relative c''
  \new Voice = "First"
  {\voiceOne r4 e4 r e e f8 g8 r4 r
   r4 e4 r e e f8 g8 r4 r
   r4 d4 r d4 d e8 f8 r4 r
   r4 d4 r d4 d e8 f8 r4 r}
  \relative c''
  \new Voice = "second"
  {\voiceTwo r4 e,4 r e e f8 g8 r4 r
   r4 e4 r e e f8 g8 r4 r
   r4 d4 r d4 d e8 f8 r4 r
   r4 d4 r d4 d e8 f8 r4 r}
>>} 

Ebsaxpart =
{
\relative c'' {

	\time 4/4
	\tempo 4 = 110
	\clef treble
	\key c \major
	R1*4^\markup{\small Bass & Drums} \bar "||"
	r1^\markup{\small Add Guitar} r r r \break
	\repeat volta 2 {r1^\markup{\small Verse 1} r r r r r r r} \break
	%a'^\markup{\small Chorus}~ a g~ g a~ a g~ g
	%a~ a g~ g d'~ d~ d~ d
	\ChorusOne
	
	\relative c''
	\repeat volta 2 {\DoDoos} \break

	\repeat volta 2 {e'1^\markup{\small Verse 2} r r g2 e d1 r1 d2 f g a}
	\repeat volta 2 {c1 r r e,2 g f1 r1 d2 f c' d} \break

	%c1^\markup{\small Chorus 2}~ c b~ b a~ a g~ g
	%f~ f e~ e d'~ d~ d~ d
	\ChorusTwo
	\repeat volta 2 {\DoDoos} \break

	\repeat volta 2 { r1^\markup{\small verse 3 = verse 1} r r r r4 r4 a'2 g a c a g a}

	}
}


\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}
    \Ebsaxpart 
    %\midi{}

}

\pageBreak
\markup{"Valerie"}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}  
    \transpose c f \Ebsaxpart  
}

