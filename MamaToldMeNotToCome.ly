
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Mama Told Me Not To Come"
  subtitle = "V1"
}
#(set-global-staff-size 22)

Intro =
{
  r1^"Intro: keys" r r \bar "||" r^"+ drums" r
}

Verse =
{
  r1^"Verse"^\segno r r r \bar "||" r r r r \bar "||"
}

Chorus = 
{<< 
    \new Voice = "first"
    \relative c''
    { \voiceOne 
      r4_"told me .." r r e8 fis a4 a8 b~ b4 a8 r  r4 r r e8 fis a4 a8 b~ b4. r8  
      r4 r r e,8 fis a4 a8 c~c  b a r r1 r1^\coda^"x2" 
    }
    \new Voice= "second"
    \relative c'
    { \voiceTwo 
      r4^"Chorus" r r e8 fis a4 a8 b~ b4 a8 r r4  r r e8 fis a4 a8 b~ b4. r8  
      r4 r r e,8 fis a4 a8 c~ c b a r  r1 r1 
    }
  >>
}

BehindSolo =
{<< 
    \new Voice = "first"
    \relative c'''
    { \voiceOne 
      cis4 b8 a8~a2 r1 cis4 b8 a8~a2 r1 
      cis4 b8 a8~a2 r1 a1 r1 ^"al Coda"^"D.S."
    }
    \new Voice= "second"
    \relative c''
    { \voiceTwo 
      cis4^"Behind gtr Solo" b8 a8~a2 r1 cis4 b8 a8~a2 r1 
      cis4 b8 a8~a2 r1 a1\> r1 \!
    }
  >>
}
 
BehindOutroA =
{<< 
    \new Voice = "first"
    \relative c''
    { \voiceOne 
      e4 d8^\coda cis8~cis2 c8 cis c b~b4 a8 r e'4 d8 cis8~cis2 a8 b c cis~ cis4 b8 r  
      e4 d8 cis8~cis2 c8 c cis cis c c b r e4 d8 cis8~cis2 a8 a b b c c cis cis 
    }
    \new Voice= "second"
    \relative c''
    { \voiceTwo 
      e4 d8 cis8~cis2 c8 cis c b~b4 a8 r e'4 d8 cis8~cis2 a8 b c cis~ cis4 b8 r  
      e4 d8 cis8~cis2 c8 c cis cis c^"on 3rd"^"STOP!" c b r e4 d8 cis8~cis2 a8 a b b c c cis cis^"x3" 
    }
  >>
}
  

  
BehindOutroB =
{<< 
    \new Voice = "first"
    \relative c'''
    { \voiceOne 
      e4 d8 cis8~cis2 r1 e4 d8 cis8~cis2 r1 
      e4 d8 cis8~cis2 r1 e4 d8 cis8~cis2 r1 
    }
    \new Voice= "second"
    \relative c'''
    { \voiceTwo 
      cis4 b8 a8~a2 r1 cis4 b8 a8~a2 r1 
      cis4 b8 a8~a2 r1 cis4 b8 a8~a2 r1 \bar "||"
    }
  >>
}
  

Nicebits = 
{
  \relative c''
 
  { 
    r1^" ......" r1 a4^"useful scale" b c cis d e g a
  }
}


Bbsaxpart =
{
  \tempo 4 = 125
  \key a \major
  \Intro
  \repeat volta 2
  {
   \Verse
   \Chorus
  }
  \BehindSolo
  \repeat volta 2 {\BehindOutroA}
  %\BehindOutroB
  \Nicebits
  

}

\score 
  {
    \new Staff \with { instrumentName = \markup \bold {"ALTO"} } 
    \transpose d a, \Bbsaxpart  
}

\markup {"Mam Told Me Not to Come "}

\score 
  {
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart   
    %\midi{}
}

