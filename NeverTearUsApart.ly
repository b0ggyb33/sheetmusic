
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Never Tear Us Apart"
  subtitle = "V1"
}
#(set-global-staff-size 22)

Intro = 
{
  \set Score.skipBars = ##t   {R1*4}
}


Verse =
{
  <<  {\chords {b1:m g e:m g}}
      \new Voice = "first"
      {\voiceOne \relative c'' 
         {d1~d  b1~b}        
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' 
         { b2.^"upper harmony on V2 rpt only"~b8 a8  g1  e2.~e8 fis8 g1^"rpt V2" \bar ":|."}
      }
  >>
}

Chorus =
{
  <<  {\chords {d2 g }}
      \new Voice = "first"
      {\voiceOne \relative c'' 
         { 
           d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2   \bar "||"
           cis1~\<cis4\! r r \tuplet 3/2 {b8^"(gtr)" d e} b2.^"play?" \tuplet 3/2 {b8 d e} b2.^"play?" r4
         }        
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' 
         { 
           d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2   \bar "||"
           cis1~cis4 r r r b2. r4 b2. r4
         }
      }
  >>
}

Solo =
{
  <<  {\chords { }}
      \new Voice = "first"
      {\voiceOne \relative c'' 
         { 
         }        
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' 
         { 
           fis2..^"Solo" fis8  e8 d4 b8 a b~b4 
           b'4~ \tuplet 3/2 {b8 fis e} fis4 ~
           \tuplet 3/2 {fis8 fis e}  d4 e fis g \bar "||"
         }
      }
  >>
}

Outro =
{
  <<  {\chords {d2 g }}
      \new Voice = "first"
      {\voiceOne \relative c'' 
         { 
           d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2   \bar "||"
           cis4 b4  \tuplet 3/2 {a8 b a}  \tuplet 3/2 {g8 a g}
            d4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2   \bar "||"
           cis2.. fis,8 a2
            \tuplet 3/2 {a8 b a}  \tuplet 3/2 {g8 a b}
           d1
         }        
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' 
         {
           d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2    
           cis4 b  \tuplet 3/2 {a8 b a}  \tuplet 3/2 {g8 a g}         
           d4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2  d,4. a'8 b2   
           cis2.. fis,8 a2
           \tuplet 3/2 {a8 b a}  \tuplet 3/2 {g8 a b} 
           d1
         }
      }
  >>
}

Bbsaxpart =
{
  \tempo 4 = 64
  \key d \major
 
  \relative c^"Intro" \Intro
  
  \repeat volta2
  {
    \relative c^"Verse"\Verse \break
    \relative c^"Chorus"\Chorus \break
  }
  \Solo \break
  \relative c^"Outro Chorus etc"\Outro
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Never Tear Us Apart"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

