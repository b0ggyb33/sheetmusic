
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Love Shack"
  subtitle = "V1"
}


verse =
{r1^"Verse" r1 r1 r1 r1 r1 r1 r1
}
  
%prechorus =
%{<< \chords 
     {
       bes aes bes aes
     }
     \relative c'' 
     \new Voice= "1"
     {
       \voiceOne {f4 r r f ees r r c8 ees f4 r \tuplet 3/2 {r f f} ees r r bes'8 aes} 
     }
     \relative c'' 
     \new Voice= "2"
     {
       \voiceTwo {bes4^"Prechorus" r r bes aes r r f8 aes bes4 r \tuplet 3/2 {r bes bes} aes r r g'8 f} 
     }
  >>
%}

Chorus= 
{<< \chords 
  {
    d1 f g bes
  }
  \relative c''
  \new Voice = "first"
  { \voiceOne r4^"Chorus"  d8 c8 d2 r4 f8 e f2 r4 g8 f g2 r4 bes8 a bes4 bes 
    a1 r r r
    r4 r d,8 c r4   r r r a'   a r d,8 c r4 r r d d 
    r4 r d8 c r4   r r r a'   a r d,8 c r4 r r d\glissando d, \bar "||"
  }
  \relative c'
  \new Voice= "second"
  { \voiceTwo d1 f g bes2. bes4 
    a1 r r r
    r4 r d8 c r4   r r r a'   a r d,8 c r4 r r d d 
    r4 r d8 c r4   r r r a'   a r d,8 c r4 r r d\glissando d, 
  }
 >>
}


Glitterfrag=
{ r1^"..." 
  r^"gltter" 
  << 
  \relative c''
  \new Voice = "first"
  { \voiceOne f4. g a4 r1^"gltter" a4. g f4   r1^"gltter"  f4. g a4  r1^"gltter"  c8 c a a g4 f 
  }
  \relative c'
  \new Voice= "second"
  { \voiceTwo f4. g a4 r1 a4. g f4   r1  f4. g a4  r1  c8 c a a g4 f 
  }
 >>
}

Solofrag=
{ r1^"..."   r^"moving"  r^"funky"  r^"funky" 
  << 
  \relative c'''
  \new Voice = "first"
  { \voiceOne \bar ".|:" d2^"pp behind gtr solo"~d4.c8 d4 d d8 f r4 \bar ":|.:"
     d2~d4.c8 d4 d d  f  \bar ":|."
  }
  \relative c''
  \new Voice= "second"
  { \voiceTwo  \bar ".|:" d2~d4.c8 d4 d d8 f r4 \bar ":|.:"
     d2~d4.c8 d4 d d  f \bar ":|."
  }
 >>
}


BangBangs=
{
  r1 r1^"....."
  << 
  \relative c'''
  \new Voice = "first"
  { \voiceOne \bar ".|:" r1^"bang bang on the" r d4 d r r r1^"repeat x4" \bar ":|."
    d4 d r r r1 d4 d r r r1
    \bar ".|:" d4^"bang baaang" d2 r4  r1 d4 d2 r4 r1^"rpt x2"\bar ":|."
    r1^"What!" r1^"tin roof"\bar "||"
    r4 r d,8 c r4^"etc chorus to end"
  }
  \relative c''
  \new Voice= "second"
  { \voiceTwo  \bar ".|:" r1 r d4 d r r r1 \bar ":|."
    d4 d r r r1 d4 d r r r1
    \bar ".|:" d4 d2 r4  r1 d4 d2 r4 r1\bar ":|."
    r1 r1
    r4 r d8 c r4
  }
 >>
}


Bbsaxpart =
{
  \tempo 4 = 130
  \key d \major  


  \Chorus  
  \Glitterfrag
  \Solofrag
  \BangBangs

}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart 

}
\pageBreak
\markup {"Love Shack   "}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
    %\midi{}
}

