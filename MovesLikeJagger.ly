
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Moves Like Jagger"
  subtitle = "V1"
}
#(set-global-staff-size 23)

Intro = 
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
  {<< 
     \new Voice = "first"
     {\voiceOne \relative c''{ fis1 }}
     \new Voice = "second"
     {\voiceTwo \relative c''{  b1  }} 
  >>}  
}

VerseOdd =
{
  \bar "||"
  \relative c^"Verse"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*3}
}

VerseEven =
{
  \bar "||"
  \relative c^"Verse"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}

FragA =
{<<
    \new Voice = "first"
    {\voiceOne \relative c'' {dis8 dis r e e r r4 r1  }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {b8 b r cis cis r r4 r1 }}
>>}  

FragB =
{<<
      \new Voice = "first"
    {\voiceOne \relative c'' {fis8 fis r gis gis r r4 r1 }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {dis8 dis r e e r r4 r1}}
>>}


Chorus =
{
  {r1_"..." r1_"and it goes like"}
  \repeat volta 2
  {  
    {<<
      \new Voice = "first"
      {\voiceOne \relative c''' {
        r8_"THIS"^"Chorus pp on C1 to ff on C3" cis2. r8 r1
        r8 cis4 cis cis cis8~cis cis4 cis cis cis8
        ~cis cis4 cis cis cis8
        ~cis cis4 cis cis r8
        
        cis,8 e gis (b gis fis) e cis cis4~cis8 e cis4 r

       
        r8 cis'2. r8 r1
        r8 gis4 gis gis gis8~gis gis4 gis gis fis8
        ~fis fis4 fis fis fis8
        ~fis fis4 fis fis r8
        cis8 e gis (b gis fis) e cis cis4~cis8 e cis4 r
      }}
      \new Voice = "second"
      {\voiceTwo \relative c'' {r8 cis2. r8 r1
        r8 gis'4 gis gis gis8~gis gis4 gis gis fis8
        ~fis fis4 fis fis fis8
        ~fis fis4 fis fis r8
        
        cis8 e gis (b gis fis) e cis cis4~cis8 e cis4 r
        
        r8_"OOOH" cis2. r8 r1
        r8 gis4 gis gis gis8~gis gis4 gis gis fis8
        ~fis fis4 fis fis fis8
        ~fis fis4 fis fis r8
        cis'8 e gis (b gis fis) e cis cis4~cis8 e cis4 r
      }}
    >>}
  }
}

Bridge =
{
  \bar "||"
  \relative c^"Bridge"
  \set Score.skipBars = ##t   {R1*4}
}

ChorusTwo =
{
  \relative c^"Short Chorus"
  \repeat volta 2
  \FragA
  \alternative
  {
    \FragB
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {dis2~dis8 (e fis) e~e2 r2 }}
      \new Voice = "second"
      {\voiceTwo \relative c'' {b2~b8 (cis dis) cis~cis2 r2 }}
    >>}
  }
}

ChorusEnd =
{
  \relative c^"End Chorus"
  \repeat volta 2
  \FragA
  \alternative
  {
    \FragB
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {fis2 e dis r fis1  }}
      \new Voice = "second"
      {\voiceTwo \relative c'' {dis2 cis b^"rall" r b1  }}
    >>}
  }
}

OutroOne =
{
  \repeat volta 2
  {<<
     \new Voice = "first"
     {\voiceOne  \relative c''{dis1 r r cis } }
     \new Voice = "second"
     {\voiceTwo  \relative c'' {b1\>  r \! r  ais2~\< ais \!} }
  >>} 
}

OutroTwo =
{
  \repeat volta 2
  {<<
     \new Voice = "first"
     {\voiceOne  \relative c''{dis1 e dis cis } }
     \new Voice = "second"
     {\voiceTwo  \relative c'' {b1\>  b \! \< b  \! \>ais2~\!\< ais \!} }
  >>} 
}

OutroThree =
{
  \repeat volta 2
  {<<
     \new Voice = "first"
     {\voiceOne  \relative c''{dis4. dis8~dis2  e4. e8~e2  dis4. dis8~dis2 cis4. cis8~cis2} }
     \new Voice = "second"
     {\voiceTwo  \relative c''{b4. b8~b2  b4. b8~b2  b4. b8~b2 ais4. ais8~ais2}}
  >>}
}

Bbsaxpart =
{
  \tempo 4 = 130
  \key cis \minor
 
  \Chorus
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Moves Like Jagger"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

