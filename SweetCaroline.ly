
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Sweet Caroline"
  opus = "30 May 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
%\layout{ragged-last =##t}

#(set-global-staff-size 22)

Riff =
{
  \relative c''{  {ees8. f16~f8  g8~} g16 r aes8 aes8 r8 aes,8. aes16~aes8 g~g f ees f}
}

Intro =
{ {<<
   \new Voice = "first"
    {\voiceOne
    \relative c''
      {
        r1 r1
        bes8 (c des4) r bes8 (c des c bes4) des8 (c bes4)
        des8 (ees f4) r des8 (ees
        f2 ees  aes ges)
      }
    } 
    \new Voice = "second"
    {\voiceTwo
    \relative c''
      {
        ges8^"Intro - swung" (aes bes4) r ges8 (aes bes aes ges4) bes8 (aes ges4)  
        ges8 aes bes4 r ges8 aes bes aes ges4 bes8 aes ges4 
        bes8 c des4 r bes8 c 
        des2 c f ees
      } 
    }
  
  {\chords { aes1*7 }}
  >>}
}

Verse = 
{
  {<<
    \new Voice = "first"
    {\voiceOne
    \relative c''
      {
         R1*4 R1*3 r4 r^"Alto V2" r ees
         aes1^" V1 long notes" r4 aes (des aes bes1~bes)
         aes1~aes r4 ees' (c bes aes ges f ees)
      }
    } 
    \new Voice = "second"
    {\voiceTwo
    \relative c''
      {
        r1^"Verse - tacit V1" r4 aes^"Tenor V2" (des aes bes1) r4 r r ees,  
        (des1) r1 r1 r1
        R1*4 R1*4
      } 
    }
    \chords 
    {
      des1*2 ges1*2 des1*2 aes1*2
      des1*2 ges1*2 des1*2 aes1*2
    }
  >>}
}

PreChorus = 
{
  {<<
    \new Voice = "first"
    {\voiceOne
    \relative c''
      {
         des'1\ppp^"PreChorus"~des1 bes1~bes aes~aes ges~ges aes r4 c, (des ees)
      }
    } 
    \new Voice = "second"
    {\voiceTwo
    \relative c''
      {
         des'1~\<des1 bes1~bes aes~aes ges2. ges,4~ges1 aes r4\! aes bes c
      } 
    }
  
    \chords 
    {
      des1*2 bes1*2:m aes1*2 ges1*2 aes1*2
    }
  >>}
}

ChorusStart = 
{
{<<
    \new Voice = "first"
    {\voiceOne
    \relative c''
      {
         f2^"Chorus" r2 r2 ges4-^ f-^  bes,2 r2 r1 r4 aes2. r4 c (des ees) 
      }
    } 
    \new Voice = "second"
    {\voiceTwo
    \relative c''
      {
         des2 r2 r2 ges4 f  bes,2 r2 r1 r4 aes2. r4 aes bes c 
      } 
    }
    
    \chords 
    {
      des1 ges2 ges4 f bes1*2 aes1*2 
      
    }
  >>}
}

ChorusEnd = 
{
  {<<
    \new Voice = "first"
    {\voiceOne
    \relative c''
      {
         f2 r2 r2 ges4-^ f-^  bes,2 r2 r1 
         aes'2\> ges f ees\! 
      }
    } 
    \new Voice = "second"
    {\voiceTwo
    \relative c''
      {
         des2 r2 r2 ges4 f  bes,2 r2 r1 
         aes'2 ges f ees
      } 
    }
    
    \chords 
    {
       
      des1 ges2 ges4 f bes1*2 aes2 ges2 f2:m ees2:m
    }
  >>}
}


Bbsaxpart =
{
  \tempo 4 = 126
  \key des \major
  \time 4/4

  
  \Intro \break
  
  \repeat volta2
  {
    \Verse \break
    \PreChorus \break
    \ChorusStart \break \ChorusEnd \break
    
  }
  \Intro \break
  
  \repeat volta2 {\ChorusStart} \break
  \ChorusEnd
  
 
}


\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Sweet Caroline"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

