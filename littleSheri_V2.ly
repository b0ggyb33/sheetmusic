\version "2.18.2"
\header{
  title = "Little Sheri"
  subtitle = "V1"
}
#(set-global-staff-size 23)

BbsaxpartNotes = \relative c ''{
  r2 r8 bes8 des f~
  \repeat volta 2 {
  f2 r8 bes,8 des f~
  f2 r8 bes,8 des e~
  e8 ees des4 \tuplet 3/2 {ees4 des8} \tuplet 3/2{ bes8 aes bes~} }
  \alternative {
  { bes2 r8 bes8 des f | }
  { bes,2 r8 f8 g aes~ | }
  }
  
  \bar "||"\bar "||"aes2. g4 
  e4 f g aes
  a4. bes8 r2
  r2 r8 ees,8 f ges~
  \bar "||"ges2. f4
  d4 ees f ges
  g4. aes8 r2
  
  r2 r8 bes8 des f~
  
  \bar "||"f2 r8 bes,8 des f~
  f2 r8 bes,8 des e~
  e8 ees des4 \tuplet 3/2 {ees4 des8} \tuplet 3/2{ bes8 aes bes~}
  bes2 r8 bes8 des f~
  
  \bar "||"f2 r8 bes8 \tuplet 3/2 {des8 bes aes}
  \tuplet 3/2 {f4 f8} r4 r8 bes,8 des e~
  e8 ees des4 \tuplet 3/2 {ees4 des8} \tuplet 3/2{ bes8 aes bes~}
  bes2 r2
  
  
}


BbsaxpartCHords = 
{ 

  \chords
  {
    r1 des1:maj7 ges:maj7 c2:m7.5- f:7.9- bes1:m7
    bes:m7
    f1:m7 bes:7.11+ ees:maj7.11+ f2:m7 bes:7.9-
    ees1:m7 aes:7.11+  des:maj7.11+ c2:m7.5- f:7.9-
    des1:maj7 ges:maj7 c2:m7.5- f:7.9- bes1:m7
    des1:maj7 ges:maj7 c2:m7.5- f:7.9- bes1:m7
  }
}

Bbsaxpart =
{   \key bes \minor
  <<
  \BbsaxpartNotes
  \BbsaxpartCHords
>>}

BbsaxBass =
{  \key bes \minor
  
  \relative c'
  {
    r1
    des4 r f r ges r bes, r c ees f ees bes des f2
    bes,4 des f2
    f4 aes c aes bes, d f d ees g bes g f aes bes r
    ees,4 g bes ees, aes, c ees c des f g aes  ges ees f c
    des4 r f r ges r bes, r c ees f ees bes des f2
    des4 r f r ges r bes, r c ees f ees bes des bes2
  }
}


\score 
{ <<
    \new Staff  \with {instrumentName = #"Alto"} 
    { \transpose d b, \Bbsaxpart }
    \new Staff  \with {instrumentName = #"Alto"} 
    {\transpose d b, \BbsaxBass }
>>}

\pageBreak
\markup {"Little Sheri"}


\score 
{
  <<
    \new Staff \with { instrumentName = #"Tenor"}
    \transpose d e {\Bbsaxpart} 
    \new Staff  \with {instrumentName = #"Tenor"} 
    \transpose d e {\BbsaxBass }

>>}