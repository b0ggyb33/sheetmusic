\version "2.18.2"
\header{
  title = "Little Sheri"
  subtitle = "V1"
}
#(set-global-staff-size 23)

BbsaxpartNotes = \relative c ''{
  r2 r8 bes8 des f~
  \repeat volta 2 {
  f2 r8 bes,8 des f~
  f2 r8 bes,8 des e~
  e8 ees des4 \tuplet 3/2 {ees4 des8} \tuplet 3/2{ bes8 aes bes~} }
  \alternative {
  { bes2 r8 bes8 des f~ | }
  { bes,2 r8 f8 g aes~ | }
  }
  
  aes2. g4 
  e4 f g aes
  a4. bes8 r2
  r2 r8 ees,8 f ges~
  ges2. f4
  d4 ees f ges
  g4. aes8 r2
  
  r2 r8 bes8 des f~
  
  f2 r8 bes,8 des f~
  f2 r8 bes,8 des e~
  e8 ees des4 \tuplet 3/2 {ees4 des8} \tuplet 3/2{ bes8 aes bes~}
  bes2 r8 bes8 des f~
  
  f2 r8 bes8 \tuplet 3/2 {des8 bes aes}
  \tuplet 3/2 {f4 f8} r4 r8 bes,8 des e~
  e8 ees des4 \tuplet 3/2 {ees4 des8} \tuplet 3/2{ bes8 aes bes~}
  bes2 r2
  
  
}


BbsaxpartCHords = 
{ 

  \chords
  {
    r1 des1:maj7 ges:maj7 c2:m7.5- f:7.9- bes1:m7
    bes:m7
    f1:m7 bes:7.11+ ees:maj7.11+ f2:m7 bes:7.9-
    ees1:m7 aes:7.11+  des:maj7.11+ c2:m7.5- f:7.9-
    des1:maj7 ges:maj7 c2:m7.5- f:7.9- bes1:m7
    des1:maj7 ges:maj7 c2:m7.5- f:7.9- bes1:m7
  }
}

Bbsaxpart =
{<<
  \BbsaxpartNotes
  \BbsaxpartCHords
>>}


\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"}  
    \transpose d a, \Bbsaxpart 
}

\pageBreak
\markup {"Little Sheri"}


\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
}