\version "2.16.0"  % necessary for upgrading to future LilyPond versions.
\header{
  title = "You Can't Hurry Love"
  subtitle = "V1"
}

Bbsaxpart =
{
  \tempo 4 = 190
  \key c \major 
  
  {r1^"4 bars bass intro" r r r}
  \relative c''
  {c4 r1 r2 r4 \bar "||" r1^"love, love" r r r  r r r r} 
  \relative c''
  \repeat volta 2 
  { f4^"you CAN'T: Verse" e d e r1 a4 g f g r1 g4 a b e, r1 d4 e f e d^"to Break! after V3" e f r}
  \relative c''
  { e4^"Chorus" e e r e e8 e e e e r e4 e e r e e8 e e e e r
    c4 c c r c c8 c c c c r c4 c c r c c8 c c c c r
    a4 a a r a a8 a a a a r a4 a a r a a8 a a a a r
    b4 b b r b b8 b b b b r a4 a a r g g8 g g g g r \bar "||"
  }
  {r1^"Break!" r r r}
  \relative c''
  \repeat volta 2 
  { f4^"After about 2+16 bars: Repeat until end " e d e 
    r1 a4 g f g r1 g4 a b e, r1 d4 e f e d e f r}
}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}
}

