
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Mustang Sally"
  subtitle = "V1"
}
#(set-global-staff-size 23)

Intro = 
{
  \set Score.skipBars = ##t   {R1*3}
 <<
    \new Voice = "first"
    {\voiceOne \relative c'' { r2 r4 r8 } }   
    \new Voice = "second"
    {\voiceTwo \relative c'' { r2 r4 r8 } }   
>>}

Verse = 
{<<
    \new Voice = "first"
    {\voiceOne \relative c'' {
      cis8 \bar ".|:" d4^"Verse"  c2. r2 r4 r8 cis  (d4) c2. r2 r4 r8 cis    
      (d4) c2. r2 r4 r8 cis  (d4) c2. r2 r4 r8 fis 
      (g4) f2. r2 r4 r8 fis  (g4) f2. r4 fis8 g fis g fis cis    
      (d4) c2. r2 r4 r8 cis  (d4) c2. r1    
      a'~a2 a4 gis g-. r r2 r1 fis1 e c r2 r4 r8 
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
      cis8 (d4) a'2._"V1: Grunts only"_"ignore dotted minims" r2 r4 r8 cis,  (d4) a'2. r2 r4 r8 cis,    
      (d4) a'2. r2 r4 r8 cis,  (d4) a'2. r2 r4 r8 fis 
      (g4) d'2. r2 r4 r8 fis,  (g4) d'2. r4 fis,8 g fis g fis cis    
      (d4) a'2. r2 r4 r8 cis,  (d4) a'2. r1    
      a~_"play - any octave!"a2 a4 gis g-. r r2 r1 fis1_"ditto!" e c r2 r4 r8 
    }}
>>}

Chorus = 
{<<
    \new Voice = "first"
    {\voiceOne \relative c'' {
      cis8 (d4^"Chorus") r r8 fis fis e d4 r4 r4 r8 cis  (d4) r r8 fis fis e d4 r4 r4 r8 cis    
      (d4) r r8 fis fis e d4 r4 r4 r8 cis  (d4) r r8 fis fis e d4 d8 r d e r8 fis 
      (g4) r r8 b b a g4 r4 r4 r8 fis  (g4) r r8 b b a g4 r4  fis8 g fis cis    
      (d4) r r8 fis fis e d4 c2 r8 cis  (d4) r r8 fis fis e d4 c2 r4   
      a'1~a2 a4 gis g-. r r2 r1 fis1 e c r2 r4 r8 cis \bar ":|."
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
      cis8 (d4)_"C1: This pattern only" r r8 fis fis e d4 r4 r4 r8 cis  (d4) r r8 fis fis e d4 r4 r4 r8 cis    
      (d4) r r8 fis fis e d4 r4 r4 r8 cis  (d4) r r8 fis fis e d4 d8 r d e r8 fis 
      (g4) r r8  b b a g4 r4 r4 r8 fis  (g4) r r8  b b a g4 r4 fis8 g fis cis    
      (d4) r r8 fis fis e d4 a'2 r8 cis,  (d4) r r8 fis fis e d4 a'2 r4   
      a1_"ditto!"~a2 a4 gis g-. r r2 r1 fis1_"ditto!!" e c r2 r4 r8 cis
    }}
>>\break}

Outro =
{
    \repeat volta2
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {fis1^"Outro x3 - 3rd rall & tumble" e c4 r8 e' d r r4 r r8 e, d r r4}}   
      \new Voice = "second"
      {\voiceTwo \relative c' {fis1 e c~c}}
    >>} <<{d''1\fermata}{d'}>>
}


Bbsaxpart =
{
  \tempo 4 = 140
  \key d \major
 
  \Intro
  \Verse
  \Chorus
  \Outro
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Mustang Sally"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

