
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Gimmie Shelter"
  subtitle = "V1"
}

Intro = {
  \relative c''
  {
   \compressFullBarRests R1*4
   \compressFullBarRests R1*4
   \compressFullBarRests R1*3 r2 r4 c8 bes 
  }
  }

Verseone = {
  \relative c''
  {
    \compressFullBarRests R1*8
  }
  %\chords{bes1 bes bes bes bes bes bes bes}
  }
  
 Versetwo = {
  \relative c''
  {
    \compressFullBarRests R1*8
  }
  %\chords{bes1 bes bes bes bes bes bes bes}
  }

Chorusone = {
  \relative c''
  {
    r8 bes8 des-. bes des16 bes des4.
    r8 bes8 des-. bes des16 bes des4.    
    bes8 aes bes r8 bes8 aes bes des-. 
    r8 bes8-. r8 bes8-. r8 bes8-. r8 bes8-. 
    r8 aes8 bes8 bes des2 
    r4      bes8 bes des2
    r4      bes8 bes des2
    r8 aes8-. r8 bes8-. r8 aes8-. r8 bes8-. 
    r8 bes8-. des-. bes-. r2
    
  }
}

Chorustwo = {
  \relative c''
  {
    r8 bes des bes bes aes bes bes 
    r8 bes des bes bes aes bes bes 
    r8 bes des bes bes aes bes bes 
    r8 aes r bes r bes r bes 
    r8 bes des bes bes aes bes bes 
    r8 bes des bes bes aes bes bes 
    r8 bes des bes bes aes bes bes 
    r8 aes r bes r bes r bes 
    r8 bes8-. des-. bes-. r2
  }
}

ShortGuitarSolo = {<<
  \relative c''
  {
    r1 r1 r1 r4 bes8 \tuplet 3/2{ ees8 des bes} bes8 bes 
  }
  \chords
  {
    bes1 bes1 bes1 bes1 
  }
  
>>}

Solo = {<<
  \relative c''
  {
  
  }
  \chords
  {
    bes bes bes bes
    bes aes ges 
    ges bes
    bes aes ges 
    ges bes
  }
  
>>}

Outro = {
  \relative c''
  {
  r8 bes des bes bes aes bes bes 
  r8 bes des bes bes aes bes bes 
  bes8 aes bes r8 bes8 aes bes des-. 
  r8 aes r bes r bes r bes
  r8 aes8 bes8 bes des2 
  r4 bes8 bes des2 
  r4 bes8 bes des2 
  \repeat volta 2 {
  r8 aes r bes r aes r bes
  r8 bes des bes bes aes bes bes 
  r8 bes des bes bes aes bes bes 
  r8 bes des bes bes aes bes bes 
  } 
    
  }
}

Bbsaxpart =
{
  \tempo 4 = 140
  \key bes \major
  \Intro
  \bar "||"
  \Verseone
  \bar "||"
  \Chorusone
  \bar "||"
  \ShortGuitarSolo
  \bar "||"
  \Versetwo
  \bar "||"
  \Chorustwo
  \bar "||"
  \Solo
  \bar "||"
  \Intro
  \bar "||"
  \Verseone
  \bar "||"
  \Outro
  
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose f c \Bbsaxpart  
}

\pageBreak
\markup {"Gimmmie Shelter"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
     \Bbsaxpart
    %\midi{}
}

