
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Let's Dance"
  subtitle = "V1"
}

Ahs = 
{<< 
    \new Voice = "first"
    \relative c'
    { \voiceOne f8 f f f f f f f a a a a a a a a
       c c c c c c c c ees ees ees ees ees ees ees ees \bar "||"}
    \new Voice= "second"
    { \voiceTwo f,8 f f f f f f f  f f f f f f f f
      a a a a a a a a c c c c c c c c
    }
  >>
}

Cmseven = 
{<< \chords{c1:m7}
    \new Voice = "first"
    \relative c'''
    { \voiceOne g8. g16 g8 r g8 r r4 }
    \new Voice= "second"
    \relative c''
    { \voiceTwo ees8. ees16 ees8 r ees r r4   }
  >>
}

Fseven = 
{<<\chords{f1:7}
    \new Voice = "first"
    \relative c'''
    { \voiceOne a8. a16 a8 r a8 r r4}
    \new Voice= "second"
    \relative c''
    { \voiceTwo ees8. ees16 ees8 r ees8 r r4  }
  >>
}

Aflat = 
{<< \chords{aes1}
    \new Voice = "first"
    \relative c'''
    { \voiceOne aes8. aes16 aes8 r aes8 r r4}
    \new Voice= "second"
    \relative c''
    { \voiceTwo ees8. ees16 ees8 r ees8 r r4  
    }
  >>
}




Chops = { \Cmseven r1 \Fseven r1 \Aflat r1 \Cmseven r1}

Stomp = 
{<<   \new Voice = "first"
    \relative c'
    { \voiceOne r8 c ees c f c ees c  }
    \new Voice= "second"
    \relative c'
    { \voiceTwo r8 c ees c f c ees c  }
>>}


Verse = 
{<< %\chords{c1*2:m7 f1*2:7 aes1*2 c1*2:m7}
    \new Voice = "first"
    \relative c'
    { \voiceOne \bar ".|:"r1^"Verse" r1 r8^"tacit V1" c ees c f c ees c  r8^"play        V3->Outro" c ees c f c ees c \bar ":|."}
    \new Voice= "second"
    \relative c'
    { \voiceTwo r1 r1 r8 c ees c f c ees c  r8 c ees c f c ees c }
  >>
}

Outro =
{ <<
  \Chops
  {r^"Outro"}
  >>
{ << 
    { \bar".|:" \Cmseven \Stomp \Fseven \Stomp \Aflat \Stomp \Cmseven \bar ":|."}
    {r8^"sax solo over"}
  >>
}
}


  

Chorus = 
{<< \chords 
  {
    %c1:m bes1 aes g c:m bes aes g
  }
  \relative c''
  \new Voice = "first"
  { \voiceOne r1^"Chorus" r1 r4 r8 bes c c r4 r4 r8 bes c c r4 
     r1 r2 r4 r8 bes bes bes r bes  c c r c bes bes r bes c c r4 r1 r1 r1 \bar "||"
  }
  \relative c''
  \new Voice= "second"
  { \voiceTwo r1 r1 r4 r8 bes c c r4 r4 r8 bes c c r4 
     r1 r2 r4 r8 bes bes bes r bes c c r c bes bes r bes c c r4 r1 r1 r1 }
 >>
}


Bbsaxpart =
{
  \tempo 4 = 115
  \key f \major  

  \relative c''
  {
  \Ahs
  \Chops
  \Verse
  \Chorus
  \Outro
  }
 
 

}

\score 
{
  \new Staff \with { instrumentName = #"Alto Sax"}  
  \transpose d a, \Bbsaxpart 
}


\pageBreak
\markup {"Let's Dance"}

\score 
{
  \new Staff \with { instrumentName = #"Tenor Sax"}
  \Bbsaxpart   
}

