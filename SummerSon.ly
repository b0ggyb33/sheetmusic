
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Summer Son"
  
  opus = "4 December 2019"
}


Bbsaxpart =
{
  \tempo 4 = 130
  \key g \minor
  
  <<\chords
  {
    g1*2:m c1*2:m f1 d:7 g1*2:m
  }
  \relative c''
  {r4^"2 bars gtr; chorus x2 after V2; xn for outro" bes4 a2 bes8 a bes a~a2 
   r4 bes4 a2 bes8 a bes a~a2 
   r4 bes4 a2 fis2. fis4 
   g8 r bes4 a2 bes8 a bes a~a2}  
  >>
  
  <<\chords
  {
    ees1 f ees f d1*2:m g:m
  }
  \relative c''
  {r1^"Verse" r r r r r r r}  
  >>

  <<\chords
   {
     bes1*2 ees bes c:m ees f
   }
   \relative c''
   {
     d1\>^"PreChorus" r\! ees\> r\! 
   
     {<<
       \new Voice = "first" 
       {\voiceOne \relative c''
         {
           d1\> r\! g\> r\!
           ees8 ees r4 r2 r2 r4 ees8 ees f8 f8 r4 r2 f4 f r2
         }
       }
       \new Voice = "second"
       {\voiceTwo \relative c''
         {
           f1 r ees r 
           bes8 bes r4 r2 r2 r4 bes8 bes c8 c8 r4 r2 c4 c r2
         }
       }
     >>} 
   
   
    
   }  
  >>


}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\markup {"   "}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart
    %\midi{}
}

