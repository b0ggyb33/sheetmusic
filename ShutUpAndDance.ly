
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Shut Up And Dance"
  subtitle = "V2"
}

Intro = 
{ r1^"twangy gtr" r \bar "||"  r^"Intro chorus" r r r\bar "||" 
  r r r r\bar "||" r^"bass & gtr" r r r }

Verse = 
  { r1^"Verse" r r r \bar "||" r r r r\bar "||"}

PreChorus =
  { \relative c''' {r4^"PreChorus" ees, f2 r4 ees c bes r ees f2 bes,8 c ees f~f4 g}
  \bar "||"}
  
OutroPrechorus =
  { \relative c''' {r4^"Out PreChorus" ees, f2 r4 ees c bes 
                    r4 ees f2 r4 ees c bes \bar "||" r ees f2 
                      r4 ees c bes r4 ees f2 bes,8 c ees f~f4 g}
  \bar "||"}

  
Chorus =
  { \relative c'' {ees4^"Chorus" ees8 c~c2 ees4 ees8 f~f2 ees4 ees8 c~c2 bes'4 g f ees8 ees
    \bar "||"ees4 ees8 c~c2 ees4 ees8 f~ f2   bes4. g8 c4 r bes4 g f ees8 ees }}
  
OutroChorus =
  { \relative c'' {ees4^"Repeat Outro Chorus" ees8 c~c2 ees4 ees8 f~f2 ees4 ees8 c~c2 bes'4 g f ees8 ees
    \bar "||"ees4 ees8 c~c2 ees4 ees8 f~ f2    bes4. g8 c4 r bes4 g f ees8 ees }}
Outro =
  { \bar ".|:" \relative c'' {ees2^"Outro x2" c ees f  bes4. g8 c4 r bes4 g f ees8 ees\bar ":|."
    c8 c r ees es r aes aes bes1 }}
  
KeysSolo =
  {\relative c'' {r1^"Keys solo" r r r\bar "||"
    r r c8 c r ees es r aes aes bes1 \bar "||"}}

Bbsaxpart =
{
  \tempo 4 = 130
  \key bes \major  

  \relative c''
  \Intro
  \repeat volta 2
  {\Verse \PreChorus \Chorus}
  \alternative
  {
    {\relative c''
     {bes8^"2 or 4 bars sax" c ees f~ f4 ees  g4 ees8 c~c4 bes
      bes8 c ees f~ f4 ees  g8 f ees c~c bes g f}
    }
    {\relative c''{bes2 c ees f}}
  }
  \KeysSolo
  \OutroPrechorus
  r1^"twangy gtr" r r r \bar "||"
  r^"Outro chorus" r r r\bar "||" 
  r r \relative c'''{bes4. g8 c4 r bes4 g f ees8 ees} \bar "||"
  \OutroChorus
  \Outro
}

\score 
{
  \new Staff \with { instrumentName = #"Alto Sax"}  
  \transpose d a, \Bbsaxpart  
}
\pageBreak
\markup {"Shut Up And Dance"}

\score 
{
  \new Staff \with { instrumentName = #"Tenor Sax"}
  \Bbsaxpart   
  %\midi{}
}

