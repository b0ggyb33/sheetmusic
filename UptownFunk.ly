
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Uptown Funk"
  subtitle = "V1.1"
}

#(set-global-staff-size 23)

Intro = 
{ \repeat volta 4 
  { \relative g' {e4^"Intro" r r r8 e }}
  \alternative 
  {
    { \relative g' {a4 g8 e a4 g8 e }}    
    { \relative g' {a4 g8 e a4 g'8 g }}  
  }
}

VerseOneTwo = 
{ 
  \bar ".|:"\relative g' {r1^"Verse1 & Verse2 ..." r1^"    x4" \bar ":|.|:" e4_"hot" r r r8 e a4 g8 e a4 g8^" x4" e } \bar ":|."
  \relative g' {r1_"Girls" r r r_"Cos"\bar "||" r r r_"Sat" r4_"Don't" r^"to Chorus" r g'8 g} \bar "|."
}

VerseThree =
{
  \bar ".|:"\relative g' {e4^"Verse 3 ..." r r r8 e a4 g8 e a4 g8^" x2" e \bar ":|.|:" e4_"Up" r r r8 e a4 g8 e a4 g8^" x4" e } \bar ":|."
  \relative g' {r1_"Dance" r r r\bar "||" r_"Dance" r r_"Sat" r4_"Don't" r^"to Chorus" r g'8 g} \bar "|."

}

ChorusFragA =
{
  \relative g'' {g16 e d e r8 }
  %\relative g'' \tuplet 10/6 {g8 e d e r}  
}

ChorusFragB =
{
  \relative g'' {r2 r8 g16 g g g g g}
}

Chorus = 
{
  \relative g'' ^"Chorus"{\bar ".|:" \ChorusFragA  \ChorusFragA  \ChorusFragA }
  \relative g'' {g8 r4 r g16 fis e d r1 \ChorusFragB \bar ":|."}
  \repeat volta 3
  {\relative g'' \ChorusFragB \ChorusFragB \ChorusFragB}
  \alternative
  {
    {\relative g' {r1 g'4\fermata_"Stop!" r4 r2^"to V2"}}
    {\relative g' {r1^"to V3"}}
    {\relative g' {g'4 g g^"to Outro" g}} 
  }
}


Outro = 
{<< 
    \relative g''
    \new Voice = "first"
    { 
      \voiceOne  
      r8 g8^"Outro"  r4 r8 g g r r2 r4 g16 fis e d 
      r8 g8 r4 r8 g g r e4 g  \tuplet 3/2 {b8 d e } g8 e\bar "||"
      r8 g,8 r4 r8 g g r r2 r4 g16 fis e d 
      e4 g4 (g2)   e'8 d c b g a a8. g16 r8 g8 r4 r8 g g r r2 r4 g16 fis e d 
      r8 g8 r4 r8 g g r e8 e g g \tuplet 3/2 {b8 d e } g8 e  \bar "||"
      r8 g,8 r4 r8 g g r r2 r4 g16 fis e d 
      e4 g8 a4 b8 d e 
      \tuplet 3/2 {g8 g g}  \tuplet 3/2 {g8 r g}  \tuplet 3/2 {g8 g g} g4\fermata  \bar "||"
    }
    \relative g''
    \new Voice= "second"
    { \voiceTwo
      r8 g,8 r4 r8 g g r  r2 r4 g16 fis e d  
      r8 g8 r4 r8 g g r  e4 g   \tuplet 3/2 {b8 d e } g8 e
      r8 g,8 r4 r8 g g r  r2  r4 g16 fis e d  
      e4 g4 (g2)       e'8 d c b g a a8. g16 
      r8 g8 r4 r8 g g r  r2 r4 g16 fis e d  
      r8 g8 r4 r8 g g r  e8 e g g \tuplet 3/2 {b8 d e } g8 e 
      r8 g,8 r4 r8 g g r  r2  r4 g16 fis e d 
      e4 g8 a4 b8 d e 
      \tuplet 3/2 {g8 g g}  \tuplet 3/2 {g8 r g}  \tuplet 3/2 {g8 g g} g4\fermata      
    }
 >>
}



Bbsaxpart =
{
  \tempo 4 = 115
  \key g \major  

  \Intro 
  \VerseOneTwo
  \VerseThree
  \Chorus
  \Outro
  
}

\score 
{
  \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
  \transpose d a, \Bbsaxpart 
  %\midi{}
}

\pageBreak
\markup { "Uptown Funk"  }
\score 
{
    
  \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
  \Bbsaxpart   

}

%-------------------------------------------------------
OutroOLD = 
{<< 
    \relative g''
    \new Voice = "first"
    { 
      \voiceOne  
      r8^"Outro" g r4 r8 g g r a,4 g8 e r4 g'16 fis e d 
      r8 g r4 r8 g g r a,4 g8 e \tuplet 3/2 {b''8 d e } g8 e\bar "||"
      r8 g, r4 r8 g g r a,4 g8 e r4 g'16 fis e d 
      r4 g2.  e'8 d c b g a a8. g16  \bar "||"
      r8 g r4 r8 g g r a,4 g8 e r4 g'16 fis e d 
      r8 g r4 r8 g g r g8 g g g \tuplet 3/2 {b8 d e } g8 e  \bar "||"
      r8 g, r4 r8 g g r a,4 g8 e r4 g'16 fis e d 
      e4 g8 a4 b8 d e 
      \tuplet 3/2 {g8 g g}  \tuplet 3/2 {g8 r g}  \tuplet 3/2 {g8 g g} g4\fermata  \bar "||"
    }
    \relative g''
    \new Voice= "second"
    { \voiceTwo
      r8 g, r4 r8 g g r  a4 g8 e r4 g16 fis e d  
      r8 g r4 r8 g g r  a4 g8 e  \tuplet 3/2 {b'8 d e } g8 e
      r8 g, r4 r8 g g r  a4 g8 e  r4 g16 fis e d  
      g1 e'8 d c b g a a8. g16 
      r8 g r4 r8 g g r  a4 g8 e r4 g16 fis e d  
      r8 g r4 r8 g g r  g8 g g g \tuplet 3/2 {b8 d e } g8 e 
      r8 g, r4 r8 g g r  a4 g8 e  r4 g16 fis e d 
      e4 g8 a4 b8 d e 
      \tuplet 3/2 {g8 g g}  \tuplet 3/2 {g8 r g}  \tuplet 3/2 {g8 g g} g4\fermata      
    }
 >>
}
