
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Don't you want me"
  subtitle = "V1"
}
#(set-global-staff-size 26)

  
SynthSolo =
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}


Verse =
{
  << 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c''' 
      {
        r1 r r r
        r1 r r r2 r8 a b16 a r8
        g4 r r2 r2 r4 a g r r2 r2 r8 a b16 a r8
        g4 r r2 r2 r4 a g r r2 r     
     } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c'''
      {
        r1 r r r
        r1 r r r2 r8 a b16 a r8
        g4 r r2 r2 r4 a g r r2 r2 r8 a b16 a r8
        g4 r r2 r2 r4 a g r r2 r                
      }}
    }
    {\chords 
     {
       g1 a g a \bar "||"g a g a
       \bar "||"g1 a g a \bar "||"g a g a
     } 
    }
  >>
}
  
  
PreChorus = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r1 r r r
        r1 r r r
        d1 e fis ais4 ais ais ais
      }}    
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r1 r r r
        r1 r r r
        b1 cis d fis4 fis fis fis
      }}
    }
    \chords
    {
      b1:m fis:m g e2:m a 
      \bar "||"b1:m fis:m g e2:m a
      \bar "||"b1:m cis:m d fis 
    }
  >>
}  

Chorus = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r1 r2 r4 e8 fis
        g2 r2 r2 r8 a b16 a r8
        g4 r r2 r2 r4 e8 fis
        g2 r2 r1
      } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r1 r2 r4 e8 fis
        g2 r2 r2 r8 a b16 a r8
        g4 r r2 r2 r4 e8 fis
        g2 r2 r1
      }}
    }
    {
      \chords 
      { 
         g1 a g a g a g a 
      } 
    }
  >>
}


ChorusMod = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c''' 
      {
        b4 r r2 r2 r4 g8 a
        b2 r2 r2 r8 cis d16 cis r8
        b4 r r2 r2 r4 g8 a
        b2 r2 r2 r8 cis d16 cis r8
      } }   
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c'''
      {
        g4 r r2 r2 r4 e8 fis
        g2 r2 r2 r8 a b16 a r8
        g4 r r2 r2 r4 e8 fis
        g2 r2 r2 r8 a b16 a r8
      }}
    }
    {
      \chords 
      { 
         g1 a g a g a g a 
      } 
    }
  >>
}




Outro = 
{<< 
    {
      \new Voice = "first"
      {\voiceOne 
      \relative c'' 
      {
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
        r4 d' g,8 g r4 d' d8 g,4 a8 b4 
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
      }  }  
    }
    {
      \new Voice= "second"
      {\voiceTwo
      \relative c''
      {
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1
        r4 d' g,8 g r4 d' d8 g,4 a8 b4 
        r4 d g,8 g r4 d' d8 g,4 a8 b4 b8 a g2 r4 
        b4 b a8 g fis4 g2. r4 r1^" etc .."
                
      }}
    }
    {\chords {} 
    }
  >>
}




Bbsaxpart =
{
  \time 4/4
  \tempo 4 = 120
  \key d \major


  \relative c^"Intro"  \set Score.skipBars = ##t   {R1*2}
  \bar "||"\relative c^"Synth" \SynthSolo
  \repeat volta 2
  {
   \relative c^"Verse" \Verse
    \bar "||" \relative c^"PreChorus" \PreChorus
    \bar "||" \relative c^"Chorus" \Chorus
  }
  \relative c^"Synth" \SynthSolo
  \bar "||"  \relative c^"Chorus" \Chorus
  \repeat volta 2
  {
    \relative c^"Chorus Outro" \ChorusMod
  }


}

\score 
  {
    \new Staff \with {instrumentName = #"ALTO"}  
    \transpose d a, \Bbsaxpart 
    
}

\pageBreak
\markup {"Don't you want me"}

\score 
  {
    \new Staff \with { instrumentName = #"TENOR"}
    \Bbsaxpart 
    %\midi {}

}

