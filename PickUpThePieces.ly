
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Pick Up the Pieces"
  %subtitle = "V1"
  opus = "15 Nov 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

%#(set-global-staff-size 26)

Intro =
{ \relative c^"Intro"
  \set Score.skipBars = ##t   {R1*4}  
}

Tail =
{
  \relative c''
  {
    { \override Stem.direction = #UP
      c'16 f, g 
    }
    {<<
    \new Voice = "first" 
    {\voiceOne \relative c'''{bes16~bes8 g f16 g r bes~bes8 g f16 g r8}}
    \new Voice = "second"
    {\voiceTwo \relative c''{f16~f8 e d16 e r f~f8 e d16 e r8}}
    >>}
  }
}


ASection = 
{
  <<
    \relative c''
    {  \override Stem.direction = #UP
       bes16^\segno d^"repeat fist time only" a' g  r8  f16 c e d r8    \Tail
       R1*2
       bes16 d a' g  r8  f16 c e d r8    \Tail
       R1*2
       bes'16 d, a' g r8 bes c16 bes r8   \Tail
       R1*2 \bar "||"R1*2^"1st & 2nd only"
    }
    \chords 
    {

    }
  >>
}



Finish =
{
  <<
    \relative c''
    { 
       bes16 d a' g  r8  f16 c e d r8    \Tail
    }
    \chords 
    {

    }
  >>
}

Temp =
{ <<
  
  \new Voice = "first"    
  {\voiceOne \relative c''' 
   {
     
   }
  }   
    
  \new Voice = "second"
  {\voiceTwo \relative c'' 
   {
     
   }
  }
  
  
  \chords
  {}>>
}



BSection = 
{
  <<
  \new Voice = "first"    
  {\voiceOne \relative c''' 
    {
      \repeat volta2{c16 bes c bes g8 c16 bes c bes f8 r4 
      c'16 bes c bes g8 c16 bes c^"x3 after solo" bes f8 r4 }
      c'16 bes c bes g8 c16 bes c bes d8 r4
      c16 bes c bes g8 c16 bes c bes f8 d'4~
      d1^\coda^"after solo"~\bar "||"d~\bar "||"d^"1st DS"~\bar "||"d^"2nd time"
    }
  }   
    
  \new Voice = "second"
  {\voiceTwo \relative c''' 
   {
     g2~g8 f r4  g2~g8 f r4  g2~g8 d r4  g2~g8 f d4~d1~d1~d1~d1   
   }
  }
  >>
}

SaxSolo =
{ <<
  
  \new Voice = "first"    
  {\voiceOne \relative c ''''
   {
     bes4~ g d16 f8. \grace cis16~ d4
     bes16 b( g d g4) d16( d c d bes8) g'-.
     r8 g16 bes r c r bes \tuplet 3/2 {des c bes} g d f8 g
     bes4 c8. d16~ d8 f g r
     g,16 g r g f'-> r g, g'-> r8 bes8-> g16 g fis r
     f16-- f-. r8 \grace c'16 d8. bes16 r g r d f8 g
     g,16-- g-. r8 g16 g bes c r c8 bes16 c8 d16 r
     f16-- f-. r8 f16 g8 g16~ g8 bes~ g8 r8
     g,16-- g-. cis' d~ d2 cis16 d bes8
     g16 g8 d16 f8 g16 g~ g8 bes cis16 d-> r8
     r8 d r f-. b2->~ b1
     a2~( \tuplet 3/2 {a8 cis, d} c8) r
     r4 bes'8-> r cis,16 d8-. r16 a8 g
     r4 \grace gis8~ a16 c8.~ c4 \grace g8~ a16 c4.
     d1
   }
  }   
    
  \new Voice = "second"
  {\voiceTwo \relative c'' 
   {
     r1^"Solo" r  r r\break
     c8 c r4 r4 c8  b bes bes r4  r r 
     c8 c r4 r4 c8  b bes bes r4  r r \break
     c8 c r4 r4 c8  b bes bes r4  r r 
     c8 c r4 r4 c8  b bes bes r4  r r \break
     r1 r r r^".....DS"  
   }
  }
  
  
  \chords
  {c1*12 g1*4:m7/d}>>
}

Bbsaxpart =
{
  %\tempo 4 = 148
  \key bes \major
  \time 4/4
  
  \Intro
  \repeat volta2
  {
   \repeat volta2 { \ASection}
   \BSection
  } \break
  \transpose a d \SaxSolo \bar "||"\break
  
   \relative c^"Outro"
   R1*8
  \Finish
  
}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}

    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Pick Up the Pieces"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart
    %\midi{}
}

