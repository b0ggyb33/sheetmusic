
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Get The Party Started"
  %subtitle = "V1"
}
\paper {top-margin = 0 bottom-margin = 0 left-margin = 2 right-margin = 2}

#(set-global-staff-size 23)

Intro =
{
  \relative c''
  { r4 e8 e gis gis fis e
    \repeat volta3 {cis4^\markup \bold "Intro x3" r r2 r1 r1 r4^\markup \bold"tacit 3rd" e8 e gis gis fis e}
  }
}

VerseOne = 
{
  \relative c''
  { 
    r1^\markup \bold"Verse 1" r4 e r2 r1 r4 e r2 
    r1 r4 e8 e r2 r1 r4 e r2 
    r1 r4 e r2 r1 r4 e8 e gis gis fis e
  }
}

VerseTwo = 
{
  \relative c''
  { 
    r4^\markup \bold"Verse 2" e r b gis r gis8 gis b4 r4 e r b gis gis8 gis b b cis4  
    r4 e r b gis r b r e8 cis4 b4 r8 r4 r4 gis b8 b cis4
    r4 e r b gis r gis8 gis b4 r4 e r b gis e'8 e gis gis fis e
  }
}

VerseThree = 
{
  \relative c''
  { 
    r1^\markup \bold"Verse 3" r4 e8 e r2 r1 r4 e r2 
    r1 r4 e8 e r2 r1 r4 e r2 
    r1 r4 e r2 r1 r4 e8 e gis gis fis e
  }
}

Chorus =
{
  \relative c''
  { cis4^\markup \bold"Chorus" r r2 r1 r1 r4 e8 e gis gis fis e
    cis4 r r2 r1 r1 r1
  }
}

ChorusTwo =
{
  \relative c''
  { cis4^\markup \bold"Chorus 2" r r2 r1 cis8 cis r4 r2 r4 e8 e gis gis fis e
    cis4 r r2 r4 cis8 cis r2 cis8 cis r4 r gis  r4 e'8 (cis) fis (e) cis' r
  }
}

ChorusThree =
{
  \relative c''
  { cis4^\markup \bold"Chorus 3" r r2 r4 cis r2 cis8 cis r4 r2 r4 e8 e gis gis fis e
    cis4 r cis r r4 cis8 cis r2 cis8 cis r4 r gis  r4 e'8 (cis) fis (e) cis' r
  }
}

Break =
{
  \relative c''
  { r1^\markup \bold"Break" r4 cis 8 cis e e fis (cis) r1 r4 gis'8 r gis fis e cis
    r1 r4 cis 8 cis e e fis (gis) r1 r1
  }

}

Outro =
{
  \relative c''
  { r4^\markup \bold"Outro" cis 8 cis e e fis (cis) r1 r4 gis'8 r gis fis e cis
    r1 r4 cis 8 cis e e fis (gis) r1 r4 gis8 r gis fis e cis r1 gis'1
  }

}



Bbsaxpart =
{
  \tempo 4 = 130
  \key cis \minor
  \time 4/4
  
  \Intro 
  \VerseOne
  \bar "||" \Chorus
  \bar "||" \VerseTwo
  \bar "||" \ChorusTwo
  \bar "||" \Break
  \bar "||" \VerseThree
  \repeat volta 3 {\ChorusThree}
  \Outro
}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Get The Party Started"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 


}

