
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Suspicious Minds"
  opus = "30 April 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
\layout{ragged-last =##t}

%#(set-global-staff-size 25)

Intro =
{ 
  \relative c^"Intro"
  { \compressFullBarRests
    R1*2
  }
}

Verse = 
{
  <<
    \relative c''
    { 
      cis1^"Verse:V1: Play where marked"_"V2 play all" ~cis d~d 
      e1 d r4 b8^"V1: Play here ......" (a b4) cis  d4 cis b8 (a cis4) \break
      <<
        \new Voice = "first"
        {\voiceOne \relative c'' {e1^"... only lower note & stop" ~e fis~fis gis1 a  gis2 fis2 e d}}
        \new Voice = "second"
        {\voiceTwo \relative c''{cis1 ~cis d~d e1 fis e2^"V1: Play here ..." d cis b}}
      >>
    }
    \chords 
    {
      a1*2 d e1 d1 a1*2
      a1*2 d1*2 e1 d1 e2 d cis:m e:7
    }
  >>
}

VerseFinal = 
{
  <<
    \relative c''
    { 
      r1^"Final Verses: V3, gtr solo, V4"\ppp _"V3: slow build through V4" r r r 
      e1 d r4 cis8 (b) fis' (e4.)~e4 cis8 (b) fis' (e4.)\break
      <<
        \new Voice = "first"
        {\voiceOne \relative c'' { e8 e e4 r2  r1 d8 d d4 r2  r2  a'8 (fis e cis e1) d1 gis2 fis2 e d}}
        \new Voice = "second"
        {\voiceTwo \relative c''{}}
      >>
    }
    \chords 
    {
      a1*2 d e1 d1 a1*2
      a1*2 d1*2 e1 d1 e2 d cis:m e:7
    }
  >>
}

ChorusStart = 
{
  <<
    \relative c''
    { 
      d1^"Chorus" cis2~cis8 cis (b cis e1) fis2 gis  
      a1\> gis fis\!

    }
    \chords 
    {
      d1 a cis d2 e fis1:m cis d 
    }
  >>
}

ChorusEnd = 
{
  <<
    \relative c''
    { 
      fis2_"very slow ..."^"mi.." f2^"..inds" 
      \time 6/4 fis1. e^"love" d e^"tears" 
      fis gis^"good" a gis^"know"
      a,1.^"lie" r1.^"Mmm" e2.~e^"yeah" \time 4/4 r4^"yeah!"_"back to tempo" e'-. e-. e-.
    }
    \chords 
    {
      cis1:7 
      fis1.:m cis d e 
      fis1.:m cis d e   a d a e1:7
    }
  >>
}


Outro =
{
  <<
    \relative c''
    { 
      <<
        \new Voice = "first"
        {\voiceOne \relative c''' 
          {
             a1^"Outro" ~a~a~a2 
              a8 (fis e cis e1) \break d1
             r4 cis'8 (b) fis' (e4.)~e4 cis8 (b) fis' (e4.)
          }
        }
        \new Voice = "second"
        {\voiceTwo \relative c''
          {
            e8\ff e e4 r2  e8 e e4 r2
             d8 d d4 r2  d8 d d4 a'8 fis e cis 
             e1 d1
             a4 r r r8 a8 a4 r r r
          }
        }
      >>
    }
    \chords 
    {
      a1*2 d e1 d1 a1*2     
    }
  >>
}




Bbsaxpart =
{
  \tempo 4 = 118
  \key a \major
  \time 4/4

  
  \Intro
  
  \repeat volta2
  {

    \Verse \break
    \ChorusStart
  }
  
  \alternative
  {
    {<<{r1}\chords {e2 e:7}>> }
    {\ChorusEnd}
  } \break
  \repeat volta2 {\VerseFinal }\break
  \repeat volta2 {\Outro}
 
}


\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Suspicious Minds"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

