
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Does Your Mother Know"
  %subtitle = "V1"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 10 right-margin = 10}

#(set-global-staff-size 23)



Intro =
{
  \relative c^"Intro: Add drums"
  { \compressFullBarRests R1*4 
    
    \repeat volta2  
    { \relative c'' 
      {r4 cis8 (d) cis r r cis~cis (d) cis (d) cis r r4} 
    }
  }
}

Verse = 
{
  <<
    \relative c''
    { 
      \repeat volta 2
      {
        r1^"Verse" r1 \time 2/4 r2 \time 4/4 d2 cis
        b a \break gis1 r2 r4 gis a1 a4\segno b^"Play 2nd time" cis d
      }

    }
    \chords 
    {
      a1. fis2:m2 a2 d2 a/cis b:m a e1*2 a1*2
    }
  >>
}

Chorus = 
{
  <<
    \relative c''
    { 
      \repeat volta 2
      {
        a2^"Chorus x2 - on Outro xn" b a b a a  a8 b c4 cis8 (c b4)
      }

    }
    \chords 
    {
      d2 a:sus4 d a:sus4 d1 a1
    }
  >>
}

Bridge = 
{
  <<
    \relative c'''
    { 
      \repeat volta 2
      {
        a2\p^"Bridge - play 1st time only" e\> fis d 
        e cis_"2nd Bridge -> D.S & Outro"^"1st Bridge -> Intro" b a\!
      }

    }
    \chords 
    {
      a1 d a1*2
    }
  >>
}

Bbsaxpart =
{
  \tempo 4 = 96
  \key a \major
  \time 4/4
  
  \relative c^"Synth"
  {\compressFullBarRests R1*4}
  \repeat volta3
  {
    \Intro \break
    \Verse \break
    \Chorus \break
    \Bridge
  }


}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
%\pageBreak
\markup {"  "} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

