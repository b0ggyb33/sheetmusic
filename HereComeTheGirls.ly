
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Here Come The Girls"
  subtitle = "V1"
}
#(set-global-staff-size 19)

LeadIn = 
{<<
    \new Voice = "first"
    {\voiceOne \relative c''' {
      a16 a
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
      d16 d
    }}
>>} 


DrumsIntro = 
{
  \set Score.skipBars = ##t   {R1*3}
  { r2 r4 r8}
  \LeadIn
}

MainHornRiff =
{\repeat volta2{
{<<
    \new Voice = "first"
    {\voiceOne \relative c''' {
       a16 a a8 a c16 c c c c8 c b16 b
       b8 a16 a a8 a16 a a a a8 a a16 a
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
       d16 d d8 d f16 f f f f8 f g16 g
       g8 f16 f f8 d16 d d d d8 d d16 d
    }}
>>}} }

VerseOdd =
{
  \repeat volta2 
  {\relative c
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {
        d8 r r f r2 \tuplet 3/2 {g8 r f r d r} r2 d8 r r f r2
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c' {
      
      }}
    >>}
  }
  \alternative
  {
    {\relative c''' \tuplet 3/2 {g8 r f r d r} r2}
    {\relative c''' \tuplet 3/2 {g8 r f r d r} r4 r8 \LeadIn}
  }
}

VE = 
{
  \relative c
  {<<
    \new Voice = "first"
    {\voiceOne \relative c'' {
      d2
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
      b2  
    }}
  >>} 
  r2

  \relative c
  {r2 r8 }
  {<<
    \new Voice = "first"
    {\voiceOne \relative c''' {
      b8 b
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
      b8 b  
    }}
  >>} 
  {r8}
}



VerseEvenOne =
{
  \VE
  \VE
  <<{\chords {}}
      \new Voice = "first"
      {\voiceOne \relative c'' 
         {d2\< r4\! b16 a r8 b16 r b'8}        
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' 
         { b2 r4   b16 a r8 b16 r b'8 }
      }
  >>
  r4 r2  
  \relative c
  {<<
    \new Voice = "first"
    {\voiceOne \relative c'' {
      d2
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {
      b2  
    }}
  >>} 
  r2
  r2 r4 r8 \LeadIn
}

VETwo =
{
  r2 r4 r16
  <<{\chords {}}
      \new Voice = "first"
      {\voiceOne \relative c' 
        {fis16 a bes b16}       
      }   
      \new Voice = "second"
      {\voiceTwo \relative c' 
        {}     
      }
  >>
  r16 r8 r4 r8 
  <<{\chords {}}
      \new Voice = "first"
      {\voiceOne \relative c'' 
        {cis8}       
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' 
        {a8}     
      }
  >>
  
  r4
}
VerseEvenTwo =
{
  r2 r8
  <<{\chords {}}
      \new Voice = "first"
      {\voiceOne \relative c'' 
        {cis8}       
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' 
        {a8}     
      }
  >>
  r4
  \VETwo
  \VETwo
  \VETwo
  r2 r4 r8 \LeadIn 
}


Naas =
{
  \repeat volta2
  {
    <<{\chords {}}
      \new Voice = "first"
      {\voiceOne \relative c''' 
         {a4 g f8 f g4 f e8 (d) d4 r
         a'4 g f8 f g4 f e d4 r}
         
      }   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        
      }}
    >>
  }
}

Solos =
{
  \relative c
  {
    <<
    \chords{d1 d g g }
    \new Voice = "first"
    {\voiceOne \relative c'' {
     %\tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
     \relative c''' 
     {r4 r8 c16 b a g f d c8 d f4 f8 d r4 r8 f}
     %\tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2
     \relative c''' 
     {g4 g8 f r2 g8 g g8. g16 g f8 r4 } 
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
     \tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
     \tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2 \bar "||"  \break   
    }}
    >>
    
    <<
    \chords{d1 d g g }
    \new Voice = "first"
    {\voiceOne \relative c''' {
      a8. g16 a8. g16 a8. g16 f8. d16   d8 d8 d16 d8. c4 r
      g'8 g g8. g16 f4 r4  d8 d d8. d16 c4 r4
     %\tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
     %\tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
     \tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
     \tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2 \bar "||" \break  
    }}
    >>

    <<
    \chords{d1 d g g }
    \new Voice = "first"
    {\voiceOne \relative c''' {   
     \tuplet 3/2 {a8 a a} a8 a a g4.  \tuplet 3/2 {a8 a a} a8 a a g4. 
     \tuplet 3/2 {g8 g g} g8 g g f4.  \tuplet 3/2 {g8 g g} g8 g g f4. 
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
     \tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
     \tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2 \bar "||"  \break     
    }}
    >>
    
    <<
    \chords{d1 d g }
    \new Voice = "first"
    {\voiceOne \relative c''' {
      d8. c16 a8 gis f d c d  f4 f8 d r2
      g4 g8 f r2 
     %\tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
     %\tuplet 3/2 {f4 fis g} r2
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
     \tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
     \tuplet 3/2 {f4 fis g} r2
    }}
    >>

    <<
      \chords{}
      \new Voice = "first"
      {\voiceOne \relative c'' {
       \tuplet 3/2 {d8^"As written" e r} f8 g a2~a2.
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c' {
       \tuplet 3/2 {d8 e r} f8 g a2~a2.     
      }}
    >>
    r8 \LeadIn
  }
}

Outro =
{
  \relative c
  {
  <<
    \chords{}
    \new Voice = "first"
    {\voiceOne \relative c'' {
     \tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2
     
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
     \tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2
      
    }}
  >>

  <<
      \chords{}
      \new Voice = "first"
      {\voiceOne \relative c'' {
       \tuplet 3/2 {c4 cis d} r2  \tuplet 3/2 {c4 cis d} r2
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c' {
       \tuplet 3/2 {c4 cis d} r2 \tuplet 3/2 {c4 cis d} r2 \bar "||"     
      }}
  >>
  <<
    \chords{}
    \new Voice = "first"
    {\voiceOne \relative c'' {
     \tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2
    
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
     \tuplet 3/2 {f4 fis g} r2  \tuplet 3/2 {f4 fis g} r2
    
    }}
  >>
 
  <<
      \chords{}
      \new Voice = "first"
      {\voiceOne \relative c'' {
       \tuplet 3/2 {d8 e r} f8 g a4 cis~cis1 d16 c a g a c cis8 d4\fermata r4
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c' {
       \tuplet 3/2 {d8 e r} f8 g a4 cis~cis1 d16 c a g a c cis8 d4 r4  
      }}
   >>

  }
}


Bbsaxpart =
{
  \tempo 4 = 104
  \key d \minor
 
  \relative c^"Intro Drums" \DrumsIntro
  \relative c^"Intro Riff rpt x2" \MainHornRiff
  \relative c^"Chorus rpt x2" \MainHornRiff  
  \relative c^"Verse 1" \VerseOdd  
  \relative c^"Chorus rpt x2" \MainHornRiff
  \relative c^"Verse 2" \VerseEvenOne 
  \relative c^"Chorus rpt x FOUR" \MainHornRiff
  \relative c^"Verse 3 - No Repeat" \VerseOdd  \pageBreak
  \relative c^"Naa Naas - Play 2nd time" \Naas  
  \relative c^"Verse 4" \VerseEvenTwo 
  \relative c^"Chorus rpt x FOUR" \MainHornRiff 
  \relative c^"Solos AdLIb" \Solos
  \relative c^"Chorus rpt x FOUR" \MainHornRiff
  \relative c^"Outro" \Outro 
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Here Come the Girls"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

