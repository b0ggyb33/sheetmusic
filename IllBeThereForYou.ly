
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "I'll Be There For You"
  %subtitle = "V1"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

%#(set-global-staff-size 26)

SaxA =
{
  \relative c''
  { 
    r4 e e8 dis r e~e4 r8  cis e fis r gis~gis4 (a) a (e) e dis cis dis  
  }
}

SaxB =
{
    \relative c''
    { 
        r4 r8 e e dis cis e~e4 r8 e fis gis r gis~gis4 (a) a (gis) b1^"To verse" 
    }
}

Intro =
{
  \repeat volta3 {b'4_"build each time"\fermata^"with gtr" r r r r1} 
  \repeat volta3 {b'4\fermata^"repeat with drums" r r r r1^"(lost rpt sign!)"} 
}

VerseOdd = 
{
  <<
    \relative c''
    { 
      b1^"ODD No VERSE" r r cis
      r1 r r dis
    }
    \chords 
    {
       b1*3 a1  
       b1*3 dis1:m \break
    }
  >>
}
VerseEven = 
{
  <<
    \relative c''
    { 
      r1^"EVEN No VERSE" r r b4 r8 b r fis' gis r
      a1 gis fis r2 fis8 e dis cis
    }
    \chords 
    {
      a1 e b1*2 \break
      a1 e fis fis:7 \break
    }
  >>
}

LastVerseEven = 
{
  <<
    \relative c''
    { 
      r1^"EVEN No VERSE" r r b4 r8 b r fis' gis r
      a1 gis fis ~fis^"NB: Extra bar here!" r2 fis8 e dis cis
    }
    \chords 
    {
      a1 e b1*2 \break
      a1 e fis fis:7 \break
    }
  >>
}


Chorus = 
{
  <<
    \relative c''
    { 
      b2.^"CHORUS" b4 e,2 e fis1 r^"      x3"
      r1 cis'2^"Twice at end" cis
    }
    \chords 
    { 
      \repeat volta3 {b1 e fis1*2 }
      a1*2 \break
    }
  >>
}

Bridge = 
{
  <<
    \relative c''
    { 
      b1^"Extra 2 bars before?" r \bar "||"
      r1^" BRIDGE" r r r  r1 r r r  
      cis1~\<cis b~b a~a ais b2 cis dis1^"Gtr solo starts"~dis\!
    }
    \chords 
    { 
      b1*2
      e1*2 e1*2 gis1*4:m \break
      cis1*2:m b1*2 a1*2 \break
      fis1 e2 fis2 gis1:m e1 \break
      
    }
  >>
}




GtrSolo =
{
  <<
    \relative c'' { r1^"Gtr Solo continues" }
    \chords 
    { 
               fis1 fis 
      gis1:m e fis fis 
      gis1:m e fis fis 
      b1 e fis fis \break 
    }
  >>
}




LastBit = 
{
  <<
    \relative c''
    { 
      b1
    }
    \chords { b1*2 }
  >>
}


Bbsaxpart =
{
  \tempo 4 = 188
  \key b \major
  \time 4/4

  \Intro \break
  \bar "[|:"
    \VerseOdd
    \VerseEven
    \Chorus
  \bar ":|]"
  \Bridge
  \GtrSolo
  \LastVerseEven
  \repeat volta2 {\Chorus} \LastBit
}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}

    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"I'll Be There For You"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart
    %\midi{}
}

