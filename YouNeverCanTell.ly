\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "You Never Can Tell"
  opus = "Version 5 May 19"
}

variation =
#(define-music-function
     (parser location phrase)
     (ly:music?)
   #{
     << \chords 
     {
       fis1*2
       cis1*2:7
     }
     \relative c'
     {
         $phrase
         \transpose fis cis $phrase
     }
     >>
   #})

BrassOne = 
{<< \chords 
     {
       fis1*4
       fis1*2 cis1 cis1:7
       cis1*2 cis1*2:7
       cis1 cis1:7 fis1 cis1:7
     }
    \relative c'
    { r1 r1 r8 fis ais b c cis fis e c cis ais fis r2 \break
     r1 r1 r8 cis eis fis g gis cis b g gis eis cis r2 \break
     r1 r1 r8 cis eis fis g gis cis b g gis eis cis r2 \break
     r1 r1 r8 ais' a ais a ais a ais fis dis4 cis8 r2     
    }
  >>
}

SoloSection = 
{<< \chords 
     {
       cis1 \bar "||" fis1*4
       fis1*2 cis1 cis1:7
       cis1*2 cis1*2:7
       cis1 cis1:7 fis1 cis1:7
     }
    \relative c''
    { r2^"After final verse ..." r8 cis dis f 
      fis4^"Outro/Solo Section" dis8 f4 cis4 dis8~dis ais4 cis4 gis4 ais8~ais2 r2 r2 r8 cis dis f \break
     fis4 dis8 f4 cis4 dis8~dis ais4 cis4 c4 b8~b2 r2 r2 r8 cis dis e  \break
     f4 cis8 dis4 b4 cis8~cis8 gis4 cis4 c4 b8~b2 r2  r2 r8 cis dis e  \break
     f4 cis8 dis4 b4 cis8~cis8 gis4 b4 ais4 fis8~fis2 r2 fis dis4 cis8 r2    
    }
  >>
}


verse =
{r1^"Verse" r1 r1 r1 r1 r1 r1 r1
}
  

Bbsaxpart =
{
  \tempo 4 = 130
  \key fis \major
  %\SoloSection \break
  
  \BrassOne \break
  \SoloSection  
}

\score 
  {
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a \Bbsaxpart
}

\pageBreak
\markup {"You Never Can Tell"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart   
}

