FROM ubuntu:jammy

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get upgrade -yq
RUN apt-get install lilypond tree texlive-latex-base software-properties-common -yq

RUN mkdir /workdir
WORKDIR /workdir

RUN mkdir public

RUN apt-get install -yq git cmake g++
RUN apt-get install -yq libasound2-dev portaudio19-dev libmp3lame-dev libsndfile1-dev libportmidi-dev
RUN apt-get install -yq libasound2-dev portaudio19-dev libmp3lame-dev libsndfile1-dev libportmidi-dev
RUN apt-get install -yq libdrm-dev libgl1-mesa-dev libegl1-mesa-dev
RUN  apt-get install -yq qtbase5-dev qttools5-dev qttools5-dev-tools qtwebengine5-dev \
qtscript5-dev libqt5xmlpatterns5-dev libqt5svg5-dev libqt5webkit5-dev \
qtbase5-private-dev libqt5x11extras5-dev
RUN  apt-get install -yq qml-module-qtquick-controls2 qml-module-qtquick-window2 qml-module-qtquick2 qml-module-qtgraphicaleffects qml-module-qtqml-models2 qtquickcontrols2-5-dev libqt5x11extras5 libqt5x11extras5-dev libqt5networkauth5 libqt5networkauth5-dev qtbase5-private-dev qtdeclarative5-dev qtquickcontrols2-5-dev
RUN git clone --depth 1 https://github.com/musescore/MuseScore.git
WORKDIR /workdir/MuseScore
RUN cmake -P build.cmake -DCMAKE_BUILD_TYPE=Release
RUN cmake --install builds/Linux-Qt-usr-Make-Release/ --prefix "/usr/"

RUN apt-get install -yq wget

RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/openssl_1.1.1f-1ubuntu2.16_amd64.deb
RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/libssl-dev_1.1.1f-1ubuntu2.16_amd64.deb
RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.16_amd64.deb
RUN dpkg -i libssl1.1_1.1.1f-1ubuntu2.16_amd64.deb
RUN dpkg -i libssl-dev_1.1.1f-1ubuntu2.16_amd64.deb
RUN dpkg -i openssl_1.1.1f-1ubuntu2.16_amd64.deb

WORKDIR /workdir
