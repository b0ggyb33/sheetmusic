
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Accidentally in Love"
  subtitle = "V1"
}
#(set-global-staff-size 23)

Intro = 
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
  {<< 
     \new Voice = "first"
     {\voiceOne \relative c''{ fis1 }}
     \new Voice = "second"
     {\voiceTwo \relative c''{  b1  }} 
  >>}  
}

VerseOdd =
{
  \bar "||"
  \relative c^"Verse"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*3}
}

VerseEven =
{
  \bar "||"
  \relative c^"Verse"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}

FragA =
{<<
    \new Voice = "first"
    {\voiceOne \relative c'' {dis8 dis r e e r r4 r1  }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {b8 b r cis cis r r4 r1 }}
>>}  

FragB =
{<<
      \new Voice = "first"
    {\voiceOne \relative c'' {fis8 fis r gis gis r r4 r1 }}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {dis8 dis r e e r r4 r1}}
>>}


ChorusOneThree =
{
  \relative c^"Full Chorus"
  \repeat volta 2
  \FragA
  \alternative
  {
    \FragB
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {dis2~dis8 (e fis) e~e2~e8 fis8 (e) dis~dis2 r2 r1 }}
      \new Voice = "second"
      {\voiceTwo \relative c'' {b2~b8 (cis dis) cis~cis2~cis8 dis8 (cis) b~b2 r2 r1 }}
    >>}
  }
}

Bridge =
{
  \bar "||"
  \relative c^"Bridge"
  \set Score.skipBars = ##t   {R1*4}
}

ChorusTwo =
{
  \relative c^"Short Chorus"
  \repeat volta 2
  \FragA
  \alternative
  {
    \FragB
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {dis2~dis8 (e fis) e~e2 r2 }}
      \new Voice = "second"
      {\voiceTwo \relative c'' {b2~b8 (cis dis) cis~cis2 r2 }}
    >>}
  }
}

ChorusEnd =
{
  \relative c^"End Chorus"
  \repeat volta 2
  \FragA
  \alternative
  {
    \FragB
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {fis2 e dis r fis1  }}
      \new Voice = "second"
      {\voiceTwo \relative c'' {dis2 cis b^"rall" r b1  }}
    >>}
  }
}

OutroOne =
{
  \repeat volta 2
  {<<
     \new Voice = "first"
     {\voiceOne  \relative c''{dis1 r r cis } }
     \new Voice = "second"
     {\voiceTwo  \relative c'' {b1\>  r \! r  ais2~\< ais \!} }
  >>} 
}

OutroTwo =
{
  \repeat volta 2
  {<<
     \new Voice = "first"
     {\voiceOne  \relative c''{dis1 e dis cis } }
     \new Voice = "second"
     {\voiceTwo  \relative c'' {b1\>  b \! \< b  \! \>ais2~\!\< ais \!} }
  >>} 
}

OutroThree =
{
  \repeat volta 2
  {<<
     \new Voice = "first"
     {\voiceOne  \relative c''{dis4. dis8~dis2  e4. e8~e2  dis4. dis8~dis2 cis4. cis8~cis2} }
     \new Voice = "second"
     {\voiceTwo  \relative c''{b4. b8~b2  b4. b8~b2  b4. b8~b2 ais4. ais8~ais2}}
  >>}
}

Bbsaxpart =
{
  \tempo 4 = 140
  \key b \major
 
  \Intro
  \VerseOdd  
  \VerseEven
  \ChorusOneThree

  \VerseOdd  
  \VerseEven
  \Bridge  
  \ChorusTwo
  \ChorusOneThree
  \OutroOne
  \OutroTwo
  \OutroThree
  \ChorusEnd
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Accidentally in Love"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

