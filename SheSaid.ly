
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "She Said"
  subtitle = "V1.1"
}
#(set-global-staff-size 28)

Intro = 
{
  %\set Score.skipBars = ##t   {R1*4}  
  {
   \bar "||"r1^\segno_"add bass" r r  
   <<      \new Voice = "first"
     {\voiceOne \relative c'''{c4-^ bes-^ aes-^ \tuplet 3/2 {d,8 dis e}} }
           \new Voice = "second"
     {\voiceTwo \relative c'{r2 r4 \tuplet 3/2 {d8 dis e}} } 
   >>
  }  
}

Chorus =
{
  \repeat volta 2
  {
    <<
    \new Voice = "first"
    {\voiceOne \relative c' {f1^"Chorus" aes c f e ( e2. d4 c1~)}}
    
    \new Voice = "second"
    {\voiceTwo \relative c' {f1 aes c c c~c~c~}}
  >>  
  }
  \alternative
  {
    {<<
      \new Voice = "first"
      {\voiceOne \relative c' {c'}}  
      \new Voice = "second"
      {\voiceTwo \relative c' {c'}}
    >>}
    {<<
      \new Voice = "first"
      {\voiceOne \relative c' {c'2 aes4(bes)} } 
      \new Voice = "second"
      {\voiceTwo \relative c' {c'2 f,4 (g)} }
    >>}
  }
  
  \repeat volta 2
  {<<
    \new Voice = "first"
    {\voiceOne \relative c'' {c1~c1 }}    
    \new Voice = "second"
    {\voiceTwo \relative c'' {aes1~aes}}
  >>}
  \alternative
  {
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {c1~c2 aes4(bes)}}    
      \new Voice = "second"
      {\voiceTwo \relative c'' { g1~g2 f4 (g)}}
    >>}
    {<<
      \new Voice = "first"
      {\voiceOne \relative c'' {e1~e}}    
      \new Voice = "second"
      {\voiceTwo \relative c'' { g2 aes4 (bes) c1}}
    >>}
  }
}

VerseChorus =
{
  
  {\repeat volta 2
   << \new Voice = "first"{\voiceOne  \relative c''{
    {f4^"Verse" r8 f4 r8 r4 }
    {f4 r8 f4 r8 r4 }    
    {f4 r8 f4 r8 r4 }
    {f f f f}
    {c4 r8 c4 r8 r4 }
    {c4 r8 c4 r8 r4 }
    {c4 r8 c4 r8 r4 }
    {c4 r8 c4 r8 {\tuplet 3/2 {d8 dis e^\coda}}} \bar ":|."
      }}
     \new Voice = "second"{\voiceTwo  \relative c' {
    {f4 r8 f4 r8 r4 }
    {f4 r8 f4 r8 r4 }    
    {f4 r8 f4 r8 r4 }
    {f f f f}
    {c4 r8 c4 r8 r4 }
    {c4 r8 c4 r8 r4 }
    {c4 r8 c4 r8 r4 }
    {c4 r8 c4 r8 {\tuplet 3/2 {d8 dis e}}} \bar ":|."
     }}
   >>
  }
  \relative c'
  {<<
    \new Voice = "first"{\voiceOne  \relative c''{
    {f4 r^"V1 only: 4 bars gtr & dms"  r2 r1 r1 r1}} }
    \new Voice = "second"{\voiceTwo  \relative c' {
    {f4 r r2 r1 r1 r1 }  }}
  >>} 
  \Chorus
}

Rap =
{
  \relative c^"Rap"
  \Chorus
  <<
    \new Voice = "first"
    {\voiceOne \relative c'' {e8^"After rap" e e e e e e e e4 r4^"al Coda"^"D.S." r2}}
    
    \new Voice = "second"
    {\voiceTwo \relative c'' {c8 c c c c c c c c4 r4 r2}}
  >>
}

Outro =
{
   \bar ".." 
   {<<
     \new Voice = "first"{\voiceOne  \relative c''{
     {f1^\coda r }} }
     \new Voice = "second"{\voiceTwo  \relative c' {
     {f1\>  r \!}  }}
   >>} 
   {r1 r1 \bar "||"r1 r1 r^"Ral" r ^"drums"}
   <<
     \new Voice = "first"
     {\voiceOne \relative c'' {f1  \tuplet 3/2 {f4 ees c} bes4 aes f1\fermata}}
     \new Voice = "second"
     {\voiceTwo \relative c' {f1~\< | f2 ees c1 \! } }
   >>
}

Bbsaxpart =
{
  \tempo 4 = 140
  \key f \minor
 
  \Intro
  \repeat volta 2
  {
    \VerseChorus
  }
  \Rap
  \Outro
}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"She Said"}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart
    %\midi{}
}

