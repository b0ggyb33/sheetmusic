
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.
\header{
  title = "Chicken Payback"
  subtitle = "V1"
}

Run =
{ \relative c''
  {ais8^"hook" ais r dis dis r b b r e e r cis r fis4~fis1~fis2. r8 f8 \bar "||"}
}

FourBars =
{ \relative c''
  {fis4 r r r r1 r1 r2. r8 f \bar "||"}
}

Verse =
{ \relative c''
  {
    fis4^"Verse: V1, V2, hook, V3=gtr solo, V4, V5" r r r r1 r1 r2. r8 f \bar "||"
    fis4 r r r r1 r1 r2. r8 bes \bar "||"
    b4 r r r r1 r1 r2 b8 ais a gis \bar "||"
    fis4 r r r r1 r1 r2.^"back to hook after V2" r8 f \bar "||"
  } 
}


AltVerse =
{ \relative c''
  {
    fis4^"Alternative Verse" r r r r1 r1 r2. r8 f \bar "||"
    fis4 r r r r1 r1 r2. r8 bes \bar "||"
    b2. r8 bes b1 r1 \grace bes8 b4 \grace bes8 b4 \grace bes8 b4 \grace bes8 b8 a \bar "||"
    fis4 r cis8 cis b bes~bes2 r r2 cis8 cis b bes~bes2 r4 r8 f' \bar "||"
  } 
}



Bbsaxpart =
{
  \tempo 4 = 140
  \key fis \major 
  \bar "[|:"\Run
  \bar ".|:"\FourBars   \bar ":..:"\Verse \bar ":|]"
  \FourBars \FourBars \Run
}
  

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \AltVerse  
}


%\pageBreak
%\markup {"Chicken Payback"}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}
}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \AltVerse
    %\midi {}
}

