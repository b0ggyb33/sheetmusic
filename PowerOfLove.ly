
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "The Power of Love"
  opus = "14 August 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
#(set-global-staff-size 21)

Pickup = 
{<< 
    {
      \new Voice = "first" {
      \voiceOne 
      \relative c''' 
      {
        r4 r r b 
    }    
    }}
    {
      \new Voice= "second" {
      \voiceTwo{
      \relative c''
      {
        r4 r r b
      }}
    }}
  >>
}


IntroAa = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''' 
      {
        d8 r r4 r fis, g8 r r4 r b
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        d8 r r4 r a g8 r r4 r b
      }
    }}
    {
      \chords {d2.  d4 g2.  g4 } 
    }
  >>
}

%IntroAbc = 
%{<< 
{
      \new Voice = "first"{
      \voiceOne 
      \relative c''' 
      {
        d8 r r4 r a g8 r r4 r b
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'''
      {
        a8 r r4 r fis d8 r r4 r g
      }
    }}
    {\chords {d2.  d4 g2.  g4 } 
    }  >>
%}



IntroB = 
{<< 
    {
      \new Voice = "first" {
      \voiceOne 
      \relative c'' 
      {
        f8 r r f r4 r8 g~g r r4 r r8 g
      }    
    }}
    {
      \new Voice= "second" {
      \voiceTwo
      \relative c''
      {
        d8 r r d r4 r8 d~d r r4 r r8 d        
      }
    }}
    {\chords {d1:m7 g} 
    }
  >>
}

IntroBtweak = 
{<< 
    {
      \new Voice = "first" {
      \voiceOne 
      \relative c'' 
      {
        f8 r r f r4 r8 g~g r r4 r r
      }    
    }}
    {
      \new Voice= "second" {
      \voiceTwo
      \relative c''
      {
        d8 r r d r4 r8 d~d r r4 r8 g,8 b8 c        
      }
    }}
    {\chords {d1:m7 g} 
    }
  >>
}

IntroC = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
        f8 r r f r4 r8 g~g r r4 c8 r b4
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        d8 r r d r4 r8 d~d r r4 g8 r g4        
      }
    }}
    {\chords {d1:m7 g2 c4 g4} 
    }
  >>
}

VerseOne =
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}

VerseTwoThre =
{
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*3}
  \Pickup
}

Bridge = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
        c1 cis d d d2. d4 c1 d2. d4 c1 d2 d d1
      } }   
    }
    {
      \new Voice= "second" {
      \voiceTwo
      \relative c''
      {
        a1 a f g bes2. bes4 a2 f bes2. bes4 a1 bes2\< bes2 a1\!      
      } }
    }
    {\chords {f1 a d:m7 g:7 bes f bes f bes a:sus4 r} 
    }
  >>
}

ChorusTag = 
{<< 
    {
      \new Voice = "first"
      \voiceOne 
      \relative c'' 
      {
        c2 d d1^"to Bridge 2nd"
      }    
    }
    {
      \new Voice= "second"
      \voiceTwo
      \relative c''
      {
        g2 g a1   
      }
    }
    {\chords {c2 g2 a1:sus4} 
    }
  >>
}

Bbsaxpart =
{
  \tempo 4 = 120
  \key c \major


  \Pickup  \relative c_"Intro x3"  \repeat volta3{\IntroAa} 
  \IntroB \IntroC
  \bar "||"\relative c_"Verse 1"  \VerseOne \bar "||" \IntroC  
  \break 
  
  \repeat volta 2
  {
    \relative c_"Verse 2 & 3" \VerseTwoThre 
    \relative c^"Chorus x3" \repeat volta2 {\IntroAa}    
    \ChorusTag 
  }
  \alternative
  {
    {\bar "||" r1  \IntroB \IntroC }
    {\break \relative c_"Bridge" \Bridge \bar "||"}    
  }
  
  \set Score.skipBars = ##t   {R1*2}
  \set Score.skipBars = ##t   {R1*2}
  \set Score.skipBars = ##t   {R1*2} \break
  \relative c_"Gtr Solo 16 bars - built pp to f"
  \relative c^"rptx3" \repeat volta3 { \IntroB \IntroBtweak }  \break
  \relative c_"last 4 bars of Gtr Solo" \IntroB \IntroC
  \relative c_"Outro - repeat until ..."\repeat volt 2 {\IntroAa}

}

\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"}  
    \transpose d a, \Bbsaxpart 
    
}

\pageBreak
\markup {"The Power of Love"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

