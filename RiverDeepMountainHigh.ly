
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "River Deep Mountain High"
  opus = "8 May 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

#(set-global-staff-size 24)

IntroRiff =
{
  \relative c'''
  { 
    r4\segno r4 r4 r8 g \bar "||"
    \time 3/4 c8 c bes c~c bes
    \time 4/4 g8 g f g~g4 f
    c8 c bes c~c4 bes
    c4 r r2 r1
  }
}

Verse = 
{
  <<
    \relative c''
    { 
      \repeat volta2 {r1^"VERSE"  r r r r1 r r r^"x2"}
      \repeat volta3 {r1 r r r^"x3"} 
    }
    \chords 
    {
      f1*4\bar "||"       
      c1*4 
      g1*2 c1*2  
    }
  >>
}

Chorus = 
{
  <<
    \relative c''
    { 
      \repeat volta2 {r1^"CHORUS" r r4 bes a bes c bes a bes} 
      \alternative
      {
        {r1 r r4 c b c d c b c}
        {r1 r r^"After C1, C3: Back to sign"_"After C2: Cont to Bridge" r}
      }
    }
    \chords 
    { 
       bes1*4 c1*4 c1*4
    }
  >>
}

Connector =
{
  r4^"Pause before BRIDGE" r r r \time 2/4 \relative c''{r8 g bes b} \time 4/4
}

RiffA =
{
  \relative c'' {c8 r c4 r8 g bes b c bes c g r g bes b}
}

RiffAA =
{
  \relative c'' {c8 r c4 r8 g bes b c bes c g r g bes b}
}

RiffB =
{
  \relative c'' 
  \repeat volta2 {c8 r c4 r8 g bes b }
  \alternative
  {
    {c bes c g r g bes b}
    {c bes c g r c ees e}
  }
}

RiffC =
{
  \relative c'' 
  \repeat volta2 {f8 r f4 r8 c ees e }
  \alternative
  {
    {f ees f c r c ees e}
    {f ees f c r g bes b}
  }
}

DeepMountain =
{
  \relative c'' { c8^"Deep! Mountain high" r r4 r r r4 r r8 g bes b }
}



Bbsaxpart =
{
  \tempo 4 = 170
  \key c \major
  \time 4/4

  \IntroRiff \break
  \Verse \break
  \Chorus \break
  
  \Connector
  \repeat volta2 {\relative c^"No vocal x2"\RiffA} \break
  \repeat volta2 {\relative c^"I love you baby ... ... ... ... flower loves the spring" {\RiffAA} }\break
  \relative c^"I love you baby ... ... ... ... robin loves the spring" \RiffB \break 
  \relative c^"I love you baby ... ... ... ... school boy loves his pet" \RiffC \break
  \relative c^"I love you baby ... ... " \RiffAA \break \DeepMountain
  \repeat volta6 {\relative c^"12 bars Gtr Solo: repeat x6 - to Chorus"\RiffA}
}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}

    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"River Deep Mountain High"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart
    %\midi{}
}

