
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Mamma Mia"
  opus = "11 April 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
\layout{ragged-last =##t}

%#(set-global-staff-size 25)

Intro =
{ 
  \relative c^"Intro: plinky piano"
  { \compressFullBarRests
    R1*4
    \relative c_"with gtr" 
    R1*4
  }
}




Verse = 
{
  <<
    \relative c''^"Verse"
    { 
       
      \bar "[|:"
        \relative c''
        {r1 r1 r4 e8 e fis (e) e e~e r a a gis (e) e e} 
      \bar ":|."
      %\compressFullBarRests
      \relative c''
      {
        R1*4 R1*2 fis4\p fis fis8 dis cis4\bar "||" b^"pre-chorus" r a'\f gis
        fis4 r r r  r r a gis fis r r r r1 
      }

    }
    \chords 
    {
      \compressFullBarRests
    }
  >>
}


Chorus = 
{
  <<
    \relative c'''
    { 
       r1^"Chorus" r1 r1\pp e~\coda\<
       e~ e~ e~ e
       e,1\ff\! dis cis b
       a4-. r2. r1 
       e'1 cis cis2. cis4 b2. cis4 \bar "||"e1^"back to gtr riff & V2 until Coda"~\>e1\ppp\! r1 r1 \bar ":|]"
    }
    \chords 
    { 
     
    }
  >>
}

Coda =
{
  <<
    \relative c'''
    {  
       \repeat volta2{b~\<\coda ^"Coda"_"Build each time" b a~ a\!^"x2"}
       \repeat volta2
       {
         <<
           {e'~_"Build each time" e~e~ e^"x2"}
           {b~\< b a~ a\!^"x2"}
         
         >>
       }
       e1 dis cis b
       a4-. r2. r1
       e'1 cis cis2. cis4 b2. cis4 \bar ":|]" e1~\>e1\pp\!
    }
    \chords 
    { 
     
    }
  >>
}



Bbsaxpart =
{
  \tempo 4 = 136
  \key e \major
  \time 4/4
  
  \Intro 
  
  \Verse \break
    
  \Chorus \break
  
  \Coda


}


\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
%\pageBreak
%\markup {"Mamma Mia"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

