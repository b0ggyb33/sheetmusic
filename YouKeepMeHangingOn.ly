
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Now That I've Found You"
  %subtitle = "V1"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

#(set-global-staff-size 27)

Intro =
{
  \relative c''
  { 
    r1^"Intro" r1 r1 r1 
  }
}

ChorusOneThree =
{
  \repeat volta2
  {
    <<
      \relative c''
      {
        e4^"Chorus 1&3 - play 3x" r r2 e4 r r2 d4 r r2 c4 r c r
      }
      \relative c''
      {
        a4 r r2 g4 r r2 g4 r r2 f4 r f r
      }
      \chords {a1:m e:m g f}
    >>
  }
}



XBreak =
{
  \relative c''
  { 
    r1^"2 Bar Break" r4 e-- d-. r  
  }
}



VerseOneThree = 
{
    <<
      \relative c''
      {
        f1^"Verse 1 & 3" r f e f r f e2 e d1 r d^"Outro after V3" r
      }
      \relative c''
      {
        c1 r c c c r \break c c2 b b1 r gis r
      }
      \chords {bes1:/c bes1:/c f1 c  bes1:/c bes1:/c f1 c2 e2:m g1 g e:7 e:7 }
    >>
}

ChorusTwo =
{
    <<
      \relative c''
      \new Voice = "first"
      { \voiceOne
        e4^"Chorus 2" r r2 e4 r r2 d4 r r2 c4 r e8 c d4
        a2 r2 e'4 r r2 d4 r r2 c4 r e8 c d4
        a2 r2 e'4 r r2 d4 r r2 c4 r c r
      }
      \relative c''
      \new Voice = "second"
      { \voiceTwo
        a4 r r2 g4 r r2 g4 r r2 f4 r e'8 c d4
        a2 r2 g4 r r2 g4 r r2 f4 r e'8 c d4
        a2 r2 g4 r r2 g4 r r2 f4 r f r
      }
      \chords {a1:m e:m g f a1:m e:m g f a1:m e:m g f}
    >>

}



VerseTwo = 
{
    <<
      \relative c''
      {
        f1^"Verse 2" r f e f r f e r r
      }
      \relative c''
      {
        c1 r c c c r c a r r
      }
      \chords {bes1:/c bes1:/c f1 c  bes1:/c bes1:/c f1 a:m  }
    >>
}


BridgeOutroChorus =
{
    <<
      \relative c''
      \new Voice = "first"
      { \voiceOne
        e4^"Bridge & Outro" r r2 e4 r r2 d4 r r2 \bar ".|:" c4^"No repeat for bridge" r e8 c d4
        a2 r2 e'4 r r2 d4^"Verse 3 after bridge" r r2 \bar ":|." c4 r c r
      }
      \relative c''
      \new Voice = "second"
      { \voiceTwo
        a4 r r2 g4 r r2 g4 r r2 f4 r e'8 c d4
        a2 r2 g4 r r2 g4 r r2 f4 r f r
      }
      \chords {a1:m e:m g f a1:m e:m g f}
    >>

}



Bbsaxpart =
{
  \tempo 4 = 126
  \key a \minor
  \time 4/4
  
  \Intro  \break
  %\repeat volta2
  {
    \ChorusOneThree \break
    \VerseOneThree \break
    \ChorusTwo \break
    \VerseTwo  \break
  }
  \BridgeOutroChorus

}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"You Keep Me Hanging On"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

