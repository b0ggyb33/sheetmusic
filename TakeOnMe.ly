\version "2.18.2"
\header{
	title = "Take On Me"
}

intro = {
	\relative c '''
	{
		gis8 gis e   cis r   cis r fis
		r8   fis8 r   fis ais ais b cis
		b    b   b   fis r   dis r   gis
	  r    gis r   gis fis fis gis fis
	}
}

chorus = {
	\relative c ''
	{
		r1 r r fis4 r8 gis4 gis8 fis4
	}
}

tenorpart = {
    \time 4/4
    \clef treble
    \key b \major
    \intro
		\break
		\chorus
}
\score
{
    \new Staff \with { instrumentName = #"Alto Sax"}
    \relative c '''
		{
			 \transpose f c \tenorpart
		}
}
\score
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \tenorpart

}
