
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "December 63 (Oh What a Night)"
  opus = "1 June 19"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
\layout{ragged-last =##t}

%#(set-global-staff-size 25)

Riff =
{
  \relative c''{  {ees8. f16~f8  g8~} g16 r aes8 aes8 r8 aes,8. aes16~aes8 g~g f ees f}
}
AlternateRiff =
{
  \relative c''{ {ees8.^"alto" f16~f8  g8~} g16 r aes8 aes8 r8 aes,8.^"tenor" aes16~aes8 g~g f ees f}
}

Intro =
{ 
  \relative c^"Intro: drums and piano"
  { \compressFullBarRests
    R1*2
    \Riff
  }
}

Verse = 
{
  <<
    \relative c''^"Verse"
    { 
      R1*4 R1*2 \Riff

    }
    \chords 
    {
      \compressFullBarRests
    }
  >>
}

Chorus = 
{
  <<
    \relative c'''
    { 
      r1^"Chorus" r g2. g4 f2. d4  
     c1\> r\! g'4 g8 f8~f4 r f4 f8 d~ d4 c8 bes 

    }
    \chords 
    {
      f1:m7 aes c:m7 bes
      f1:m7 aes c:m7 bes2 bes:6
    }
  >>
}

ShortFIll = 
{
  \repeat volta2
  {<<
    
    { \relative c''
      {
        \tuplet 3/2 {c4^"Short Fill"  c r  c4 c r  es es f~} f4 \tuplet 3/2 {f8 (es c)}
      }

    }
    \chords 
    {
      c1:m ees2 f
    }
  >>}
}


Bridge = 
{
  <<
    \relative c'
    { 
       r4^"Bridge"r r8 f (aes c) f4 (d bes )bes' (g2.) ees8 (c  bes1)
    }
    \chords 
    { 
      f1:m g:m7 aes:7 bes:7
    }
  >>
}

Solo = 
{
  <<
    \relative c'^"Solo"
    { 
      R1*2 R1*2 R1*2 R1*2 \bar ":|]"
    }
    \chords 
    { 
      ees1 aes2 bes
    }
  >>
}



Outro =
{
  \repeat volta2
  {<< 
    \relative c^"Outro: repeat until ..."
    {  
      \AlternateRiff
    }
    \chords 
    { 
      
    }
  >>}
}



Bbsaxpart =
{
  \tempo 4 = 105
  \key ees \major
  \time 4/4

  
  \Intro \break
  \Verse \break
  \Verse \break
  
  \repeat volta2
  {\bar "[|:"
    \Chorus \break
    \Verse \break
    \ShortFIll
    \Bridge
  }
  
  \alternative
  {
    {\Solo \bar ":|]"\break }
    {\Outro}
  }
 
}


\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"December 63 (Oh What a Night)"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart 
}

