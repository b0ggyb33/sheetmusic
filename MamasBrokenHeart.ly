
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Mama's Broken Heart"
  opus = "13 November 19"
}

\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
\layout{ragged-last =##t}

#(set-global-staff-size 27)


Intro = 
{ 
    \relative c'''^"intro"

    { \compressFullBarRests R1*4 }
}

BlankVerse = 
{<< 
    \relative c''
    \new Voice = "first"
    { \voiceOne  r1^"Verse 1 & 3" r r r \bar "||"  r r r r \bar "||"
    }
    \relative c''
    \new Voice= "second"
    { \voiceTwo 
    }
  >>
}

BittyVerse = 
{<< 
    \relative c'''
    \new Voice = "first"
    { \voiceOne  r1^"Verse 2 & 4" r2 r8 a8_"quaver lead-in on V4 only" cis4( b2) r2 r r8 gis8 b4( a2) r  r2 r4_"V4 only" a4\>( b4 a8 gis~ gis4) gis a\! r r2
      \bar "||" 
    }
    \relative c'''
    \new Voice= "second"
    { \voiceTwo r1\p r2 r8 fis, a4( gis2) r2 r r8 f8 gis4( fis2) r r2 r4 fis4( gis4 fis8 f~ f4) f4 fis r4 r2

      \bar "||" 
    }
  >>
}
  

Chorus = 
{<< 
  \relative c'''
  \new Voice = "first"
  { \voiceOne a1^"Chorus"~a b~b cis~cis dis~dis cis4 b gis f
    \repeat volta 2 { fis4. \grace gis16( a8) cis a cis a^"x2"}  fis2 r\bar "||"
  }
  \relative c''
  \new Voice= "second"
  { \voiceTwo fis1\ff~fis gis~gis a~a b f,4 gis8 b~b2 cis'4 b gis f
    \repeat volta 2 { fis4. \grace gis16( a8) cis a cis a}   fis2^"back to V3 or I'lude" r
  }
 >>
}


Outro = 
{<< 
  \relative c''
  \new Voice = "first"
  { \voiceOne fis1^"outro Chorus" a gis b a cis b~b cis4 b gis f
    \repeat volta 2 { fis4. \grace gis16( a8) cis a cis a^"x3"}  fis4. \grace gis16( a8) cis2 \bar "||"
  }
  \relative c'
  \new Voice= "second"
  { \voiceTwo fis1 a gis b a cis b f4 gis8 b~b2 cis'4 b gis f
    \repeat volta 2 { fis4. \grace gis16( a8) cis a cis a}   fis4. \grace gis16( a8) cis2
  }
 >>
}


Interlude =
{
  \relative c'
  {
    r1^"Interlude" r r r r^"& pause ..." \bar "||"
    
  } 
}

Bbsaxpart =
{
  \tempo 4 = 110
  \key fis \minor  


  \Intro \bar "[|:"
  \BlankVerse \break
  \BittyVerse \break
  \Chorus \bar ":|]"
  \Interlude \break
  \Outro
   
}

\score 
  {
    \new Staff \with { instrumentName = #"ALTO"}  
    \transpose d a, \Bbsaxpart 
    %\midi{}
}

\pageBreak
\markup {"Mama's Broken Heart"   }

\score 
  {
    
    \new Staff \with { instrumentName = #"TENOR"}
    \Bbsaxpart   

}

