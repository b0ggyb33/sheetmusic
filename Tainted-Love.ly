\version "2.18.2"
\header{
	title = "Tainted Love"
}
#(set-global-staff-size 22.5)

%Cminor concert, Aminor in Eb
%which is same signature as C with G#

%F# (A D A F#)
%F# F# A A D D A B


intro = {\relative c ''{a'1^drums}}

saxFill = {\relative c '' {a8-. a8-. r4 r4 r4}}


verseOne = 
{%0:08
  \relative c ''
  {
     \bar "||"
     r1^"verse"  \saxFill \saxFill r1
     r1 \saxFill \saxFill r1
  }
}


bridgeOne = 
{%0:33
  <<
    {\relative c ''{ e1 g a a  d2~d }}
    {\relative c ''{ \bar "||" cis1\<\p^"bridge" e f fis  g2 fis4 f4\! }}
  >>	
}

chorusOne = {
	\relative c ''{
  \bar "||"
  r8\ff^"chorus" a8-. r4 r2  r8 a8-. r4
  r2
}
}

bridgeTwo = 
{%1:18
  <<
    {\relative c ''{ e1 g a a  d1 }}
    {\relative c ''{ \bar "||" cis1\<\p^"bridge" e f fis g\! }}
  >>	
}

chorusTwo = {
  \relative c '''
	{
		\bar "||"
  	a8\ff-.^"chorus" a8-. r4 r2
  	a8-. a8-. r4 r2
	}
}

bridgeThing = {%1:38
  \bar "||"
	\relative c ''{
	r1^"dont touch me"  r1
  r2 r4 fis,16 a8. a8 a a a c8 c b b
}
}

outro = 
{%1:50
  \relative c ''
  { \bar "||"
    a8-.^"outro"  a8-. r4 r2
    a8 a r4 r8 a (c b
    \repeat volta 2 { a8)^"till fade" a r4 r8 a (c b)  }
  }
}

altopart = 
\transpose c a,
{
    \tempo 4 = 80
    \time 4/4
    \clef treble
    \key a \minor
    \intro r1 r
    \verseOne
    \bridgeOne
    \chorusOne
    \verseOne
    \bridgeTwo
    \chorusTwo
    \bridgeThing
    \relative c''^"Extra" {} \bridgeTwo
    \outro
}

\score
{
    \new Staff \with { instrumentName = #"ALTO"}
    \relative c'' {
        \altopart
	}
    %\midi{}	
}

\score
{
    \new Staff \with { instrumentName = #"TENOR"}
    \transpose c f, \altopart

}
