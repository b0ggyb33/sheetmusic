
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "All About That Bass"
  subtitle = "V1"
}





Bbsaxpart =
{
  \tempo 4 = 130
  \key b \major
  
  <<\chords
  {
    b1*2 cis:m fis b
  }  
  {r1^"Intro = Chorus" r1 r1 r1 r1 r1 r1 r1 }  
  >>
  
  <<\chords
  {
    b1*2 cis:m fis b
    b1*2 cis:m fis b2 e2 b1
  }  
  { \bar "||"r1^"Verse" r1 r1 r1 r1 r1 r1 r1
    \bar "||"r1 r1 r1 r1 r1 r1 r1 r1 }  
  >>
 

  <<\chords
  {
    b1*2 cis:m fis b
    b1*2 cis:m fis1 e1 b1*2 
  } 
  \relative  c''
  { \bar "||"r1^"Pre-chorus: ad lib similar" 
    r1 r4 cis8 e fis gis fis e  cis 4 r8 b gis2 fis e dis2. cis4 b1 r
    \relative c''
    {
    \bar "||"r1 r1 r4 cis8 e fis gis fis e  cis 4 r8 b gis2 fis e dis2. cis4 b1 r} }  
  >>
 
  <<\chords
  {
    b1*2 cis:m fis b
  }  
  {\bar "||" r1^"Outro = Chorus: 1 tacit, 2 bass grunts + adlibs" r1 r1 r1 r1 r1 r1 r1 }  
  >>
 


}

\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a, \Bbsaxpart  
}

\markup {"   "}

\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart
    %\midi{}
}

