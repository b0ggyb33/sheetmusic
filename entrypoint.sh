#!/bin/sh
ls | grep ".ly$" | xargs lilypond ;
for i in *.mscx
do
	QT_QPA_PLATFORM="offscreen" mscore $i -P -o $(echo $i | sed -E "s/mscx/pdf/")
done
find *.lytex | xargs -I{} ./compile_lytex.sh {}
find *.setlist | xargs -I{} ./compile_setlist_from_file.sh {}
mkdir -p public | true
mv *.pdf public/
cd public
mkdir -p notes
mv notes*.pdf notes/
../makeindex.sh
