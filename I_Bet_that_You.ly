\version "2.24.0"

\score {
  <<
    \new Staff = "Guitar" \with {
      instrumentName = "Guitar"
    } {
      \clef treble
      \time 4/4
      \tempo 4 = 180
      c'4 c' g'4 e'4 |
      f'4 e'4 d'4 c'4 |
      g'4 a'4 b'4 c''4 |
      g'8 f'8 e'8 d'8 c'4 |
    }

    \new Staff = "Bass" \with {
      instrumentName = "Bass"
    } {
      \clef bass
      g,4 c4 e,4 g,4 |
      a,4 f,4 g,4 e,4 |
      d,4 g,4 a,4 c4 |
      f,4 e,4 d,4 c4 |
    }

    \new Staff = "Trumpet" \with {
      instrumentName = "Trumpet"
    } {
      \clef treble
      r4 g'4 f'4 e'4 |
      c'4 g'4 a'4 c''4 |
      g'4 b'4 c''4 |
      c'4 a'4 b'4 c''4 |
    }

    \new Staff = "Tenor Saxophone" \with {
      instrumentName = "Tenor Saxophone"
    } {
      \clef treble
      r4 f'4 d'4 c'4 |
      g'4 c'4 g'4 e'4 |
      c'4 a'4 c'4 |
      g'4 a'4 c'4 |
    }

    \new DrumStaff = "Drums" \with {
      instrumentName = "Drum Kit"
    } {
      \clef percussion
      r4 g,4 d,4 g,4 |
      g,,4 g,4 d,4 g,4 |
      g,,4 g,4 d,4 g,4 |
      g,4 g,,4 d,,4 |
    }
  >>
}
