
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Dear Future Husband"
  subtitle = "V1"
}

intro =
{
  <<
  \relative c { \repeat percent 4 {r1} \bar "||" \repeat percent 8 {r} }
  \chords {\repeat percent 4 {g:1} 
           \repeat percent 4 {g:1} 
           \repeat percent 4 {c:1} }
  >>
}

verse = 
{
  <<
  {r1^"Verse" r r r\bar "||" r r r r\bar "||" r r r r\bar "||" r r r r}
  \chords {\repeat percent 4 {g1} 
           \repeat percent 4 {c}
           \repeat percent 4 {g} 
           \repeat percent 4 {c} }
  >>
}

prechorus =
{
  \repeat volta 2
  {
    << 
    \new Voice = "first"
    {\voiceOne f2^"PreChorus x2" e2 f4 e f e f2 e2 f4 e f e}
    \new Voice= "second" 
    {\voiceTwo f2 e2 f4 e f e f2 e2 f4 e f e}
    \chords {\repeat percent 4 {d1:m} }
    >>
  }
  <<
    \new Voice = "first"
    {\voiceOne d1~d}
    \new Voice= "second" 
    {\voiceTwo d2. b8 a g1}
    \chords {\repeat percent 2 {g1} }
  >>
}

chorus = 
{
  \repeat volta 2
  {
    << 
      \new Voice = "first"
      {\voiceOne c1^"Chorus" f4 e d c e1 e4 d c b \bar "||"
                 c1 c4 b a g a1 e'2 d2}
      \new Voice= "second" 
      {\voiceTwo c1 f4 e d c e1 e4 d c b 
                 c1 c4 b a g a1 b2 b2}
      \chords {c1 c e:m e:m a:m a:m d:m e2:m g2}
    >>
  }
}

bridge =
{
  {r1^"Bridge" r r r r1 r r r}
}

ending =
{
  << 
    \relative c'
    \new Voice = "first"
    {\voiceOne e'2^"x5 at end" d2 c1}
    \relative c''
    \new Voice = "second" 
    {\voiceTwo b2 b2 c1}
  >>

}

Bbsaxpart =
{
  \tempo 4 = 160
  \key ees \major
  
  {r1^"Intro: slow .... Aaah to speed" r1 r1 r1 r1 r1 r1 r1\bar "||"}
  \relative c'
  {ees4 r ees r r1 c4 r c r r1 aes'4 r aes r bes4 r bes r ees r r2 r2 bes8 bes c bes \bar "||"}
  \relative c'
  {ees4 r ees r ees4 r ees d c r c r c r c r 
   aes'4 r aes r bes4 r bes r8 bes ees,4 r r2 r1 }
  {\bar "||" r1^"You got to ..." r r r r r r^" .. alright Aaaah" r}
  \repeat volta 2
  \relative c'
  {ees4 r8 ees ees4 c ees4 r ees d c r c r c r c r 
   aes'4 r aes r bes4 r bes r8 bes ees,4 r ees c  ees r ees c}
  \relative c'
  {ees^"Bridge" r4 r2 r2 r4 ees c r r2 r r4 bes' aes r r aes bes r r2 r1 r2 \tuplet 3/2 {bes,4 c d} \bar "||"}
  \relative c'
  {ees4^"You'got to ..." r ees r ees4 r ees d c r c r c r c r 
   aes'4 r aes r bes4 r bes r8 bes ees,4 r r2 r1 }
  \key e \major
  \transpose ees e
  \relative c'
  {\bar ".|:"ees4 r8 ees ees4 c ees4 r ees d c r c r c r c r 
   aes'4 r aes r bes4 r bes r8 bes ees,4 r ees c  ees r ees c\bar ":|."}
  \relative c'
  {e4 r r2  r2 e'4 (dis cis2) r2 r cis4 (b a2) r2 r \tuplet 3/2 {b4 cis dis} e r r2 r2 \tuplet 3/2 {b,4 cis dis} }
  <<
  \break
  \chords
  {\repeat volta 2 { e1 e cis cis a b e e} {}}
  \relative c'{e1^"3x build up ad lib maj pentatonic, bit of b3 - up an octave maybe" e1 cis cis a' b e, e  }
  >>
    <<
    \chords {a b e e}
    \relative c''
    \new Voice = "first"
    {\voiceOne a4 r2. b4 r2. e,4 r2.}
    \relative c''
    \new Voice = "second" 
    {\voiceTwo a4 r2. b4 r2. e,4 r2.}
    >>

}

%\score 
{
    \new Staff \with { instrumentName = #"Alto Sax"}  
    \transpose d a \Bbsaxpart  
}
\pageBreak
\markup {"Dear Future Husband"}


\score 
{
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart
    %\midi{}
}

