
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Keep on Movin'"
  subtitle = "V0"
}
#(set-global-staff-size 23)

Intro =
{<< 
     \new Voice = "first"
     {\voiceOne \relative c''{ r1^"Intro" r r r r r r }}
     \new Voice = "second"
     {\voiceTwo \relative c'{   }} 
>>}

Verse= 
{   \set Score.skipBars = ##t   {R1*4^"Verse"}
     \set Score.skipBars = ##t   {R1*4}
     \set Score.skipBars = ##t   {R1*3}
<<     \new Voice = "first"
     {\voiceOne \relative c'''{ 
       b4_"flying" a8 gis a gis~gis4 e1 r r 
       b'4_"rocking" a8 gis a gis~gis4 
     }}
     \new Voice = "second"
     {\voiceTwo \relative c'''{ 
       b4 a8 gis a gis~gis4 e1 r r
       b'4 a8 gis a gis~gis4
     }} 
>>}

Chorus = 
{<< 
     \new Voice = "first"
     {\voiceOne \relative c'''{ gis1^"Chorus" a a a  gis1 a a a }}
     \new Voice = "second"
     {\voiceTwo \relative c''{ e1 fis fis e  e1 fis fis e }} 
     {\chords {e1 fis:m7 d:9 a}}
>>}

PostChorusTwo = 
{<< 
     \new Voice = "first"
     {\voiceOne \relative c'''{
       r4^"PostChorus 2"_"FEELING" d2. b2. r4 r8 d, d4 b8 r r4 \tuplet 3/2 {b8 cis d} d8 b d4 e
       r4 d'2. b2. r4 r8 d, d4 b8 r r4 r8 a \tuplet 3/2 {b8 cis d} e8 d d4
     }}
     \new Voice = "second"
     {\voiceTwo \relative c'''{ 
       r4 d2. b2. r4 r8 d, d4 b8 r r4 \tuplet 3/2 {b8 cis d} d8 b d4 e
       r4 d'2. b2. r4 r8 d, d4 b8 r r4 r8 a \tuplet 3/2 {b8 cis d} e8 d d4       
     }} 
 
>>}



Bbsaxpart =
{
  \tempo 4 = 136
  \key e \major
  \Verse \break
  \Chorus \break
  
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

%\pageBreak
\markup {"Keep on Movin'"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

