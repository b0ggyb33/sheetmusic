
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Bohemian Like You"
  subtitle = "V1"
}
#(set-global-staff-size 23)

Intro = 
{
  \repeat volta2
  {
    <<{\chords {cis1 e b fis}}
      {r1^"Intro" r r r}
    >>
  }
}

StrumRiff =
{\repeat volta2{
{<<
    \new Voice = "first"
    {\voiceOne \relative c'' {
      f8 f fis r fis f4 gis8~
      gis gis a r a gis4 dis8~ 
      dis dis e r e dis4 ais'8~
      ais b b4 ais8 b b4    
    }}   
    \new Voice = "second"
    {\voiceTwo \relative c' {
      f8 f fis r fis f4 gis8~
      gis gis a r a gis4 dis8~ 
      dis dis e r e dis4 ais'8~
      ais b b4 ais8 b b4
    }}
>>}} \break}

LowRiff =
{<<
    \new Voice = "first"
    {\voiceOne \relative c''' {cis8 b gis cis,~cis4 cis}}   
    \new Voice = "second"
    {\voiceTwo \relative c'' {cis8 b gis cis,~cis4 cis}}
>>}


Verse =
{
  %\bar "||"
  \relative c^"Verse"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}

Chorus =
{
  %\bar "||"
  \relative c^"Chorus"
  \relative c_"I like .. waho woo!"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}

VerseEven =
{
  \bar "||"
  \relative c^"Verse"
  \set Score.skipBars = ##t   {R1*4}
  \set Score.skipBars = ##t   {R1*4}
}

Bridge =
{
  \repeat volta2
  {
    <<{\chords {cis1 e b fis}}
      \new Voice = "first"
      {\voiceOne \relative c''' {
         gis1^\markup{\with-color #red"Bridge after V3 & riff"} gis fis fis 
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c'' {
        cis1 b b cis
      }}
    >>
  }
}


LastChorus =
{
  \repeat volta2
  {
    <<{\chords {cis1 e b fis}}
      \new Voice = "first"
      {\voiceOne \relative c'''' {
         gis1^"Last Chorus" gis fis fis 
      }}   
      \new Voice = "second"
      {\voiceTwo \relative c''' {
        cis1 b b cis
      }}
    >>
  }
}



OutroThree =
{
  \repeat volta 2
  {<<
     \new Voice = "first"
     {\voiceOne  \relative c''{dis4. dis8~dis2  e4. e8~e2  dis4. dis8~dis2 cis4. cis8~cis2} }
     \new Voice = "second"
     {\voiceTwo  \relative c''{b4. b8~b2  b4. b8~b2  b4. b8~b2 ais4. ais8~ais2}}
  >>}
}

Bbsaxpart =
{
  \tempo 4 = 140
  \key b \major
 
  \Intro \break
  \relative c^\markup{\with-color #red"play 2nd time"}\StrumRiff
  
  \repeat volta 3
  {
    \relative c^\segno
    \LowRiff \LowRiff \LowRiff \relative c^"STOP 3rd time!"\LowRiff \bar "||"
    %\Chorus
    %\StrumRiff
  
  }
  \alternative
  {
    {\set Score.repeatCommands = #'((volta "1") end-repeat)  \Verse }
    {\set Score.repeatCommands = #'((volta "2, 3") end-repeat) \Verse \Chorus \StrumRiff }   
  }
  \Bridge
  \LastChorus
  \StrumRiff
  \LowRiff \LowRiff \LowRiff \LowRiff
}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"ALTO"}}  
    \transpose d a, \Bbsaxpart  
}

\pageBreak
\markup {"Bohemian Like You"}

\score 
{
    \new Staff \with { instrumentName = \markup \bold {"TENOR"}}
    \Bbsaxpart
    %\midi{}
}

