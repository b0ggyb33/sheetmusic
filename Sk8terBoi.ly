
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Sk8ter Boi"
  %subtitle = "V1"
  opus = "11 March 2020"

}
#(set-global-staff-size 22)


ChorusStart = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''
      {
        r4^"Chorus: rpt x2 CH1 - x4 CH2 & End" d8 r8 d2 
        r4 d8 r8 d2 
        r4 c8 r8 c2 
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      {
        r4 g8 r8 g2 
        r4 fis8 r8 fis2 
        r4 e8  r8 e2    
      }
    }}
    {\chords {g1 d c } 
    }
  >>
}

ChorusAltEndOne = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
        b1
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'
      {
        dis2 e4 fis
      }
    }}
    {\chords {b1 } 
    }
  >>
}

ChorusChorusAltEndTwo = 
{<< 
    {
      \new Voice = "first"
      {
      \voiceOne 
      \relative c'' 
      {b1 g'2 g g g }
      }
    }
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'
      {dis2 e4 fis   g2 g \bar "||"g^"1st chorus only" g }
    }}
    {\chords {b1 c c} 
    }
>>}

Bridge = 
{<< 
    {
      \new Voice = "first"
      {
      \voiceOne 
      \relative c''' 
      { g1^"Bridge after gtr solo"~g g~g fis~fis e dis e~e d~d d~d c b~ \bar "||"b4\rtoe^"Quiet verse ..." r r r }
      }
    }
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c''
      { e1~e d~d d~d c b b~b b~b a~a g fis (e4) r r r}
    }}
    {\chords {e1*2:m g d c1 b1 e1*2:m g d c1 b1 e } 
    }
>>}



Bbsaxpart =
{
  \tempo 4 = 150
  \key g \major

  \repeat volta 2
  {
    \ChorusStart
  }
  \alternative
  { 
    {\ChorusAltEndOne}
    {\ChorusChorusAltEndTwo}
  } \break

  \Bridge
}

\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"}  
    \transpose d a, \Bbsaxpart 
    
}

%\pageBreak
\markup {"Intro; Verse; Chorus; Verse; Double Chorus; "}
\markup {"Gtr solo; Bridge; Quiet Verse; Chorus & Exit Chorus  "}
\markup {"  "} \markup {"  "}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

