
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Wake Me Up Before You Go Go"
  %subtitle = "V1"
  opus = "12 Nov 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}

%#(set-global-staff-size 26)



Intro =
{ \relative c^"Intro"
  \set Score.skipBars = ##t   {R1*8}  
}


Verse = 
{
  <<
    \relative c'
    { 
      r1 r r r   r1 r r r  \break
      e4 r8 e8~e2  fis4 r8 fis8~fis2 
      g4 r8 g8~g2  fis4 r8 fis8~fis2
      e4 r8 e8~e2  fis4 r8 fis8~fis2 
      g4 r8 g8~g2  a2 a2 a4 r r r
    }
    \chords 
    {
      d1*2 e1:m d1
      d1*2 e1:m d1
      e1:m fis:m g fis:m
      e1:m fis:m g a r
    }
  >>
}


Chorus = 
{
  \repeat volta 4
  {
  <<
    \relative c'{ r1 r r r^"x4"  }
    \chords { d1*2 e1:m d1}
  >>
  }
  \set Score.skipBars = ##t   {R1*3} 
  \relative c^"high ... ... yeah yeah"
  \set Score.skipBars = ##t   {R1*4} 
}



Instrumental = 
{
  <<
    \relative c''
    { 
       fis4 fis e8 d b4  d2. fis8 a8 
       g4 fis e d 8 b \tuplet 3/2 {fis4 a b fis' d b} \break
       d,4 d fis8 a b4 d2~d8 fis fis a b4 fis e8 d b a   fis a r4 b8 d fis e~
       e1 \set Score.skipBars = ##t   {R1*3} 
       \set Score.skipBars = ##t   {R1*3} a,2.^"We'll stay..." b8 a
       d4 r r r \set Score.skipBars = ##t   {R1*2}  r4 r r b8 a
       d4 r r r \set Score.skipBars = ##t   {R1*3} 
    }
    \chords 
    {
      d1*2 e1:m d  d1*2 e1:m d 
    }
  >>
}

LastChorusAndOutro =
{
  \set Score.skipBars = ##t   {R1*4} 
  \set Score.skipBars = ##t   {R1*3} 
  \relative c'' {d8 d b b a a fis4}
  \relative c''
  {
    r8 a r4 a2   r1   r8 b r4 b2  r1
    r8 a r4 a2   r4 r r d8 b   e8 r e r e2   d8 d b b a a fis4
    r8 a r4 a2   d4 b r r   r8 b r4 b2   d4 b a8 b r4
    r8 a r4 a2   d4 b8 d8 r4 r   r8 b r4 b2      d8 d b b a a fis4
  }
  \repeat volta2
  {
    \relative c''
    { 
       fis4 fis e8 d b4  d2. fis8 a8 
       g4 fis e d 8 b \tuplet 3/2 {fis4 a b fis' d b} \break
       d,4 d fis8 a b4 d2~d8 fis fis a b4 fis e8 d b a'~a1
    }
  }
  
  
  
  
  
}


Bbsaxpart =
{
  %\tempo 4 = 148
  \key d \major
  \time 4/4
  
  \Intro 
  \repeat volta2
  {
    \relative c^"VERSE" {\Verse} \break
    \relative c^"CHORUS" {\Chorus} \break
  }
  \relative c^"After 2nd Chorus: INSTRUMENTAL" \Instrumental \break
  \relative c^"LAST CHORUS & OUTRO"  \LastChorusAndOutro
}

\score 
  {
    %\font-size (-6)
    \new Staff \with { instrumentName = \markup \bold "ALTO"}

    \transpose d a \Bbsaxpart 
    %\midi {}
}
\pageBreak
\markup {"Wake Me Up Before You Go Go"} 

\score 
  {
    \new Staff \with { instrumentName = \markup \bold "TENOR"}
    \Bbsaxpart
    %\midi{}
}

