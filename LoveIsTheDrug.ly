
\version "2.16.0"  % necessary for upgrading to future LilyPond versions.

\header{
  title = "Love Is The Drug"
  opus = "21 October 2019"
}
\paper {top-margin = 5 bottom-margin = 5 left-margin = 5 right-margin = 5}
#(set-global-staff-size 21)

Intro = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c'' 
      {
          
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'' 
      {
         r1 r1 d8 b8~b4 r2 r1 r r \break
         r1 r r r  r1 r r r
         r1 r4 r r d8 fis b2 r r1  r1 r4 r r d,8 fis a2 r r1
      }
    }}
    {\chords 
     { 
       r1*6
       e1:m e2:m g b1*2   e1:m e2:m d b1*2
       e1:m e2:m g b1*2   e1:m e2:m d b1*2
     } 
    }
  >>
}

Prelude =
{
  \relative c^"More Intro"
  \set Score.skipBars = ##t   {R1*4} 
  \set Score.skipBars = ##t   {R1*4} 
}

Verse =
{
  \relative c^"Verse"
  \set Score.skipBars = ##t   {R1*4} 
  \set Score.skipBars = ##t   {R1*4} 
  \set Score.skipBars = ##t   {R1*4} 
  \set Score.skipBars = ##t   {R1*4}  
}

Chorus =
{
  \relative c^"Chorus"
  \set Score.skipBars = ##t   {R1*4} 
  \set Score.skipBars = ##t   {R1*4}  
}

ChorusLast =
{
  \relative c^"Chorus"
  \set Score.skipBars = ##t   {R1*4} 
  \set Score.skipBars = ##t   {R1*3}  
}

Interlude = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''' 
      {
         a1 a  b4. a4. g4 fis1  a1 a  b4. a4. g4 fis1  
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'' 
      {
         fis1^"Interlude" e  g4. fis4. e4 d1  fis1 e  g4. fis4. e4 d1
      }
    }}
    {\chords 
     { 
        d1 a g2 d4 a  d1 a g2 d4 a 
     } 
    }
  >>
}

OutroStart = 
{
  \relative c'
  {
     r4^"me!" r r d'8 fis \break
    
    \repeat volta2
    {
      b2^"Outro" r r4 r r d,8 fis
      b2 r r4 r r d,8 fis
      a2 r r4 r r d,8 fis    
    }
    \alternative
    {
      {b2 r r4 r r d,8 fis}
      {b1~b4 r4 r r}
    }
  }
}

OutroEnd = 
{<< 
    {
      \new Voice = "first"{
      \voiceOne 
      \relative c''' 
      {
         a1 a  b4. a4. g4 fis1  a1 a  b4. a4. g4 fis1\fermata  
      }    
    }}
    {
      \new Voice= "second"{
      \voiceTwo
      \relative c'' 
      {
         fis1 e  g4. fis4. e4 d1  fis1 e  \repeat volta3 {g4. fis4. e4^"x3"} d1
      }
    }}
    {\chords 
     { 
        d1 a g2 d4 a  d1 d a g2 d4 a  d1
     } 
    }
  >>
}



Bbsaxpart =
{
  \tempo 4 = 114
  \key e \minor


  \Intro  \break
  \Prelude
  \Verse \break
  \Chorus \break
  \Interlude \break
  \Verse
  \Chorus \ChorusLast 
  \OutroStart \break
  \OutroEnd
  
  
}


\score 
  {
    \new Staff \with {instrumentName = #"Alto sax"} 
    \transpose c g, \Bbsaxpart   
}

\pageBreak
\markup {"Lady Marmalade"}

\score 
  {
    \new Staff \with { instrumentName = #"Tenor Sax"}
    \Bbsaxpart 
    %\midi {}

}

